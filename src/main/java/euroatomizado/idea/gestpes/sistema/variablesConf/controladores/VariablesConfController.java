package euroatomizado.idea.gestpes.sistema.variablesConf.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto.VariablesConfFormDto;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto.VariablesConfInfoDto;
import euroatomizado.idea.gestpes.sistema.variablesConf.managers.VariablesConfManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/variables-conf")
public class VariablesConfController {

    @Autowired
    private VariablesConfManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<VariablesConfInfoDto> obtenerVariables() {
        return manager.obtenerVariables();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public VariablesConfInfoDto crear(@RequestBody @Valid VariablesConfFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public VariablesConfInfoDto modificar(@RequestBody @Valid VariablesConfFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
