package euroatomizado.idea.gestpes.sistema.variablesConf.dominio.daos;

import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.entidades.VariablesConfEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VariablesConfDao extends CrudRepository<VariablesConfEntity, String> {

    List<VariablesConfEntity> findAll();

    @Query(" select vai.varValor " +
            "from VariablesConfEntity vai where vai.varNombre = :nombre ")
    String obtenerValorPorNombre(@Param("nombre") String nombre);

}