package euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class VariablesConfFormDto {

    @NotNull
    @Size(min = 1, max = 50)
    private String varId;
    @Size(min = 1, max = 200)
    private String varNombre;
    @Size(min = 1, max = 200)
    private String varValor;
    @Size(min = 1, max = 200)
    private String varDescripcion;

}
