package euroatomizado.idea.gestpes.sistema.variablesConf.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto.VariablesConfFormDto;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto.VariablesConfInfoDto;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.entidades.VariablesConfEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VariablesConfManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<VariablesConfInfoDto> obtenerVariables() {
        return obtenerDatosSalida(daoManager.getVariablesConfDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public String obtenerValorVariablePorNombre(String nombre) {
        return daoManager.getVariablesConfDao().obtenerValorPorNombre(nombre);
    }

    @Transactional(rollbackFor = Exception.class)
    public VariablesConfInfoDto guardar(VariablesConfFormDto datos) {
        VariablesConfEntity entity = mapperManager.getMapeadorVariablesConf().toEntidad(datos);
        entity.setVariablesConfFormDto(datos);
        try {
            if (!existe(entity.getVarId()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    public VariablesConfEntity guardarNuevo(VariablesConfEntity nuevo) {
        String id = nuevo.getVariablesConfFormDto().getVarId();
        nuevo.setVarId(id);
        daoManager.getVariablesConfDao().save(nuevo);
        return nuevo;
    }

    public VariablesConfEntity modificar(VariablesConfEntity nuevo) {
        VariablesConfEntity entity = obtener(nuevo);
        mapperManager.getMapeadorVariablesConf().actualizar(nuevo, entity);
        daoManager.getVariablesConfDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        if (existe(id))
            daoManager.getVariablesConfDao().delete(id);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    public VariablesConfInfoDto obtenerDatosSalida(VariablesConfEntity entity) {
        return mapperManager.getMapeadorVariablesConf().toInfoDto(entity);
    }

    private List<VariablesConfInfoDto> obtenerDatosSalida(List<VariablesConfEntity> entidades) {
        return mapperManager.getMapeadorVariablesConf().toInfoDto(entidades);
    }

    public boolean existe(String id) {
        return id != null && daoManager.getVariablesConfDao().exists(id);
    }

    public VariablesConfEntity obtener(VariablesConfEntity datos) {
        return daoManager.getVariablesConfDao().findOne(datos.getVarId());
    }

}
