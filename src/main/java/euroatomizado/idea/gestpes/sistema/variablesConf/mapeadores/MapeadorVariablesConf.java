package euroatomizado.idea.gestpes.sistema.variablesConf.mapeadores;

import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto.VariablesConfFormDto;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto.VariablesConfInfoDto;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.entidades.VariablesConfEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorVariablesConf {

    VariablesConfInfoDto toInfoDto(VariablesConfEntity datos);

    List<VariablesConfInfoDto> toInfoDto(List<VariablesConfEntity> datos);

    VariablesConfEntity toEntidad(VariablesConfFormDto dto);

    void actualizar(VariablesConfEntity datosNueva, @MappingTarget VariablesConfEntity datos);

}