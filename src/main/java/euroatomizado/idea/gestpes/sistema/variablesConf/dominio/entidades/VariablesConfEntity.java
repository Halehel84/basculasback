package euroatomizado.idea.gestpes.sistema.variablesConf.dominio.entidades;

import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto.VariablesConfFormDto;
import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "variables_conf", schema = "dbo")
@Data
public class VariablesConfEntity {

    @Id
    @Column(name = "var_id")
    private String varId;
    @Column(name = "var_nombre")
    private String varNombre;
    @Column(name = "var_valor")
    private String varValor;
    @Column(name = "var_descripcion")
    private String varDescripcion;

    @Transient
    private VariablesConfFormDto variablesConfFormDto;

}
