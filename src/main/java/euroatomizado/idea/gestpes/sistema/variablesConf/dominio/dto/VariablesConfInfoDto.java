package euroatomizado.idea.gestpes.sistema.variablesConf.dominio.dto;

import lombok.Data;

@Data
public class VariablesConfInfoDto {

    private String varId;
    private String varNombre;
    private String varValor;
    private String varDescripcion;

}
