package euroatomizado.idea.gestpes.sistema.usuarios.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto.UsuarioFormDto;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto.UsuarioInfoDto;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.entidades.UsuarioEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<UsuarioInfoDto> obtenerUsuarios() {
        return obtenerDatosSalida(daoManager.getUsuarioDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioInfoDto guardar(UsuarioFormDto datos) {
        UsuarioEntity entity = mapperManager.getMapeadorUsuario().toEntidad(datos);
        entity.setUsuarioFormDto(datos);
        try {
            if (!existe(entity.getUsuId()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    public UsuarioEntity guardarNuevo(UsuarioEntity nuevo) {
        String id = nuevo.getUsuarioFormDto().getUsuId();
        nuevo.setUsuId(id);
        daoManager.getUsuarioDao().save(nuevo);
        return nuevo;
    }

    public UsuarioEntity modificar(UsuarioEntity nuevo) {
        UsuarioEntity entity = obtener(nuevo);
        mapperManager.getMapeadorUsuario().actualizar(nuevo, entity);
        daoManager.getUsuarioDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        if (existe(id))
            daoManager.getUsuarioDao().delete(id);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    public UsuarioInfoDto obtenerDatosSalida(UsuarioEntity entity) {
        return mapperManager.getMapeadorUsuario().toInfoDto(entity);
    }

    private List<UsuarioInfoDto> obtenerDatosSalida(List<UsuarioEntity> entidades) {
        return mapperManager.getMapeadorUsuario().toInfoDto(entidades);
    }

    public boolean existe(String id) {
        return id != null && daoManager.getUsuarioDao().exists(id);
    }

    public UsuarioEntity obtener(UsuarioEntity datos) {
        return daoManager.getUsuarioDao().findOne(datos.getUsuId());
    }

}
