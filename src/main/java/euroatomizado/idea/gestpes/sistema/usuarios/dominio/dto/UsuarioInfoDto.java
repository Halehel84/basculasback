package euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UsuarioInfoDto {

    private String usuId;
    private String usuUsuario;
    private String usuPwd;
    private String usuNombre;
    private String usuTelefono;
    private String usuEmail;
    private String usuRol;
    private Character usuActivo;
    private LocalDateTime usuFechaAlta;
    private LocalDateTime usuFechaBaja;

}
