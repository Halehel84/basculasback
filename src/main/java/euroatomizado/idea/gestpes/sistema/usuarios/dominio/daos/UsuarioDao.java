package euroatomizado.idea.gestpes.sistema.usuarios.dominio.daos;

import euroatomizado.idea.gestpes.sistema.usuarios.dominio.entidades.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioDao extends CrudRepository<UsuarioEntity, String> {

    List<UsuarioEntity> findAll();

}
