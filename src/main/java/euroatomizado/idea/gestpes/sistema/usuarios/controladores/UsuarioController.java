package euroatomizado.idea.gestpes.sistema.usuarios.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto.UsuarioFormDto;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto.UsuarioInfoDto;
import euroatomizado.idea.gestpes.sistema.usuarios.managers.UsuarioManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<UsuarioInfoDto> obtenerUsuarios() {
        return manager.obtenerUsuarios();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public UsuarioInfoDto crear(@RequestBody @Valid UsuarioFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public UsuarioInfoDto modificar(@RequestBody @Valid UsuarioFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
