package euroatomizado.idea.gestpes.sistema.usuarios.dominio.entidades;

import euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto.UsuarioFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "usuarios", schema = "dbo")
@Data
public class UsuarioEntity {

    @Id
    @Column(name = "usu_id")
    private String usuId;
    @Column(name = "usu_usuario")
    private String usuUsuario;
    @Column(name = "usu_pwd")
    private String usuPwd;
    @Column(name = "usu_nombre")
    private String usuNombre;
    @Column(name = "usu_telefono")
    private String usuTelefono;
    @Column(name = "usu_email")
    private String usuEmail;
    @Column(name = "usu_rol")
    private String usuRol;
    @Column(name = "usu_activo")
    private Character usuActivo;
    @Column(name = "usu_f_alta")
    private LocalDateTime usuFechaAlta;
    @Column(name = "usu_f_baja")
    private LocalDateTime usuFechaBaja;

    @Transient
    private UsuarioFormDto usuarioFormDto;

}