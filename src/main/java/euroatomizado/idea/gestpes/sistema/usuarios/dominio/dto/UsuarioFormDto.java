package euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class UsuarioFormDto {

    @NotNull
    @Size(min = 1, max = 50)
    private String usuId;
    @NotNull
    @Size(min = 1, max = 200)
    private String usuUsuario;
    @NotNull
    @Size(min = 1, max = 200)
    private String usuPwd;
    @Size(min = 1, max = 200)
    private String usuNombre;
    @Size(min = 1, max = 200)
    private String usuTelefono;
    @Size(min = 1, max = 200)
    private String usuEmail;
    @NotNull
    @Size(min = 1, max = 200)
    private String usuRol;
    @Size(min = 1, max = 200)
    private Character usuActivo;
    private LocalDateTime usuFechaAlta;
    private LocalDateTime usuFechaBaja;

}