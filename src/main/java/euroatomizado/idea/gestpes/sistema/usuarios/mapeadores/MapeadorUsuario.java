package euroatomizado.idea.gestpes.sistema.usuarios.mapeadores;

import euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto.UsuarioFormDto;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.dto.UsuarioInfoDto;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.entidades.UsuarioEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorUsuario {

    UsuarioInfoDto toInfoDto(UsuarioEntity datos);

    List<UsuarioInfoDto> toInfoDto(List<UsuarioEntity> datos);

    UsuarioEntity toEntidad(UsuarioFormDto dto);

    void actualizar(UsuarioEntity datosNueva, @MappingTarget UsuarioEntity datos);

}