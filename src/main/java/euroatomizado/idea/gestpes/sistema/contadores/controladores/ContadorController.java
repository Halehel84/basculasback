package euroatomizado.idea.gestpes.sistema.contadores.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.dto.ContadorFormDto;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.dto.ContadorInfoDto;
import euroatomizado.idea.gestpes.sistema.contadores.managers.ContadorManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/contadores")
public class ContadorController {

    @Autowired
    private ContadorManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ContadorInfoDto> obtenerContadores() {
        return manager.obtenerContadores();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public ContadorInfoDto crear(@RequestBody @Valid ContadorFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public ContadorInfoDto modificar(@RequestBody @Valid ContadorFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}