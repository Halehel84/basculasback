package euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades;

import euroatomizado.idea.gestpes.sistema.contadores.dominio.dto.ContadorFormDto;
import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "contadores", schema = "dbo")
@Data
public class ContadorEntity {

    @EmbeddedId
    private ContadorEntityPk contadorPk;
    @Column(name = "con_contador", columnDefinition = "real")
    private Float conContador;

    @Transient
    private ContadorFormDto contadorFormDto;

}
