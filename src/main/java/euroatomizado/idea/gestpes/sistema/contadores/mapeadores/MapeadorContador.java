package euroatomizado.idea.gestpes.sistema.contadores.mapeadores;

import euroatomizado.idea.gestpes.sistema.contadores.dominio.dto.ContadorFormDto;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.dto.ContadorInfoDto;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades.ContadorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorContador {

    @Mapping(source = "contadorPk.empresaId", target = "empresaId")
    @Mapping(source = "contadorPk.conTipoDoc", target = "conTipoDoc")
    @Mapping(source = "contadorPk.seccionId", target = "seccionId")
    ContadorInfoDto toInfoDto(ContadorEntity datos);

    List<ContadorInfoDto> toInfoDto(List<ContadorEntity> datos);

    ContadorEntity toEntidad(ContadorFormDto dto);

    void actualizar(ContadorEntity datosNueva, @MappingTarget ContadorEntity datos);

}