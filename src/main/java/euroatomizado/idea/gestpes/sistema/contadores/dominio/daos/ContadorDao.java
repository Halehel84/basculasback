package euroatomizado.idea.gestpes.sistema.contadores.dominio.daos;

import euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades.ContadorEntity;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades.ContadorEntityPk;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContadorDao extends CrudRepository<ContadorEntity, ContadorEntityPk> {

    List<ContadorEntity> findAll();

    @Query(" select con " +
            "from ContadorEntity con where con.contadorPk.conTipoDoc = 'SACAS' and con.contadorPk.seccionId = :seccion")
    ContadorEntity obtenerContadorSacas(@Param("seccion") String seccion);

    @Query(" select con " +
            "from ContadorEntity con where con.contadorPk.conTipoDoc = 'UT' and con.contadorPk.seccionId = :seccion")
    ContadorEntity obtenerContadorTicket(@Param("seccion") String seccion);

}
