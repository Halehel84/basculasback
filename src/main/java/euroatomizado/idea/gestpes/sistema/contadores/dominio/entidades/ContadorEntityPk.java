package euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ContadorEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresaId;
    @Column(name = "con_tipo_doc")
    private String conTipoDoc;
    @Column(name = "xseccion_id")
    private String seccionId;

}
