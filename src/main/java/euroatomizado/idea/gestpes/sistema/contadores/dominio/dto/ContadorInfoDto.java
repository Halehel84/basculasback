package euroatomizado.idea.gestpes.sistema.contadores.dominio.dto;

import lombok.Data;

@Data
public class ContadorInfoDto {

    private String empresaId;
    private String conTipoDoc;
    private String seccionId;
    private Float conContador;

}
