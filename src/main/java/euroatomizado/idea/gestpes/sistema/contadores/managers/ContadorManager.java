package euroatomizado.idea.gestpes.sistema.contadores.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.dto.ContadorFormDto;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.dto.ContadorInfoDto;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades.ContadorEntity;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades.ContadorEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class ContadorManager implements PrimaryKeyStringManagerInterface<ContadorInfoDto, ContadorEntity, ContadorEntityPk, ContadorFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ContadorInfoDto> obtenerContadores() {
        return obtenerDatosSalida(daoManager.getContadorDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ContadorInfoDto guardar(ContadorFormDto datos) {
        ContadorEntity almacen = mapperManager.getMapeadorContador().toEntidad(datos);
        almacen.setContadorFormDto(datos);
        try {
            if (!existe(almacen.getContadorPk()))
                return obtenerDatosSalida(guardarNuevo(almacen));
            return obtenerDatosSalida(modificar(almacen));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public ContadorEntity guardarNuevo(ContadorEntity nuevo) {
        String id = nuevo.getContadorFormDto().getConTipoDoc();
        nuevo.setContadorPk(obtenerPrimaryKey(id));
        daoManager.getContadorDao().save(nuevo);
        return nuevo;
    }

    @Override
    public ContadorEntity modificar(ContadorEntity nuevo) {
        ContadorEntity almacen = obtener(nuevo);
        mapperManager.getMapeadorContador().actualizar(nuevo, almacen);
        daoManager.getContadorDao().save(almacen);
        return almacen;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        ContadorEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getContadorDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public ContadorInfoDto obtenerDatosSalida(ContadorEntity almacen) {
        return mapperManager.getMapeadorContador().toInfoDto(almacen);
    }

    private List<ContadorInfoDto> obtenerDatosSalida(List<ContadorEntity> almacenes) {
        return mapperManager.getMapeadorContador().toInfoDto(almacenes);
    }

    @Override
    public boolean existe(ContadorEntityPk pk) {
        return pk != null && daoManager.getContadorDao().exists(pk);
    }

    @Override
    public ContadorEntity obtener(ContadorEntity datos) {
        return daoManager.getContadorDao().findOne(datos.getContadorPk());
    }

    @Override
    public ContadorEntityPk obtenerPrimaryKey(String id) {
        ContadorEntityPk pk = new ContadorEntityPk();
        pk.setConTipoDoc(id);
        pk.setEmpresaId(EMPRESA);
        return pk;
    }

}
