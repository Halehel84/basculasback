package euroatomizado.idea.gestpes.sistema.estados.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class EstadoFormDto {

    @NotNull
    private Integer estId;
    @NotNull
    @Size(min = 1, max = 4)
    private String estEmpresa;
    @NotNull
    @Size(min = 1, max = 50)
    private String estGrupo;
    @NotNull
    @Size(min = 1, max = 50)
    private String estDescripcion;

}
