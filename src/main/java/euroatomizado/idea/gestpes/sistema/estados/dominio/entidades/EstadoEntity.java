package euroatomizado.idea.gestpes.sistema.estados.dominio.entidades;

import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoFormDto;
import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "estados", schema = "dbo")
@Data
public class EstadoEntity {

    @Id
    @Column(name = "est_id")
    private Integer estId;
    @Column(name = "est_empresa")
    private String estEmpresa;
    @Column(name = "est_grupo")
    private String estGrupo;
    @Column(name = "est_descripcion")
    private String estDescripcion;

    @Transient
    private EstadoFormDto estadoFormDto;

}
