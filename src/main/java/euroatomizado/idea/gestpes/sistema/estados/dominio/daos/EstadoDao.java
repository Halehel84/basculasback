package euroatomizado.idea.gestpes.sistema.estados.dominio.daos;

import euroatomizado.idea.gestpes.sistema.estados.dominio.entidades.EstadoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadoDao extends CrudRepository<EstadoEntity, Integer> {

    List<EstadoEntity> findAll();

}
