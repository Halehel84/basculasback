package euroatomizado.idea.gestpes.sistema.estados.mapeadores;

import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoFormDto;
import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoInfoDto;
import euroatomizado.idea.gestpes.sistema.estados.dominio.entidades.EstadoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorEstado {

    EstadoInfoDto toInfoDto(EstadoEntity datos);

    List<EstadoInfoDto> toInfoDto(List<EstadoEntity> datos);

    EstadoEntity toEntidad(EstadoFormDto dto);

    void actualizar(EstadoEntity datosNueva, @MappingTarget EstadoEntity datos);

}