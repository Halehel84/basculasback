package euroatomizado.idea.gestpes.sistema.estados.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoFormDto;
import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoInfoDto;
import euroatomizado.idea.gestpes.sistema.estados.dominio.entidades.EstadoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class EstadoManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<EstadoInfoDto> obtenerEstados() {
        return obtenerDatosSalida(daoManager.getEstadoDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public EstadoInfoDto guardar(EstadoFormDto datos) {
        EstadoEntity entity = mapperManager.getMapeadorEstado().toEntidad(datos);
        entity.setEstadoFormDto(datos);
        try {
            if (!existe(entity.getEstId()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    public EstadoEntity guardarNuevo(EstadoEntity nuevo) {
        Integer id = nuevo.getEstadoFormDto().getEstId();
        nuevo.setEstId(id);
        nuevo.setEstEmpresa(EMPRESA);
        daoManager.getEstadoDao().save(nuevo);
        return nuevo;
    }

    public EstadoEntity modificar(EstadoEntity nuevo) {
        EstadoEntity entity = obtener(nuevo);
        mapperManager.getMapeadorEstado().actualizar(nuevo, entity);
        daoManager.getEstadoDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(Integer id) {
        if (existe(id))
            daoManager.getEstadoDao().delete(id);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    public EstadoInfoDto obtenerDatosSalida(EstadoEntity entity) {
        return mapperManager.getMapeadorEstado().toInfoDto(entity);
    }

    private List<EstadoInfoDto> obtenerDatosSalida(List<EstadoEntity> entidades) {
        return mapperManager.getMapeadorEstado().toInfoDto(entidades);
    }

    public boolean existe(Integer id) {
        return id != null && daoManager.getEstadoDao().exists(id);
    }

    public EstadoEntity obtener(EstadoEntity datos) {
        return daoManager.getEstadoDao().findOne(datos.getEstId());
    }

}

