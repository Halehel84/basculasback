package euroatomizado.idea.gestpes.sistema.estados.dominio.dto;

import lombok.Data;

@Data
public class EstadoInfoDto {

    private Integer estId;
    private String estEmpresa;
    private String estGrupo;
    private String estDescripcion;

}
