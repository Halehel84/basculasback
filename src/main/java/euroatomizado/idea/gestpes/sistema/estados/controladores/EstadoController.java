package euroatomizado.idea.gestpes.sistema.estados.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaFormDto;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaInfoDto;
import euroatomizado.idea.gestpes.sacas.managers.SacaManager;
import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoFormDto;
import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoInfoDto;
import euroatomizado.idea.gestpes.sistema.estados.managers.EstadoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/estados")
public class EstadoController {

    @Autowired
    private EstadoManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<EstadoInfoDto> obtenerEstados() {
        return manager.obtenerEstados();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public EstadoInfoDto crear(@RequestBody @Valid EstadoFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public EstadoInfoDto modificar(@RequestBody @Valid EstadoFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable Integer id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
