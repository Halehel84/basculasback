package euroatomizado.idea.gestpes.sistema.auditoria.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.sistema.auditoria.dominio.dto.AuditoriaInfoDto;
import euroatomizado.idea.gestpes.sistema.auditoria.dominio.entidades.AuditoriaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AuditoriaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<AuditoriaInfoDto> obtenerAuditorias() {
        List<AuditoriaEntity> lista = daoManager.getAuditoriaDao().findAll();
        return mapperManager.getMapeadorAudtoria().toInfoDto(lista);
    }

}
