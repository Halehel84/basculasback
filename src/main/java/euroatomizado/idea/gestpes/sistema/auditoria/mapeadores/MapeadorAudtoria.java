package euroatomizado.idea.gestpes.sistema.auditoria.mapeadores;

import euroatomizado.idea.gestpes.sistema.auditoria.dominio.dto.AuditoriaInfoDto;
import euroatomizado.idea.gestpes.sistema.auditoria.dominio.entidades.AuditoriaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorAudtoria {

    AuditoriaInfoDto toInfoDto(AuditoriaEntity datos);

    List<AuditoriaInfoDto> toInfoDto(List<AuditoriaEntity> datos);

    void actualizar(AuditoriaEntity datosNueva, @MappingTarget AuditoriaEntity datos);

}
