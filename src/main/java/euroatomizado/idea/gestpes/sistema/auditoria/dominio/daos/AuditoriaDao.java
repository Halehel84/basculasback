package euroatomizado.idea.gestpes.sistema.auditoria.dominio.daos;

import euroatomizado.idea.gestpes.sistema.auditoria.dominio.entidades.AuditoriaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuditoriaDao extends CrudRepository<AuditoriaEntity, String> {

    List<AuditoriaEntity> findAll();

}
