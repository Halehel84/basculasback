package euroatomizado.idea.gestpes.sistema.auditoria.dominio.entidades;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "auditoria", schema = "dbo")
@Data
public class AuditoriaEntity {

    @Id
    @Column(name = "aud_id")
    private String audId;
    @Column(name = "usu_id")
    private String usuId;
    @Column(name = "aud_movimiento")
    private String audMovimiento;
    @Column(name = "aud_fecha")
    private LocalDateTime aud_fecha;
    @Column(name = "xempresa")
    private String xempresa;

}
