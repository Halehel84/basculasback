package euroatomizado.idea.gestpes.sistema.auditoria.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.sistema.auditoria.dominio.dto.AuditoriaInfoDto;
import euroatomizado.idea.gestpes.sistema.auditoria.managers.AuditoriaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/auditoria")
public class AuditoriaController {

    @Autowired
    private AuditoriaManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<AuditoriaInfoDto> obtenerAuditorias() {
        return manager.obtenerAuditorias();
    }

}
