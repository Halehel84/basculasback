package euroatomizado.idea.gestpes.sistema.auditoria.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AuditoriaInfoDto {

    private String audId;
    private String usuId;
    private String audMovimiento;
    private LocalDateTime aud_fecha;
    private String xempresa;

}
