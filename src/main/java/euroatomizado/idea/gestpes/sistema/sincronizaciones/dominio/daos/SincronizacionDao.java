package euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.daos;

import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.entidades.SincronizacionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SincronizacionDao extends CrudRepository<SincronizacionEntity, String> {

    List<SincronizacionEntity> findAll();

}
