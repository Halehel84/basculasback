package euroatomizado.idea.gestpes.sistema.sincronizaciones.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoFormDto;
import euroatomizado.idea.gestpes.sistema.estados.dominio.dto.EstadoInfoDto;
import euroatomizado.idea.gestpes.sistema.estados.managers.EstadoManager;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto.SincronizacionFormDto;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto.SincronizacionInfoDto;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.managers.SincronizacionManger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/sincronizaciones")
public class SincronizacionController {

    @Autowired
    private SincronizacionManger manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<SincronizacionInfoDto> obtenerSincronizaciones() {
        return manager.obtenerSincronizaciones();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public SincronizacionInfoDto crear(@RequestBody @Valid SincronizacionFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public SincronizacionInfoDto modificar(@RequestBody @Valid SincronizacionFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
