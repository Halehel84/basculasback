package euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.entidades;

import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto.SincronizacionFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "sincronizaciones", schema = "dbo")
@Data
public class SincronizacionEntity {

    @Id
    @Column(name = "sin_id")
    private String id;
    @Column(name = "sin_tabla")
    private String tabla;
    @Column(name = "sin_fecha")
    private LocalDateTime fecha;
    @Column(name = "sin_descripcion")
    private String descripcion;

    @Transient
    private SincronizacionFormDto sincronizacionFormDto;

}