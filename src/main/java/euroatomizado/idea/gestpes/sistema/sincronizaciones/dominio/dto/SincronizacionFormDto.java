package euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class SincronizacionFormDto {

    @NotNull
    @Size(min = 1, max = 50)
    private String sinId;
    @NotNull
    @Size(min = 1, max = 200)
    private String sinTabla;
    @NotNull
    private LocalDateTime sinFecha;
    @Size(min = 1, max = 500)
    private String sinDescripcion;

}