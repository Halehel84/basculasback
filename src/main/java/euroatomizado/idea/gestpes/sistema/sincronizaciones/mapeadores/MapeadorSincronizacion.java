package euroatomizado.idea.gestpes.sistema.sincronizaciones.mapeadores;

import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto.SincronizacionFormDto;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto.SincronizacionInfoDto;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.entidades.SincronizacionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorSincronizacion {

    SincronizacionInfoDto toInfoDto(SincronizacionEntity datos);

    List<SincronizacionInfoDto> toInfoDto(List<SincronizacionEntity> datos);

    SincronizacionEntity toEntidad(SincronizacionFormDto dto);

    void actualizar(SincronizacionEntity datosNueva, @MappingTarget SincronizacionEntity datos);

}