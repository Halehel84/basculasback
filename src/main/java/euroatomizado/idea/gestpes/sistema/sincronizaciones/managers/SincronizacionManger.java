package euroatomizado.idea.gestpes.sistema.sincronizaciones.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto.SincronizacionFormDto;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto.SincronizacionInfoDto;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.entidades.SincronizacionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SincronizacionManger {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<SincronizacionInfoDto> obtenerSincronizaciones() {
        return obtenerDatosSalidaYRangoAlerta(daoManager.getSincronizacionDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public SincronizacionInfoDto guardar(SincronizacionFormDto datos) {
        SincronizacionEntity entity = mapperManager.getMapeadorSincronizacion().toEntidad(datos);
        entity.setSincronizacionFormDto(datos);
        try {
            if (!existe(entity.getId()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    public SincronizacionEntity guardarNuevo(SincronizacionEntity nuevo) {
        String id = nuevo.getSincronizacionFormDto().getSinId();
        nuevo.setId(id);
        daoManager.getSincronizacionDao().save(nuevo);
        return nuevo;
    }

    public SincronizacionEntity modificar(SincronizacionEntity nuevo) {
        SincronizacionEntity entity = obtener(nuevo);
        mapperManager.getMapeadorSincronizacion().actualizar(nuevo, entity);
        daoManager.getSincronizacionDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        if (existe(id))
            daoManager.getSincronizacionDao().delete(id);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    public SincronizacionInfoDto obtenerDatosSalida(SincronizacionEntity entity) {
        return mapperManager.getMapeadorSincronizacion().toInfoDto(entity);
    }

    private List<SincronizacionInfoDto> obtenerDatosSalida(List<SincronizacionEntity> entidades) {
        return mapperManager.getMapeadorSincronizacion().toInfoDto(entidades);
    }

    private List<SincronizacionInfoDto> obtenerDatosSalidaYRangoAlerta(List<SincronizacionEntity> entidades) {
        List<SincronizacionInfoDto> sincronizaciones = mapperManager.getMapeadorSincronizacion().toInfoDto(entidades);
        anyadirRangoMaestros(sincronizaciones);
        sincronizaciones.add(asignarSincronizacionAlbaranes());
        return sincronizaciones;
    }

    public boolean existe(String id) {
        return id != null && daoManager.getSincronizacionDao().exists(id);
    }

    public SincronizacionEntity obtener(SincronizacionEntity datos) {
        return daoManager.getSincronizacionDao().findOne(datos.getId());
    }

    private void anyadirRangoMaestros(List<SincronizacionInfoDto> sincronizaciones) {
        for (SincronizacionInfoDto sincro: sincronizaciones){
            Integer valor = Integer.valueOf(daoManager.getVariablesConfDao().obtenerValorPorNombre("rangoAlertaMaestros"));
            sincro.setRangoAlertaMaestros(valor);
        }
    }

    private SincronizacionInfoDto asignarSincronizacionAlbaranes() {
        SincronizacionInfoDto sincroAlbaranes = new SincronizacionInfoDto();
        sincroAlbaranes.setSinId("0");
        sincroAlbaranes.setTabla("tickets");
        sincroAlbaranes.setFecha(daoManager.getTicketDao().obtenerUltimaFechaCaptura());
        sincroAlbaranes.setDescripcion("Sincronización: Albaranes");
        Integer valor = Integer.valueOf(daoManager.getVariablesConfDao().obtenerValorPorNombre("rangoAlertaAlbaranes"));
        sincroAlbaranes.setRangoAlertaAlbaranes(valor);
        return sincroAlbaranes;
    }

}
