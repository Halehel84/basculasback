package euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SincronizacionInfoDto {

    private String sinId;
    private String tabla;
    private LocalDateTime fecha;
    private String descripcion;
    private Integer rangoAlertaMaestros;
    private Integer rangoAlertaAlbaranes;

}
