package euroatomizado.idea.gestpes.reports.dominio;

import lombok.Data;

@Data
public class ArchivoInfoDto {
    private String nombre;
    private String b64data;
    private String data;
    private boolean sinArchivo;
}
