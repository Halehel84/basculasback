package euroatomizado.idea.gestpes.reports.engines;


import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReportUtils {

    public byte[] generarReport(JRBeanCollectionDataSource datos, String rutaBase, String rutaReport, String report, String titulo) throws FileNotFoundException, JRException {
        /* PARAMETROS */
        java.util.HashMap parameters = new HashMap();
        parameters.put("REPORT_DATA_SOURCE", datos);
        parameters.put("rutaImagenes", rutaBase + "imagenes/");
        parameters.put("subreport", rutaBase + rutaReport);
        /* GENERAMOS EL PDF */
        InputStream inputStream = new FileInputStream(new File(rutaBase + rutaReport + report));
        byte[] bytes = JasperRunManager.runReportToPdf(inputStream, parameters);
        /*HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(titulo + ".pdf", titulo + ".pdf");
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
        return response;*/
        return bytes;
    }

}


