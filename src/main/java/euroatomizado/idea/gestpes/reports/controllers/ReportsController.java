package euroatomizado.idea.gestpes.reports.controllers;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.reports.dominio.ArchivoInfoDto;
import euroatomizado.idea.gestpes.reports.managers.ReportManager;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;
import java.io.IOException;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/reports")
public class ReportsController {

@Autowired
private ReportManager reportManager;

    @ResponseStatus(OK)
    @GetMapping(value = "/informe-ticket")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public ArchivoInfoDto obtenerInformeTicket(@PathParam("id") String id, @PathParam("empresa") String empresa, @PathParam("tipo") String tipo) throws JRException, IOException, ClassNotFoundException {
        return reportManager.obtenerInformeGenerado(id, tipo);
    }

}
