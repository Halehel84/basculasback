package euroatomizado.idea.gestpes.reports.managers;

import euroatomizado.idea.gestpes.reports.dominio.ArchivoInfoDto;
import euroatomizado.idea.gestpes.reports.engines.ReportUtils;
import euroatomizado.idea.gestpes.sistema.variablesConf.managers.VariablesConfManager;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.managers.TicketManager;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class ReportManager {

    @Autowired
    private Environment env;

    @Autowired
    private TicketManager ticketManager;

    @Autowired
    private VariablesConfManager variablesConfManager;

    private String REPOSITORIO_ALBARANES = "repositorioAlbaranes";

    public ArchivoInfoDto obtenerInformeGenerado(String id, String tipo) throws IOException, JRException {
        TicketInfoDto ticket = ticketManager.obtenerTicket(id);
        File file = new File(obtenerRutaTicket(ticket, tipo));
        if(file.exists()) {
            byte[] archivo = Files.readAllBytes(Paths.get(obtenerRutaTicket(ticket, tipo)));
            if (archivo != null) {
                return generarDto(archivo, ticket);
            } else {
                ArchivoInfoDto dto = new ArchivoInfoDto();
                dto.setSinArchivo(true);
                return dto;
                //return generarDto(generarInformeJasper(ticket), ticket);
            }
        }else{
            ArchivoInfoDto dto = new ArchivoInfoDto();
            dto.setSinArchivo(true);
            return dto;
        }
    }

    private byte[] generarInformeJasper(TicketInfoDto ticket) throws FileNotFoundException, JRException {
        ReportUtils generarReport = new ReportUtils();
        List<TicketInfoDto> lista = new ArrayList<>();
        lista.add(ticket);
        String rutaBase = env.getProperty("reports.ruta");
        return generarReport.generarReport(new JRBeanCollectionDataSource(lista), rutaBase, "legislaciones/", "legislaciones.jasper", "listadolegislacion");
    }

    private String obtenerRutaTicket(TicketInfoDto ticket, String tipo){
        String rutaInformes = variablesConfManager.obtenerValorVariablePorNombre(REPOSITORIO_ALBARANES);
        String anyo = ticket.getFecha().getYear() + "";
        String nombreMes = obtenerNombreMes(ticket.getFecha().getMonthValue());
        String nombreDocumento;
        if(tipo.equals("E")){
            nombreDocumento = ticket.getTicId() + "_e.pdf";
        }else{
            nombreDocumento = ticket.getTicId() + ".pdf";
        }
        return rutaInformes + anyo + "\\" + nombreMes + "\\" + nombreDocumento;
    }

    private ArchivoInfoDto generarDto(byte[] archivo, TicketInfoDto ticket){
        ArchivoInfoDto dto = new ArchivoInfoDto();
        byte[] b64 = Base64.getEncoder().encode(archivo);
        dto.setB64data("data:application/pdf;base64," + new String(b64));
        dto.setData(new String(b64));
        dto.setNombre(ticket.getTicId());
        return dto;
    }

    private String obtenerNombreMes(Integer mes) {
        String nombreMes = "";
        if (mes == 1) {
            nombreMes = "enero";
        } else if (mes == 2) {
            nombreMes = "febrero";
        } else if (mes == 3) {
            nombreMes = "marzo";
        } else if (mes == 4) {
            nombreMes = "abril";
        } else if (mes == 5) {
            nombreMes = "mayo";
        } else if (mes == 6) {
            nombreMes = "junio";
        } else if (mes == 7) {
            nombreMes = "julio";
        } else if (mes == 8) {
            nombreMes = "agosto";
        } else if (mes == 9) {
            nombreMes = "septiembre";
        } else if (mes == 10) {
            nombreMes = "octubre";
        } else if (mes == 11) {
            nombreMes = "noviembre";
        } else if (mes == 12) {
            nombreMes = "diciembre";
        }
        return nombreMes;
    }
}
