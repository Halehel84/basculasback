package euroatomizado.idea.gestpes.ticket.incrementos.dominio.entidades;

import euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto.IncrementoFormDto;
import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity()
@Table(name = "incrementos", schema = "dbo")
@Data
public class IncrementoEntity {

    @EmbeddedId
    private IncrementoEntityPk incrementoPk;

    @Transient
    private IncrementoFormDto incrementoFormDto;

}
