package euroatomizado.idea.gestpes.ticket.incrementos.mapeadores;

import euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto.IncrementoFormDto;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto.IncrementoInfoDto;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.entidades.IncrementoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorIncremento {

    @Mapping(source = "incrementoPk.conTipoDoc", target = "conTipoDoc")
    @Mapping(source = "incrementoPk.empresaId", target = "empresaId")
    @Mapping(source = "incrementoPk.seccionId", target = "seccionId")
    @Mapping(source = "incrementoPk.incNumero", target = "incNumero")
    IncrementoInfoDto toInfoDto(IncrementoEntity datos);

    List<IncrementoInfoDto> toInfoDto(List<IncrementoEntity> datos);

    IncrementoEntity toEntidad(IncrementoFormDto dto);

    void actualizar(IncrementoEntity datosNueva, @MappingTarget IncrementoEntity datos);

}