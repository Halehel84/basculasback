package euroatomizado.idea.gestpes.ticket.incrementos.dominio.daos;

import euroatomizado.idea.gestpes.ticket.incrementos.dominio.entidades.IncrementoEntity;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.entidades.IncrementoEntityPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncrementoDao extends CrudRepository<IncrementoEntity, IncrementoEntityPk> {

    List<IncrementoEntity> findAll();

}
