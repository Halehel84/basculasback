package euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto;

import lombok.Data;

@Data
public class IncrementoInfoDto {

    private String empresaId;
    private String conTipoDoc;
    private String seccionId;
    private String incNumero;

}
