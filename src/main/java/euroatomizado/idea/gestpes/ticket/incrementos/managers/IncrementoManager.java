package euroatomizado.idea.gestpes.ticket.incrementos.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto.IncrementoFormDto;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto.IncrementoInfoDto;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.entidades.IncrementoEntity;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.entidades.IncrementoEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class IncrementoManager implements PrimaryKeyStringManagerInterface<IncrementoInfoDto, IncrementoEntity, IncrementoEntityPk, IncrementoFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<IncrementoInfoDto> obtenerIncrementos() {
        return obtenerDatosSalida(daoManager.getIncrementoDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public IncrementoInfoDto guardar(IncrementoFormDto datos) {
        IncrementoEntity almacen = mapperManager.getMapeadorIncremento().toEntidad(datos);
        almacen.setIncrementoFormDto(datos);
        try {
            if (!existe(almacen.getIncrementoPk()))
                return obtenerDatosSalida(guardarNuevo(almacen));
            return obtenerDatosSalida(modificar(almacen));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public IncrementoEntity guardarNuevo(IncrementoEntity nuevo) {
        String id = nuevo.getIncrementoFormDto().getIncNumero();
        nuevo.setIncrementoPk(obtenerPrimaryKey(id));
        daoManager.getIncrementoDao().save(nuevo);
        return nuevo;
    }

    @Override
    public IncrementoEntity modificar(IncrementoEntity nuevo) {
        IncrementoEntity almacen = obtener(nuevo);
        mapperManager.getMapeadorIncremento().actualizar(nuevo, almacen);
        daoManager.getIncrementoDao().save(almacen);
        return almacen;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        IncrementoEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getIncrementoDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public IncrementoInfoDto obtenerDatosSalida(IncrementoEntity almacen) {
        return mapperManager.getMapeadorIncremento().toInfoDto(almacen);
    }

    private List<IncrementoInfoDto> obtenerDatosSalida(List<IncrementoEntity> almacenes) {
        return mapperManager.getMapeadorIncremento().toInfoDto(almacenes);
    }

    @Override
    public boolean existe(IncrementoEntityPk pk) {
        return pk != null && daoManager.getIncrementoDao().exists(pk);
    }

    @Override
    public IncrementoEntity obtener(IncrementoEntity datos) {
        return daoManager.getIncrementoDao().findOne(datos.getIncrementoPk());
    }

    @Override
    public IncrementoEntityPk obtenerPrimaryKey(String id) {
        IncrementoEntityPk pk = new IncrementoEntityPk();
        pk.setIncNumero(id);
        pk.setEmpresaId(EMPRESA);
        return pk;
    }

}
