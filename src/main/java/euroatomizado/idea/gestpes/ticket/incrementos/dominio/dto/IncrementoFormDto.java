package euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class IncrementoFormDto {

    @NotNull
    @Size(min = 1, max = 4)
    private String empresaId;
    @NotNull
    @Size(min = 1, max = 5)
    private String conTipoDoc;
    @NotNull
    @Size(min = 1, max = 2)
    private String seccionId;
    @NotNull
    @Size(min = 1, max = 20)
    private String incNumero;

}
