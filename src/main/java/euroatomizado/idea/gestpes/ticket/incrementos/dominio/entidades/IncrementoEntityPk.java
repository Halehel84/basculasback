package euroatomizado.idea.gestpes.ticket.incrementos.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class IncrementoEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresaId;
    @Column(name = "con_tipo_doc")
    private String conTipoDoc;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "inc_numero")
    private String incNumero;

}
