package euroatomizado.idea.gestpes.ticket.incrementos.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto.IncrementoFormDto;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.dto.IncrementoInfoDto;
import euroatomizado.idea.gestpes.ticket.incrementos.managers.IncrementoManager;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.managers.TicketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/incrementos")
public class IncrementoController {

    @Autowired
    private IncrementoManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<IncrementoInfoDto> obtenerIncrementos() {
        return manager.obtenerIncrementos();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public IncrementoInfoDto crear(@RequestBody @Valid IncrementoFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public IncrementoInfoDto modificar(@RequestBody @Valid IncrementoFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}