package euroatomizado.idea.gestpes.ticket.tickets.managers;

import euroatomizado.idea.gestpes._configuracion.configs.dataSources.EmpresaService;
import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.daos.TicketExpedicionDao;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntity;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades.ContadorEntity;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.FiltrosParrillaSemanalFormDto;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.ParrillaSemanal;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.ParrillaSemanalInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.FiltrosConsultaHistoricoFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntityPk;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.WeekFields;
import java.util.*;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;
import static euroatomizado.idea.gestpes._configuracion.utils.DateUtils.*;

@Service
public class TicketManager implements PrimaryKeyStringManagerInterface<TicketInfoDto, TicketEntity, TicketEntityPk, TicketFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private EmpresaService empresaService;
    private List<AlmacenEntity> listaAlmacenes = new ArrayList<>();
    private String EMPRESA = "NPC";

    @Transactional(rollbackFor = Exception.class)
    public TicketInfoDto obtenerTicket(String ticketId) {
        TicketEntityPk pk = new TicketEntityPk();
        pk.setTicId(ticketId);
        pk.setEmpresaId(EMPRESA);
        pk.setSeccionId(empresaService.getEmpresa());
        TicketInfoDto infoDto = obtenerDatosSalida(daoManager.getTicketDao().findOne(pk));
        TicketExpedicionEntity expedicion = daoManager.getTicketExpedicionDao().obtenerExpedicionPorTicket(ticketId);
        if (expedicion != null) {
            infoDto.setHayExpdicion(true);
        } else {
            infoDto.setHayExpdicion(false);
        }
        return infoDto;
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TicketInfoDto> obtenerTicketsAbiertos() {
        return obtenerDatosSalida(daoManager.getTicketDao().findAllByAlbaranEstadoIsNullAndFechaSalidaIsNullAndTipoTicketIsNull());
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TicketInfoDto> obtenerHistoricoTickets(FiltrosConsultaHistoricoFormDto filtros) {
        obtenerFechasInicioYFinSemana(filtros);
        if (filtros.getInicio() != null) {
            filtros.setInicio(filtros.getInicio().withHour(0).withMinute(0).withSecond(0).withNano(0));
        }
        if (filtros.getFin() != null) {
            filtros.setFin(filtros.getFin().withHour(23).withMinute(59).withSecond(59).withNano(0));
        }
        return obtenerDatosSalida(daoManager.getConsultaHistoricoTicketDao().consultaHistoricoTickets(filtros, em));
    }

    private void obtenerFechasInicioYFinSemana(FiltrosConsultaHistoricoFormDto filtros) {
        if (filtros.getVieneDeParrilla()) {
            LocalDateTime fechaRecibida = filtros.getInicio();
            filtros.setInicio(localDateToLocalDateTime(obtenerFechaPrimerDiaSemana(fechaRecibida.toLocalDate())));
            filtros.setFin(localDateToLocalDateTime(obtenerFechaUltimoDiaSemana(fechaRecibida.toLocalDate())));
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public HashMap obtenerIndicadoresIniciales() {
        LocalDateTime fechaInicioAyer = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0).minusDays(1);
        LocalDateTime fechaFinAyer = fechaInicioAyer.withHour(23).withMinute(59).withSecond(59).withNano(0);
        LocalDateTime fechaInicioSemanaAnterior = localDateToLocalDateTime(obtenerFechaPrimerDiaSemana(LocalDate.now().minusWeeks(1)));
        LocalDateTime fechaFinSemanaAnterior = localDateToLocalDateTime(obtenerFechaUltimoDiaSemana(LocalDate.now().minusWeeks(1))).withHour(23).withMinute(59).withSecond(59).withNano(0);
        LocalDateTime fechaInicioSemanaActual = localDateToLocalDateTime(obtenerFechaPrimerDiaSemana(LocalDate.now()));
        LocalDateTime fechaFinSemanaActual = localDateToLocalDateTime(obtenerFechaUltimoDiaSemana(LocalDate.now())).withHour(23).withMinute(59).withSecond(59).withNano(0);

        List<Integer> articulosPt = daoManager.getArticuloPtDao().obtenerArticulosPorComposicion(6);
        List<String> articulos = new ArrayList<>();
        if (articulosPt.size() > 0) {
            articulos = daoManager.getArticuloPtRelacionDao().obtenerArticulosComposiciones(articulosPt);
        }
        BigDecimal ventasAyer = formatearToneladas(daoManager.getTicketDao().obtenerTotalVentas(fechaInicioAyer, fechaFinAyer, articulos));
        BigDecimal ventasSemanaAnterior = formatearToneladas(daoManager.getTicketDao().obtenerTotalVentas(fechaInicioSemanaAnterior, fechaFinSemanaAnterior, articulos));
        BigDecimal ventasSemanaActual = formatearToneladas(daoManager.getTicketDao().obtenerTotalVentas(fechaInicioSemanaActual, fechaFinSemanaActual, articulos));
        BigDecimal ventasAyerComposicion = formatearToneladas(daoManager.getTicketDao().obtenerVentasComposicion(fechaInicioAyer, fechaFinAyer, articulos));
        BigDecimal ventasSAntComposicion = formatearToneladas(daoManager.getTicketDao().obtenerVentasComposicion(fechaInicioSemanaAnterior, fechaFinSemanaAnterior, articulos));
        BigDecimal ventasSActComposicion = formatearToneladas(daoManager.getTicketDao().obtenerVentasComposicion(fechaInicioSemanaActual, fechaFinSemanaActual, articulos));

        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        Integer numeroSemanaAnterior = fechaInicioSemanaAnterior.get(weekFields.weekOfWeekBasedYear());
        Integer numeroSemanaActual = fechaInicioSemanaActual.get(weekFields.weekOfWeekBasedYear());

        HashMap datos = new HashMap();
        datos.put("ayer", ventasAyer != null ? ventasAyer : new BigDecimal(0));
        datos.put("semanaAnterior", ventasSemanaAnterior != null ? ventasSemanaAnterior : new BigDecimal(0));
        datos.put("semanaActual", ventasSemanaActual != null ? ventasSemanaActual : new BigDecimal(0));
        datos.put("ayerCOMP", ventasAyerComposicion != null ? ventasAyerComposicion : new BigDecimal(0));
        datos.put("semanaAnteriorCOMP", ventasSAntComposicion != null ? ventasSAntComposicion : new BigDecimal(0));
        datos.put("semanaActualCOMP", ventasSActComposicion != null ? ventasSActComposicion : new BigDecimal(0));
        datos.put("numeroSemanaAnterior", numeroSemanaAnterior);
        datos.put("numeroSemanaActual", numeroSemanaActual);
        return datos;
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerVentasAnoAnterior() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0).minusYears(1);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerVentasPorMeses(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerVentasAnoActual() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerVentasPorMeses(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerVentasPorMesAnoAnterior() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0).minusYears(1);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerVentasPorMes(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerVentasPorMesAnoActual() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerVentasPorMes(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerComposicionesAnoAnterior() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0).minusYears(1);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerComposicionesPorMeses(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerComposicionesAnoActual() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerComposicionesPorMeses(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerComposicionesPorMesAnoAnterior() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0).minusYears(1);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerComposicionesPorMes(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<BigDecimal> obtenerComposicionesPorMesAnoActual() {
        LocalDateTime fechaInicio = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
        fechaInicio = fechaInicio.withMonth(1).withDayOfMonth(1);
        LocalDateTime fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);

        return obtenerComposicionesPorMes(fechaInicio, fechaFin);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TicketInfoDto> obtenerTicketsPendientesSincronizar() {
        return obtenerDatosSalida(daoManager.getTicketDao().findAllByAlbaranEstadoIsNullAndTipoTicketIsNullAndFechaSalidaIsNotNull());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TicketInfoDto guardar(TicketFormDto datos) {
        TicketEntity ticket = mapperManager.getMapeadorTicket().toEntidad(datos);
        ticket.setTicketFormDto(datos);
        try {
            if (!existe(ticket.getTicketPk())) {
                return obtenerDatosSalida(guardarNuevo(ticket));
            } else {
                return obtenerDatosSalida(modificar(ticket));
            }
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public TicketEntity guardarNuevo(TicketEntity nuevo) {
        String seccion = empresaService.getEmpresa();
        if (nuevo.getEntidad() == null) {
            nuevo.setEntidad(seccion);
        }
        nuevo.setTicketPk(obtenerPrimaryKey(obtenerIdNuevo(seccion), seccion));
        nuevo.setNumero(nuevo.getTicketPk().getTicId());
        nuevo.setFecha(LocalDateTime.now());
        nuevo.setFechaEntrada(LocalDateTime.now());
        nuevo = asignarRelacionesTicket(nuevo);
        daoManager.getTicketDao().save(nuevo);
        return nuevo;
    }

    @Override
    public TicketEntity modificar(TicketEntity nuevo) {
        TicketEntity almacen = obtener(nuevo);
        mapperManager.getMapeadorTicket().actualizar(nuevo, almacen);
        daoManager.getTicketDao().save(almacen);
        return almacen;
    }

    private TicketEntity asignarRelacionesTicket(TicketEntity ticket) {
        TicketFormDto dto = ticket.getTicketFormDto();
        if (dto.getArticuloId() != null) {
            ticket.setArticulo(daoManager.getArticuloDao().obtenerArticulo(dto.getArticuloId(), EMPRESA));
        }
        if (dto.getClienteId() != null) {
            ticket.setCliente(daoManager.getClienteDao().obtenerCliente(dto.getClienteId(), EMPRESA));
        }
        if (dto.getProveedorId() != null) {
            ticket.setProveedor(daoManager.getProveedorDao().obtenerProveedor(dto.getProveedorId()));
        }
        if (dto.getAlmacenOrigen() != null) {
            ticket.setAlmacenOrigen(daoManager.getAlmacenesDao().obtenerAlmacen(dto.getAlmacenOrigen()).getAlmacenesPk().getAlmacenId());
        }
        if (dto.getTransportistaId() != null) {
            ticket.setTransportista(daoManager.getTransportistaDao().obtenerTransportista(dto.getTransportistaId()));
        }
        return ticket;
    }

    private String obtenerIdNuevo(String seccion) {
        ContadorEntity contador = daoManager.getContadorDao().obtenerContadorTicket(seccion);
        contador.setConContador(contador.getConContador() + 1);
        daoManager.getContadorDao().save(contador);
        String id = contador.getConContador().intValue() + "";
        return id;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        TicketEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getTicketDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public TicketInfoDto obtenerDatosSalida(TicketEntity ticket) {
        TicketInfoDto ticketDto = mapperManager.getMapeadorTicket().toInfoDto(ticket);
        asignarDatosRelacionadosTicket(ticketDto);
        return ticketDto;
    }

    private List<TicketInfoDto> obtenerDatosSalida(List<TicketEntity> tickets) {
        List<TicketInfoDto> lista = mapperManager.getMapeadorTicket().toInfoDto(tickets);
        lista.stream().forEach((ticket) -> {
            asignarDatosRelacionadosTicket(ticket);
        });
        return lista;
    }

    public void asignarDatosRelacionadosTicket(TicketInfoDto ticket) {
        if (ticket.getAlmacenSalida() != null) {
            AlmacenEntity almacenSalida = obtenerAlmacenPorCodigo(ticket.getAlmacenSalida());
            if (almacenSalida != null) {
                ticket.setAlmacenSalidaDescripcion(almacenSalida.getDescripcion());
                ticket.setAlmacenSalida(almacenSalida.getAlmacenesPk().getAlmacenId());
            }
        }
        if (ticket.getAlmacenOrigen() != null) {
            AlmacenEntity almacenOrigen = obtenerAlmacenPorCodigo(ticket.getAlmacenOrigen());
            if (almacenOrigen != null) {
                ticket.setAlmacenOrigenDescripcion(almacenOrigen.getDescripcion());
                ticket.setAlmacenOrigen(almacenOrigen.getAlmacenesPk().getAlmacenId());
            }
        }
    }

    private AlmacenEntity obtenerAlmacenPorCodigo(String almacenId) {
        if (listaAlmacenes == null || listaAlmacenes.size() == 0) {
            listaAlmacenes = daoManager.getAlmacenesDao().findAll();
        }
        List<AlmacenEntity> listaAFiltrar = listaAlmacenes;
        for (AlmacenEntity almacen : listaAFiltrar) {
            if (almacen.getAlmacenesPk().getAlmacenId().equals(almacenId)) {
                return almacen;
            }
        }
        return null;
    }

    @Override
    public boolean existe(TicketEntityPk pk) {
        return pk != null && daoManager.getTicketDao().exists(pk);
    }

    @Override
    public TicketEntity obtener(TicketEntity datos) {
        return daoManager.getTicketDao().findOne(datos.getTicketPk());
    }

    @Override
    public TicketEntityPk obtenerPrimaryKey(String id) {
        TicketEntityPk pk = new TicketEntityPk();
        pk.setTicId(id);
        pk.setEmpresaId(EMPRESA);
        return pk;
    }

    public TicketEntityPk obtenerPrimaryKey(String id, String seccion) {
        TicketEntityPk pk = new TicketEntityPk();
        pk.setTicId(id);
        pk.setSeccionId(seccion);
        pk.setEmpresaId(EMPRESA);
        return pk;
    }

    private List<BigDecimal> obtenerVentasPorMeses(LocalDateTime fechaInicio, LocalDateTime fechaFin) {
        List<BigDecimal> ventas = new ArrayList<BigDecimal>();
        BigDecimal total = BigDecimal.valueOf(0);
        List<Integer> articulosPt = daoManager.getArticuloPtDao().obtenerArticulosPorComposicion(6);
        List<String> articulos = new ArrayList<>();
        if (articulosPt.size() > 0) {
            articulos = daoManager.getArticuloPtRelacionDao().obtenerArticulosComposiciones(articulosPt);
        }
        for (int i = 0; i < 12; i++) {
            BigDecimal valor = formatearToneladas(daoManager.getTicketDao().obtenerTotalVentas(fechaInicio, fechaFin, articulos));
            if (valor != null) {
                if (i < 1)
                    ventas.add(valor);
                else
                    ventas.add(total.add(valor));
                total = total.add(valor);
            } else
                ventas.add(BigDecimal.valueOf(0));
            fechaInicio = fechaInicio.plusMonths(1);
            fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);
        }
        return ventas;
    }

    private List<BigDecimal> obtenerVentasPorMes(LocalDateTime fechaInicio, LocalDateTime fechaFin) {
        List<BigDecimal> ventas = new ArrayList<BigDecimal>();
        List<Integer> articulosPt = daoManager.getArticuloPtDao().obtenerArticulosPorComposicion(6);
        List<String> articulos = new ArrayList<>();
        if (articulosPt.size() > 0) {
            articulos = daoManager.getArticuloPtRelacionDao().obtenerArticulosComposiciones(articulosPt);
        }
        for (int i = 0; i < 12; i++) {
            BigDecimal valor = formatearToneladas(daoManager.getTicketDao().obtenerTotalVentas(fechaInicio, fechaFin, articulos));
            if (valor != null)
                ventas.add(valor);
            else
                ventas.add(BigDecimal.valueOf(0));
            fechaInicio = fechaInicio.plusMonths(1);
            fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);
        }
        return ventas;
    }

    private List<BigDecimal> obtenerComposicionesPorMeses(LocalDateTime fechaInicio, LocalDateTime fechaFin) {
        List<Integer> articulosPt = daoManager.getArticuloPtDao().obtenerArticulosPorComposicion(6);
        List<BigDecimal> ventas = new ArrayList<BigDecimal>();
        if (articulosPt.size() > 0) {
            List<String> articulos = daoManager.getArticuloPtRelacionDao().obtenerArticulosComposiciones(articulosPt);
            BigDecimal total = BigDecimal.valueOf(0);
            for (int i = 0; i < 12; i++) {
                BigDecimal valor = formatearToneladas(daoManager.getTicketDao().obtenerVentasComposicion(fechaInicio, fechaFin, articulos));
                if (valor != null) {
                    if (i < 1)
                        ventas.add(valor);
                    else
                        ventas.add(total.add(valor));
                    total = total.add(valor);
                } else
                    ventas.add(BigDecimal.valueOf(0));
                fechaInicio = fechaInicio.plusMonths(1);
                fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);
            }
        }
        return ventas;
    }

    private List<BigDecimal> obtenerComposicionesPorMes(LocalDateTime fechaInicio, LocalDateTime fechaFin) {
        List<Integer> articulosPt = daoManager.getArticuloPtDao().obtenerArticulosPorComposicion(6);
        List<BigDecimal> ventas = new ArrayList<BigDecimal>();
        if (articulosPt.size() > 0) {
            List<String> articulos = daoManager.getArticuloPtRelacionDao().obtenerArticulosComposiciones(articulosPt);
            for (int i = 0; i < 12; i++) {
                BigDecimal valor = formatearToneladas(daoManager.getTicketDao().obtenerVentasComposicion(fechaInicio, fechaFin, articulos));
                if (valor != null)
                    ventas.add(valor);
                else
                    ventas.add(BigDecimal.valueOf(0));
                fechaInicio = fechaInicio.plusMonths(1);
                fechaFin = fechaInicio.plusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(0);
            }
        }
        return ventas;
    }

    private BigDecimal formatearToneladas(BigDecimal valor) {
        if (valor != null && valor.compareTo(BigDecimal.valueOf(0)) > 0) {
            BigDecimal formateo = valor.divide(BigDecimal.valueOf(1000));
            formateo = formateo.setScale(0, BigDecimal.ROUND_HALF_UP);
            return formateo;
        }
        return valor;
    }

}
