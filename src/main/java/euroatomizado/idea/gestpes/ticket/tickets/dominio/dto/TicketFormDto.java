package euroatomizado.idea.gestpes.ticket.tickets.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class TicketFormDto {

    @Size(max = 50)
    private String ticId;
    @Size(max = 2)
    private String seccionId;
    @Size(max = 2)
    private String entidad;
    @NotNull
    @Size(max = 2)
    private String tipoOperacion;
    @Size(max = 50)
    private String numero;
    private LocalDateTime fecha;
    @Size(max = 20)
    private String clienteId;
    @Size(max = 4)
    private String localId;
    @Size(max = 20)
    private String proveedorId;
    @Size(max = 25)
    private String articuloId;
    @Size(max = 5)
    private String transportistaId;
    @Size(max = 5)
    private String optransportistaId;
    @Size(max = 15)
    private String matricula;
    @Size(max = 15)
    private String matriculaRemolque;
    @Size(max = 5)
    private String conductorId;
    @Size(max = 50)
    private String usuarioId;
    private LocalDateTime fechaEntrada;
    private Long pesoEntrada;
    @Size(max = 10)
    private String basculaEntrada;
    @Size(max = 250)
    private String observaciones;
    @Size(max = 250)
    private String albaranEntrada;
    @Size(max = 250)
    private String pesoAlbaranEntrada;
    @Size(max = 50)
    private String referencia;
    private LocalDateTime fechaSalida;
    private Long pesoSalida;
    @Size(max = 10)
    private String basculaSalida;
    private Long pesoNeto;
    @Size(max = 5)
    private String almacenSalidaId;
    @Size(max = 50)
    private String boxSalida;
    @Size(max = 250)
    private String numeroAlbaran;
    private LocalDateTime albaranFechaCaptura;
    @Size(max = 2)
    private String albaranEstado;
    @Size(max = 50)
    private String numeroBigbagsEntrada;
    @Size(max = 25)
    private String articuloBigbagsEntrada;
    @Size( max = 50)
    private String numeroPaletsEntrada;
    @Size( max = 25)
    private String articuloPaletsEntrada;
    @Size(max = 50)
    private String numeroBigbagsSalida;
    @Size(max = 25)
    private String articuloBigbagsSalida;
    @Size(max = 50)
    private String numeroPaletsSalida;
    @Size(max = 25)
    private String articuloPaletsSalida;
    private Long cargaEstimada;
    private LocalDateTime tiempoCarga;
    @Size(max = 2)
    private String tipoPesaje;
    @Size(max = 200)
    private String clienteDireccion;
    @Size(max = 200)
    private String clientePoblacion;
    @Size(max = 200)
    private String clienteTelefono;
    @Size(max = 200)
    private String clienteFax;
    @Size(max = 200)
    private String proveedorDireccion;
    @Size(max = 200)
    private String proveedorPoblacion;
    @Size(max = 200)
    private String proveedorTelefono;
    @Size(max = 200)
    private String proveedorFax;
    @Size(max = 5)
    private String almacenOrigen;
    private Integer marcaDev;
    private Integer barcoId;
    private Integer contratoId;
    @Size(max = 1)
    private String tipoTicket;
    @Size(max = 50)
    private String indicadorEntradaEnvio;
    @Size(max = 250)
    private String observacionesExpedicion;
    @Size(max = 50)
    private String indicadorSalidaRecepcion;
    @Size(max = 50)
    private String indicadorSalidaEnvio;
    @Size(max = 2)
    private String indicadorSincronizacionCorrecta;
    @Size(max = 50)
    private String sacaId;
    private Integer ordenTransporteId;
    @Size(max = 50)
    private String lote;
    private Integer ubicacionCodigoOrigen;
    private Integer ubicacionCodigoDestino;
    private Float ajuste;

}
