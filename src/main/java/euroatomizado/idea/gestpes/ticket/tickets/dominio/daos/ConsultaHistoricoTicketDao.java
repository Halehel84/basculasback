package euroatomizado.idea.gestpes.ticket.tickets.dominio.daos;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.FiltrosParrillaSemanalFormDto;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.ParrillaSemanal;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.FiltrosConsultaHistoricoFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.QTicketEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntityPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ConsultaHistoricoTicketDao extends CrudRepository<TicketEntity, TicketEntityPk> {

    default List<TicketEntity> consultaHistoricoTickets(FiltrosConsultaHistoricoFormDto filtros, EntityManager em) {
        QTicketEntity ticketEntity = QTicketEntity.ticketEntity;
        /*FROM*/
        JPQLQuery queryPrincipal = new JPAQuery(em);
        queryPrincipal.from(ticketEntity);
        /*WHERE*/
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if (filtros.getInicio() != null && filtros.getFin() != null) {
            booleanBuilder.and(ticketEntity.fecha.between(filtros.getInicio(), filtros.getFin()));
        }
        if (filtros.getAlmacen() != null) {
            booleanBuilder.and(ticketEntity.almacenOrigen.eq(filtros.getAlmacen()));
        }
        if (filtros.getCliente() != null) {
            booleanBuilder.and(ticketEntity.cliente.clientePk.clienteId.eq(filtros.getCliente()));
        }
        if (filtros.getProveedor() != null) {
            booleanBuilder.and(ticketEntity.proveedor.proveedorPk.proveedorId.eq(filtros.getProveedor()));
        }
        if (filtros.getArticulo() != null) {
            booleanBuilder.and(ticketEntity.articulo.articuloPk.articuloId.eq(filtros.getArticulo()));
        }
        if (filtros.getEntidad() != null) {
            booleanBuilder.and(ticketEntity.entidad.eq(filtros.getEntidad()));
        }
        if (filtros.getPlanta() != null) {
            booleanBuilder.and(ticketEntity.seccion.seccionId.eq(filtros.getPlanta()));
        }
        if (filtros.getTipoOperacion() != null) {
            booleanBuilder.and(ticketEntity.tipoOperacion.eq(filtros.getTipoOperacion()));
        }
        if ((filtros.getAlbaranDesde() != null && !filtros.getAlbaranDesde().equals(""))
                && (filtros.getAlbaranHasta() == null || filtros.getAlbaranHasta().equals(""))) {
            booleanBuilder.and(ticketEntity.numeroAlbaran.eq(filtros.getAlbaranDesde()));
        }
        if ((filtros.getAlbaranDesde() == null || filtros.getAlbaranDesde().equals(""))
                && (filtros.getAlbaranHasta() != null && !filtros.getAlbaranHasta().equals(""))) {
            booleanBuilder.and(ticketEntity.numeroAlbaran.eq(filtros.getAlbaranHasta()));
        }
        if (filtros.getAlbaranDesde() != null
                && filtros.getAlbaranHasta() != null
                && !filtros.getAlbaranDesde().equals("")
                && !filtros.getAlbaranHasta().equals("")) {
            booleanBuilder.and(ticketEntity.numeroAlbaran.between(filtros.getAlbaranDesde(), filtros.getAlbaranHasta()));
        }
        if (filtros.getFamilia() != null) {
            booleanBuilder.and(ticketEntity.articulo.familiaId.eq(filtros.getFamilia()));
        }
        if (filtros.getOperadorTransporte() != null) {
            booleanBuilder.and(ticketEntity.operadorTransporte.transportistaPk.transportistaId.eq(filtros.getOperadorTransporte()));
        }
        if (!filtros.getMostrarAlbaranesAnulados()) {
            booleanBuilder.and(ticketEntity.albaranEstado.notIn("A").or(ticketEntity.albaranEstado.isNull()));
        } else if (filtros.getMostrarAlbaranesAnulados()) {
            booleanBuilder.and(ticketEntity.albaranEstado.eq("B").or(ticketEntity.albaranEstado.isNull()));
        }
        booleanBuilder.and(ticketEntity.fechaSalida.isNotNull());
        queryPrincipal.where(booleanBuilder);
        queryPrincipal.select(ticketEntity);
        /*ORDENACION*/
        queryPrincipal.orderBy(ticketEntity.ticketPk.seccionId.asc(), ticketEntity.fecha.asc());
        return queryPrincipal.fetch();
    }
}
