package euroatomizado.idea.gestpes.ticket.tickets.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.FiltrosParrillaSemanalFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.FiltrosConsultaHistoricoFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.managers.TicketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/tickets")
public class TicketController {

    @Autowired
    private TicketManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "{id:\\d+}")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public TicketInfoDto obtenerTicket(@PathVariable String id) {
        return manager.obtenerTicket(id);
    }

    @ResponseStatus(OK)
    @GetMapping(value = "abiertos")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<TicketInfoDto> obtenerTicketsAbiertos() {
        return manager.obtenerTicketsAbiertos();
    }

    @ResponseStatus(OK)
    @GetMapping(value = "indicadores")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public HashMap obtenerIndicadoresIniciales() {
        return manager.obtenerIndicadoresIniciales();
    }

    @ResponseStatus(OK)
    @GetMapping(value = "ventas-ano-anterior")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerVentasAnoAnterior() { return manager.obtenerVentasAnoAnterior(); }

    @ResponseStatus(OK)
    @GetMapping(value = "ventas-ano-actual")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerVentasAnoActual() { return manager.obtenerVentasAnoActual(); }

    @ResponseStatus(OK)
    @GetMapping(value = "ventas-mes-ano-anterior")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerVentasPorMesAnoAnterior() { return manager.obtenerVentasPorMesAnoAnterior(); }

    @ResponseStatus(OK)
    @GetMapping(value = "ventas-mes-ano-actual")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerVentasPorMesAnoActual() { return manager.obtenerVentasPorMesAnoActual(); }

    @ResponseStatus(OK)
    @GetMapping(value = "composiciones-ano-anterior")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerComposicionesAnoAnterior() { return manager.obtenerComposicionesAnoAnterior(); }

    @ResponseStatus(OK)
    @GetMapping(value = "composiciones-ano-actual")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerComposicionesAnoActual() { return manager.obtenerComposicionesAnoActual(); }

    @ResponseStatus(OK)
    @GetMapping(value = "composiciones-mes-ano-anterior")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerComposicionesPorMesAnoAnterior() { return manager.obtenerComposicionesPorMesAnoAnterior(); }

    @ResponseStatus(OK)
    @GetMapping(value = "composiciones-mes-ano-actual")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<BigDecimal> obtenerComposicionesPorMesAnoActual() { return manager.obtenerComposicionesPorMesAnoActual(); }

    @ResponseStatus(OK)
    @GetMapping(value = "pendientes-sincronizar")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<TicketInfoDto> obtenerTicketsPendientesSncronizar() {
        return manager.obtenerTicketsPendientesSincronizar();
    }

    @ResponseStatus(OK)
    @PostMapping(value = "consulta-historico")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<TicketInfoDto> obtenerConsultaHistorico(@RequestBody @Valid FiltrosConsultaHistoricoFormDto datos) {
        return manager.obtenerHistoricoTickets(datos);
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public TicketInfoDto crear(@RequestBody @Valid TicketFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        } catch (NotFoundException e) {
            return new ResponseEntity(NOT_FOUND);
        }
    }

}