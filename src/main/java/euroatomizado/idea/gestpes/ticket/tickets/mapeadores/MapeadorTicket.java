package euroatomizado.idea.gestpes.ticket.tickets.mapeadores;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.sacas.dominio.entidades.SacaEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EstadosAlbaranTicketsConstants.obtenerDescripcionEstado;
import static euroatomizado.idea.gestpes._configuracion.constantes.TiposOperacionTicketsConstants.obtenerDescripcionTipoOperacion;
import static euroatomizado.idea.gestpes._configuracion.constantes.TiposPesajeTicketsConstants.obtenerDescripcionTipoPesaje;

@Mapper(componentModel = "spring")
public interface MapeadorTicket {

    @AfterMapping
    default void convertirDatos(@MappingTarget TicketInfoDto resultado, TicketEntity ticket) {
        //entidad
        if (ticket.getEntidad() != null) {
            resultado.setEntidadDescripcion(ticket.getEntidad().equals("0") ? "NPC" : "EURO");
        }
        //Tipo operacion
        if (ticket.getTipoOperacion() != null) {
            resultado.setTipoOperacionDescripcion(obtenerDescripcionTipoOperacion(ticket.getTipoOperacion()));
        }
        //Tipo pesaje
        if (ticket.getTipoPesaje() != null) {
            resultado.setTipoPesajeDescripcion(obtenerDescripcionTipoPesaje(ticket.getTipoPesaje()));
        }
        //estado albaran
        if (ticket.getAlbaranEstado() != null) {
            resultado.setAlbaranEstadoDescripcion(obtenerDescripcionEstado(ticket.getAlbaranEstado()));
        }
//        if (ticket.getExpedicion() != null) {
//            resultado.setHayExpdicion(true);
//        }
    }

    @Mapping(source = "ticketExpedicionPk.ticketId", target = "ticketExpedicionPk.ticketId")
    @Mapping(source = "ticketExpedicionPk.empresaId", target = "ticketExpedicionPk.empresaId")
    @Mapping(source = "ticketExpedicionPk.seccionId", target = "ticketExpedicionPk.seccionId")
    TicketExpedicionInfoDto toInfoExpedicionDto(TicketExpedicionEntity datos);

    List<TicketExpedicionInfoDto> toInfoExpedicionDto(List<TicketExpedicionEntity> datos);

    @Mapping(source = "ticketPk.ticId", target = "ticId")
    @Mapping(source = "ticketPk.empresaId", target = "empresaId")
    @Mapping(source = "ticketPk.seccionId", target = "seccionId")
    @Mapping(source = "cliente.clientePk.clienteId", target = "clienteId")
    @Mapping(source = "cliente.nombre", target = "clienteDescripcion")
    @Mapping(source = "proveedor.proveedorPk.proveedorId", target = "proveedorId")
    @Mapping(source = "proveedor.nombre", target = "proveedorDescripcion")
    @Mapping(source = "articulo.articuloPk.articuloId", target = "articuloId")
    @Mapping(source = "articulo.descripcion", target = "articuloDescripcion")
    @Mapping(source = "articulo.nombreAbreviado", target = "articuloDescripcionCorta")
    @Mapping(source = "transportista.transportistaPk.transportistaId", target = "transportistaId")
    @Mapping(source = "transportista.nombre", target = "transportistaDescripcion")
    @Mapping(source = "conductor.conductorPk.conductorId", target = "conductorId")
    @Mapping(source = "conductor.nombre", target = "conductorDescripcion")
    @Mapping(source = "conductor.nif", target = "conductorNif")
    @Mapping(source = "contrato.gconCodigo", target = "contratoId")
    @Mapping(source = "ordenTransporte.otmpCodigo", target = "ordenTransporteId")
    @Mapping(source = "saca.sacaId", target = "sacaId")
    @Mapping(source = "usuario.usuId", target = "usuarioId")
    @Mapping(source = "usuario.usuNombre", target = "usuarioNombre")
    @Mapping(source = "seccion.nombre", target = "seccionDescripcion")
    @Mapping(source = "seccion.nombreAbrev", target = "seccionDescripcionCorta")
    @Mapping(source = "operadorTransporte.transportistaPk.transportistaId", target = "operadorTransporteId")
    @Mapping(source = "operadorTransporte.nombre", target = "operadorTransporteDescripcion")
    @Mapping(source = "clienteDireccionEnvio.domicilio", target = "clienteDireccionEnvioDomicilio")
    @Mapping(source = "clienteDireccionEnvio.poblacion", target = "clienteDireccionEnvioPoblacion")
    @Mapping(source = "remolque.camionRemolquePk.matriculaRemolque", target = "matriculaRemolque")
    @Mapping(source = "remolque.ejes", target = "numeroEjes")
    @Mapping(source = "remolque.pma", target = "pma")
    @Mapping(source = "ubicacionDestino.ubiCodigo", target = "ubicacionCodigoDestino")
    @Mapping(source = "ubicacionDestino.ubiDescripcion", target = "ubicacionDescripcionDestino")
    @Mapping(source = "ubicacionOrigen.ubiCodigo", target = "ubicacionCodigoOrigen")
    @Mapping(source = "ubicacionOrigen.ubiDescripcion", target = "ubicacionDescripcionOrigen")
    @Mapping(source = "barco.barcoPk.barcoId", target = "barcoId")
    @Mapping(source = "barco.nombre", target = "barcoDescripcion")
    TicketInfoDto toInfoDto(TicketEntity datos);

    List<TicketInfoDto> toInfoDto(List<TicketEntity> datos);


    @Mapping(source = "clienteId", target = "cliente.clientePk.clienteId")
    @Mapping(source = "articuloId", target = "articulo.articuloPk.articuloId")
    @Mapping(source = "seccionId", target = "seccion.seccionId")
    TicketEntity toEntidad(TicketFormDto dto);

    void actualizar(TicketEntity datosNueva, @MappingTarget TicketEntity datos);

}