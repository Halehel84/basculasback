package euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntity;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades.BarcoEntity;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.entidades.CamionRemolqueEntity;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.entidades.ClienteEnvioEntity;
import euroatomizado.idea.gestpes.maestros.conductores.dominio.entidades.ConductorEntity;
import euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.entidades.GestpesContratosEntity;
import euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades.ProveedorEntity;
import euroatomizado.idea.gestpes.maestros.secciones.dominio.entidades.SeccionEntity;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades.TransportistaEntity;
import euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.entidades.UbicacionEntity;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.entidades.OrdenesTransporteMPEntity;
import euroatomizado.idea.gestpes.sacas.dominio.entidades.SacaEntity;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.entidades.UsuarioEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity()
@Table(name = "tickets", schema = "dbo")
@Data
public class TicketEntity {

    @EmbeddedId
    private TicketEntityPk ticketPk;
    @Column(name = "xentidad")
    private String entidad;
    @Column(name = "tic_tipo_op")
    private String tipoOperacion;
    @Column(name = "tic_numero")
    private String numero;
    @Column(name = "tic_fecha")
    private LocalDateTime fecha;
    @Column(name = "xmatricula")
    private String matricula;
    @Column(name = "tic_fecha_entrada")
    private LocalDateTime fechaEntrada;
    @Column(name = "tic_peso_entrada")
    private Long pesoEntrada;
    @Column(name = "tic_bascula_entrada")
    private String basculaEntrada;
    @Column(name = "tic_observaciones")
    private String observaciones;
    @Column(name = "tic_alb_entrada")
    private String albaranEntrada;
    @Column(name = "tic_peso_alb_entrada")
    private String pesoAlbaranEntrada;
    @Column(name = "tic_referencia")
    private String referencia;
    @Column(name = "tic_fecha_salida")
    private LocalDateTime fechaSalida;
    @Column(name = "tic_peso_salida")
    private Long pesoSalida;
    @Column(name = "tic_bascula_salida")
    private String basculaSalida;
    @Column(name = "tic_peso_neto")
    private Long pesoNeto;
    @Column(name = "tic_box_salida")
    private String boxSalida;
    @Column(name = "tic_n_albaran")
    private String numeroAlbaran;
    @Column(name = "tic_alb_fecha_captura")
    private LocalDateTime albaranFechaCaptura;
    @Column(name = "tic_alb_estado")
    private String albaranEstado;
    @Column(name = "tic_n_bigbags_entrada")
    private String numeroBigbagsEntrada;
    @Column(name = "tic_a_bigbags_entrada")
    private String articuloBigbagsEntrada;
    @Column(name = "tic_n_palets_entrada")
    private String numeroPaletsEntrada;
    @Column(name = "tic_a_palets_entrada")
    private String articuloPaletsEntrada;
    @Column(name = "tic_n_bigbags_salida")
    private String numeroBigbagsSalida;
    @Column(name = "tic_a_bigbags_salida")
    private String articuloBigbagsSalida;
    @Column(name = "tic_n_palets_salida")
    private String numeroPaletsSalida;
    @Column(name = "tic_a_palets_salida")
    private String articuloPaletsSalida;
    @Column(name = "tic_carga_estimada")
    private Long cargaEstimada;
    @Column(name = "tic_tiempo_carga")
    private LocalDateTime tiempoCarga;
    @Column(name = "tic_tipo_pesaje")
    private String tipoPesaje;
    @Column(name = "xcliente_desc")
    private String clienteDescripcion;
    @Column(name = "xproveedor_desc")
    private String proveedorDescripcion;
    @Column(name = "xarticulo_desc")
    private String articuloDescripcion;
    @Column(name = "xcliente_direccion")
    private String clienteDireccion;
    @Column(name = "xcliente_poblacion")
    private String clientePoblacion;
    @Column(name = "xcliente_telefono")
    private String clienteTelefono;
    @Column(name = "xcliente_fax")
    private String clienteFax;
    @Column(name = "xproveedor_direccion")
    private String proveedorDireccion;
    @Column(name = "xproveedor_poblacion")
    private String proveedorPoblacion;
    @Column(name = "xproveedor_telefono")
    private String proveedorTelefono;
    @Column(name = "xproveedor_fax")
    private String proveedorFax;
    @Column(name = "xalmacen_origen")
    private String almacenOrigen;
    @Column(name = "tic_almacen_salida")
    private String almacenSalida;
    @Column(name = "xmarca_dev")
    private Integer marcaDev;
    @Column(name = "tic_tipo_ticket")
    private String tipoTicket;
    @Column(name = "tic_observaciones_expedicion")
    private String observacionesExpedicion;
    @Column(name = "tic_indicador_entrada_envio")
    private String indicadorEntradaEnvio;
    @Column(name = "tic_indicador_salida_recepcion")
    private String indicadorSalidaRecepcion;
    @Column(name = "tic_indicador_salida_envio")
    private String indicadorSalidaEnvio;
    @Column(name = "tic_indicador_sincronizacion_correcta")
    private String indicadorSincronizacionCorrecta;
    @Column(name = "otmp_lote")
    private String lote;
    @Column(name = "tic_ajuste", columnDefinition = "real")
    private Float ajuste;
    @Column(name = "xcliente_id")
    private String clienteId;
    @Column(name = "xarticulo_id")
    private String articuloId;
    @Column(name = "xtransportista_id")
    private String transportistaId;
    @Column(name = "xlocal_id")
    private String localId;
    @Column(name = "xproveedor_id")
    private String proveedorId;
    @Column(name = "xoptransportista_id")
    private String operarioTransporteId;
    @Column(name = "xconductor_id")
    private String conductorId;
    @Column(name = "xmatricula_rem")
    private String matriculaRemolque;
    @Column(name = "xbarco_id")
    private String barcoId;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xcliente_id", referencedColumnName = "xcliente_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private ClienteEntity cliente;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xcliente_id", referencedColumnName = "xcliente_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false),
            @JoinColumn(name = "xlocal_id", referencedColumnName = "xlocal_id", insertable = false, updatable = false)
    })
    private ClienteEnvioEntity clienteDireccionEnvio;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xarticulo_id", referencedColumnName = "xarticulo_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private ArticuloEntity articulo;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xproveedor_id", referencedColumnName = "xproveedor_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private ProveedorEntity proveedor;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xtransportista_id", referencedColumnName = "xtransportista_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private TransportistaEntity transportista;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xoptransportista_id", referencedColumnName = "xtransportista_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private TransportistaEntity operadorTransporte;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xconductor_id", referencedColumnName = "xconductor_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private ConductorEntity conductor;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xmatricula_rem", referencedColumnName = "xmatricula_rem", insertable = false, updatable = false),
            @JoinColumn(name = "xmatricula", referencedColumnName = "xmatricula", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private CamionRemolqueEntity remolque;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xbarco_id", referencedColumnName = "xbarco_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private BarcoEntity barco;

    @ManyToOne
    @JoinColumn(name = "gcon_codigo")
    private GestpesContratosEntity contrato;
    @ManyToOne
    @JoinColumn(name = "otmp_codigo")
    private OrdenesTransporteMPEntity ordenTransporte;
    @ManyToOne
    @JoinColumn(name = "sac_id")
    private SacaEntity saca;
    @ManyToOne
    @JoinColumn(name = "tic_usu_id")
    private UsuarioEntity usuario;
    @ManyToOne
    @JoinColumn(name = "xseccion_id", referencedColumnName = "xseccion_id", insertable = false, updatable = false)
    private SeccionEntity seccion;
    @ManyToOne
    @JoinColumn(name = "ubi_codigo_destino")
    private UbicacionEntity ubicacionDestino;
    @ManyToOne
    @JoinColumn(name = "ubi_codigo_origen")
    private UbicacionEntity ubicacionOrigen;
    /*
    @OneToOne(mappedBy = "ticket", fetch = FetchType.LAZY)
    private TicketExpedicionEntity expedicion;
    */
    @Transient
    private TicketFormDto ticketFormDto;

}