package euroatomizado.idea.gestpes.ticket.tickets.dominio.daos;

import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TicketDao extends CrudRepository<TicketEntity, TicketEntityPk> {

    List<TicketEntity> findAll();
    List<TicketEntity> findAllByAlbaranEstadoIsNullAndFechaSalidaIsNullAndTipoTicketIsNull();
    List<TicketEntity> findAllByAlbaranEstadoIsNullAndTipoTicketIsNullAndFechaSalidaIsNotNull();

    @Query(" select count(tic) " +
            "from TicketEntity tic where tic.tipoOperacion in ('C','R') " +
            "and tic.fecha between :inicio and :fin ")
    Integer obtenerTotalEntradas(@Param("inicio") LocalDateTime inicio, @Param("fin") LocalDateTime fin);

    @Query(" select count(tic) " +
            "from TicketEntity tic where tic.tipoOperacion in ('S','V') " +
            "and tic.fecha between :inicio and :fin ")
    Integer obtenerTotalSalidas(@Param("inicio") LocalDateTime inicio, @Param("fin") LocalDateTime fin);

    @Query(" select sum(tic.pesoNeto) " +
            "from TicketEntity tic where tic.tipoOperacion in ('C','R') " +
            "and tic.fecha between :inicio and :fin ")
    BigDecimal obtenerTotalCompras(@Param("inicio") LocalDateTime inicio, @Param("fin") LocalDateTime fin);

    @Query(" select sum(tic.pesoNeto) " +
            "from TicketEntity tic where tic.tipoOperacion in ('V') " +
            "and (tic.albaranEstado not in ('A','B') or tic.albaranEstado is null) " +
            "and tic.fecha between :inicio and :fin " +
            "and tic.numeroAlbaran is not null " +
            "and tic.articulo.familiaId = 'PT' " +
            "and tic.articulo.articuloPk.articuloId not in :articulos " )
    BigDecimal obtenerTotalVentas(@Param("inicio") LocalDateTime inicio,
                                  @Param("fin") LocalDateTime fin,
                                  @Param("articulos") List<String> articulos);

    @Query(" select tic " +
            "from TicketEntity tic where tic.saca.sacaId = :saca ")
    List<TicketEntity> obtenerTicketsPorSaca(@Param("saca") String saca);

    @Query(" select tic " +
            "from TicketEntity tic where tic.articulo.articuloPk.articuloId in (articulos) ")
    List<TicketEntity> obtenerTicketsPorArticulos(@Param("articulos") List<String> articulos);

    @Query(" select sum(tic.pesoNeto) " +
            "from TicketEntity tic where tic.tipoOperacion in ('S','V') " +
            "and tic.albaranEstado in ('C','M') " +
            "and tic.fecha between :inicio and :fin " +
            "and tic.articulo.articuloPk.articuloId in :articulos " )
    BigDecimal obtenerVentasComposicion(@Param("inicio") LocalDateTime inicio,
                                        @Param("fin") LocalDateTime fin,
                                        @Param("articulos") List<String> articulos);

    @Query(" select max(tic.albaranFechaCaptura) " +
            "from TicketEntity tic ")
    LocalDateTime obtenerUltimaFechaCaptura();

}
