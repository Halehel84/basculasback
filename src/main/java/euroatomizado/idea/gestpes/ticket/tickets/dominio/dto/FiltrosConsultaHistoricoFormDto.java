package euroatomizado.idea.gestpes.ticket.tickets.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class FiltrosConsultaHistoricoFormDto {

    private String planta;
    private String entidad;
    @NotNull
    @Size(min = 1, max = 4)
    private String tipoOperacion;
    @NotNull
    private LocalDateTime inicio;
    private LocalDateTime fin;
    private String cliente;
    private String proveedor;
    private String almacen;
    private String articulo;
    private String albaranDesde;
    private String albaranHasta;
    private String familia;
    private String operadorTransporte;
    private Boolean mostrarAlbaranesAnulados;
    private Boolean vieneDeParrilla;
}
