package euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.daos;

import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.entidades.LineaTicketEntity;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.entidades.LineaTicketEntityPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LineaTicketDao extends CrudRepository<LineaTicketEntity, LineaTicketEntityPk> {

    List<LineaTicketEntity> findAll();

}
