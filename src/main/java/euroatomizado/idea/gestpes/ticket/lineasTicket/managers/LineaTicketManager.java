package euroatomizado.idea.gestpes.ticket.lineasTicket.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyIntegerManagerInterface;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto.LineaTicketFormDto;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto.LineaTicketInfoDto;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.entidades.LineaTicketEntity;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.entidades.LineaTicketEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class LineaTicketManager implements PrimaryKeyIntegerManagerInterface<LineaTicketInfoDto, LineaTicketEntity, LineaTicketEntityPk, LineaTicketFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<LineaTicketInfoDto> obtenerLineaTickets() {
        return obtenerDatosSalida(daoManager.getLineaTicketDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public LineaTicketInfoDto guardar(LineaTicketFormDto datos) {
        LineaTicketEntity almacen = mapperManager.getMapeadorLineaTicket().toEntidad(datos);
        almacen.setLineaTicketFormDto(datos);
        try {
            if (!existe(almacen.getLineaTicketPk()))
                return obtenerDatosSalida(guardarNuevo(almacen));
            return obtenerDatosSalida(modificar(almacen));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public LineaTicketEntity guardarNuevo(LineaTicketEntity nuevo) {
        Integer id = nuevo.getLineaTicketFormDto().getLineaId();
        nuevo.setLineaTicketPk(obtenerPrimaryKey(id));
        daoManager.getLineaTicketDao().save(nuevo);
        return nuevo;
    }

    @Override
    public LineaTicketEntity modificar(LineaTicketEntity nuevo) {
        LineaTicketEntity almacen = obtener(nuevo);
        mapperManager.getMapeadorLineaTicket().actualizar(nuevo, almacen);
        daoManager.getLineaTicketDao().save(almacen);
        return almacen;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(Integer id) {
        LineaTicketEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getLineaTicketDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public LineaTicketInfoDto obtenerDatosSalida(LineaTicketEntity almacen) {
        return mapperManager.getMapeadorLineaTicket().toInfoDto(almacen);
    }

    private List<LineaTicketInfoDto> obtenerDatosSalida(List<LineaTicketEntity> almacenes) {
        return mapperManager.getMapeadorLineaTicket().toInfoDto(almacenes);
    }

    @Override
    public boolean existe(LineaTicketEntityPk pk) {
        return pk != null && daoManager.getLineaTicketDao().exists(pk);
    }

    @Override
    public LineaTicketEntity obtener(LineaTicketEntity datos) {
        return daoManager.getLineaTicketDao().findOne(datos.getLineaTicketPk());
    }

    @Override
    public LineaTicketEntityPk obtenerPrimaryKey(Integer id) {
        LineaTicketEntityPk pk = new LineaTicketEntityPk();
        pk.setLineaId(id);
        pk.setEmpresaId(EMPRESA);
        return pk;
    }

}
