package euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.entidades;

import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto.LineaTicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "lineas_tickets", schema = "dbo")
@Data
public class LineaTicketEntity {

    @EmbeddedId
    private LineaTicketEntityPk lineaTicketPk;
    @Column(name = "lin_fecha")
    private LocalDateTime linFecha;
    @Column(name = "xmatricula")
    private String matricula;
    @Column(name = "xmatricula_rem")
    private String matriculaRem;
    @Column(name = "xconductor_id")
    private String conductorId;
    @Column(name = "linea_usu_id")
    private String lineaUsuId;
    @Column(name = "lin_peso_neto", columnDefinition = "real")
    private Float linPesoNeto;
    @Column(name = "lin_fecha_entrada")
    private LocalDateTime linFechaEntrada;
    @Column(name = "lin_fecha_salida")
    private LocalDateTime linFechaSalida;
    @Column(name = "lin_alb_entrada")
    private String linAlbEntrada;
    @Column(name = "lin_anulada")
    private String linAnulada;
    @Column(name = "lin_fecha_anulacion")
    private LocalDateTime linFechaAnulacion;
    @Column(name = "lin_usu_anulacion")
    private String linUsuAnulacion;

    @Transient
    private LineaTicketFormDto LineaTicketFormDto;

}