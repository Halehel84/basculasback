package euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class LineaTicketFormDto {

    @NotNull
    @Size(min = 1, max = 4)
    private String empresaId;
    @NotNull
    @Size(min = 1, max = 50)
    private String ticId;
    @NotNull
    @Size(min = 1, max = 2)
    private String seccionId;
    @NotNull
    private Integer lineaId;
    private LocalDateTime linFecha;
    @Size(min = 1, max = 15)
    private String matricula;
    @Size(min = 1, max = 15)
    private String matriculaRem;
    @Size(min = 1, max = 5)
    private String conductorId;
    @NotNull
    @Size(min = 1, max = 50)
    private String lineaUsuId;
    @NotNull
    private Float linPesoNeto;
    private LocalDateTime linFechaEntrada;
    private LocalDateTime linFechaSalida;
    @Size(min = 1, max = 250)
    private String linAlbEntrada;
    @Size(min = 1, max = 2)
    private String linAnulada;
    private LocalDateTime linFechaAnulacion;
    @Size(min = 1, max = 50)
    private String linUsuAnulacion;

}
