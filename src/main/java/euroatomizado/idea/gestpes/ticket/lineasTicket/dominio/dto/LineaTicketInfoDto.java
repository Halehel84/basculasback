package euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LineaTicketInfoDto {

    private String empresaId;
    private String ticId;
    private String seccionId;
    private Integer lineaId;
    private LocalDateTime linFecha;
    private String matricula;
    private String matriculaRem;
    private String conductorId;
    private String lineaUsuId;
    private Float linPesoNeto;
    private LocalDateTime linFechaEntrada;
    private LocalDateTime linFechaSalida;
    private String linAlbEntrada;
    private String linAnulada;
    private LocalDateTime linFechaAnulacion;
    private String linUsuAnulacion;

}
