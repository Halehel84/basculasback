package euroatomizado.idea.gestpes.ticket.lineasTicket.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto.LineaTicketFormDto;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto.LineaTicketInfoDto;
import euroatomizado.idea.gestpes.ticket.lineasTicket.managers.LineaTicketManager;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.managers.TicketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/lineas-tickets")
public class LineaTicketController {

    @Autowired
    private LineaTicketManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<LineaTicketInfoDto> obtenerLineaTickets() {
        return manager.obtenerLineaTickets();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public LineaTicketInfoDto crear(@RequestBody @Valid LineaTicketFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public LineaTicketInfoDto modificar(@RequestBody @Valid LineaTicketFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable Integer id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}