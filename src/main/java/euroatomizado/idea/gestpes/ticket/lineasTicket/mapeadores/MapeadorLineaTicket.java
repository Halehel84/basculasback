package euroatomizado.idea.gestpes.ticket.lineasTicket.mapeadores;

import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto.LineaTicketFormDto;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.dto.LineaTicketInfoDto;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.entidades.LineaTicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorLineaTicket {

    @Mapping(source = "lineaTicketPk.ticId", target = "ticId")
    @Mapping(source = "lineaTicketPk.empresaId", target = "empresaId")
    @Mapping(source = "lineaTicketPk.seccionId", target = "seccionId")
    @Mapping(source = "lineaTicketPk.lineaId", target = "lineaId")
    LineaTicketInfoDto toInfoDto(LineaTicketEntity datos);

    List<LineaTicketInfoDto> toInfoDto(List<LineaTicketEntity> datos);

    LineaTicketEntity toEntidad(LineaTicketFormDto dto);

    void actualizar(LineaTicketEntity datosNueva, @MappingTarget LineaTicketEntity datos);

}