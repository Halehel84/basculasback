package euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.entidades;

import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto.AlbaranEstadoFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "albaranes_estados", schema = "dbo")
@Data
public class AlbaranEstadoEntity {

    @Id
    @Column(name = "ale_id")
    private String aleId;
    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "tic_id")
    private String ticId;
    @Column(name = "ale_fecha_inicio")
    private LocalDateTime aleFechaInicio;
    @Column(name = "ale_estado")
    private String aleEstado;
    @Column(name = "ale_fecha_fin")
    private LocalDateTime aleFechaFin;

    @Transient
    private AlbaranEstadoFormDto albaranEstadoFormDto;

}
