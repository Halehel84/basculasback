package euroatomizado.idea.gestpes.ticket.albaranesEstados.mapeadores;

import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto.AlbaranEstadoFormDto;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto.AlbaranEstadoInfoDto;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.entidades.AlbaranEstadoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorAlbaranEstado {

    AlbaranEstadoInfoDto toInfoDto(AlbaranEstadoEntity datos);

    List<AlbaranEstadoInfoDto> toInfoDto(List<AlbaranEstadoEntity> datos);

    AlbaranEstadoEntity toEntidad(AlbaranEstadoFormDto dto);

    void actualizar(AlbaranEstadoEntity datosNueva, @MappingTarget AlbaranEstadoEntity datos);

}