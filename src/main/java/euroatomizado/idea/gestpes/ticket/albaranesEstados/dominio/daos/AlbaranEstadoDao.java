package euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.daos;

import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.entidades.AlbaranEstadoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AlbaranEstadoDao extends CrudRepository<AlbaranEstadoEntity, String> {

    List<AlbaranEstadoEntity> findAll();

}
