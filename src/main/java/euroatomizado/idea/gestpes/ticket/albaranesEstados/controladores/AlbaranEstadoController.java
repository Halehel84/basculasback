package euroatomizado.idea.gestpes.ticket.albaranesEstados.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto.AlbaranEstadoFormDto;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto.AlbaranEstadoInfoDto;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.managers.AlbaranEstadoManager;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.managers.TicketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/albaranes-estados")
public class AlbaranEstadoController {

    @Autowired
    private AlbaranEstadoManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<AlbaranEstadoInfoDto> obtenerAlbaranEstados() {
        return manager.obtenerAlbaranEstados();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public AlbaranEstadoInfoDto crear(@RequestBody @Valid AlbaranEstadoFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public AlbaranEstadoInfoDto modificar(@RequestBody @Valid AlbaranEstadoFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}