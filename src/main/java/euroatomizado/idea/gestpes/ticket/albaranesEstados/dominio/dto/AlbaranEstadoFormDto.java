package euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class AlbaranEstadoFormDto {


    @NotNull
    @Size(min = 1, max = 4)
    private String aleId;
    @NotNull
    @Size(min = 1, max = 50)
    private String empresa;
    @NotNull
    @Size(min = 1, max = 50)
    private String ticId;
    @NotNull
    private LocalDateTime aleFechaInicio;
    @NotNull
    @Size(min = 1, max = 50)
    private String aleEstado;
    private LocalDateTime aleFechaFin;

}
