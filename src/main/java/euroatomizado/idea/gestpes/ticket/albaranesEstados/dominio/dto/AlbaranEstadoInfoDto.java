package euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AlbaranEstadoInfoDto {

    private String aleId;
    private String empresa;
    private String ticId;
    private LocalDateTime aleFechaInicio;
    private String aleEstado;
    private LocalDateTime aleFechaFin;

}
