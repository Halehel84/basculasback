package euroatomizado.idea.gestpes.ticket.albaranesEstados.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto.AlbaranEstadoFormDto;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.dto.AlbaranEstadoInfoDto;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.entidades.AlbaranEstadoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class AlbaranEstadoManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<AlbaranEstadoInfoDto> obtenerAlbaranEstados() {
        return obtenerDatosSalida(daoManager.getAlbaranEstadoDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public AlbaranEstadoInfoDto guardar(AlbaranEstadoFormDto datos) {
        AlbaranEstadoEntity entity = mapperManager.getMapeadorAlbaranEstado().toEntidad(datos);
        entity.setAlbaranEstadoFormDto(datos);
        try {
            if (!existe(entity.getAleId()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    public AlbaranEstadoEntity guardarNuevo(AlbaranEstadoEntity nuevo) {
        String id = nuevo.getAlbaranEstadoFormDto().getAleId();
        nuevo.setAleId(id);
        nuevo.setEmpresa(EMPRESA);
        daoManager.getAlbaranEstadoDao().save(nuevo);
        return nuevo;
    }

    public AlbaranEstadoEntity modificar(AlbaranEstadoEntity nuevo) {
        AlbaranEstadoEntity entity = obtener(nuevo);
        mapperManager.getMapeadorAlbaranEstado().actualizar(nuevo, entity);
        daoManager.getAlbaranEstadoDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        if (existe(id))
            daoManager.getAlbaranEstadoDao().delete(id);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    public AlbaranEstadoInfoDto obtenerDatosSalida(AlbaranEstadoEntity entity) {
        return mapperManager.getMapeadorAlbaranEstado().toInfoDto(entity);
    }

    private List<AlbaranEstadoInfoDto> obtenerDatosSalida(List<AlbaranEstadoEntity> entidades) {
        return mapperManager.getMapeadorAlbaranEstado().toInfoDto(entidades);
    }

    public boolean existe(String id) {
        return id != null && daoManager.getAlbaranEstadoDao().exists(id);
    }

    public AlbaranEstadoEntity obtener(AlbaranEstadoEntity datos) {
        return daoManager.getAlbaranEstadoDao().findOne(datos.getAleId());
    }

}

