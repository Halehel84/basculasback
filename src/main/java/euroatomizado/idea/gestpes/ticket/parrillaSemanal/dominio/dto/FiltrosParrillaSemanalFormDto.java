package euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class FiltrosParrillaSemanalFormDto {

    private String planta;
    private String entidad;
    @NotNull
    @Size(min = 1, max = 4)
    private String tipoOperacion;
    @NotNull
    private LocalDate semana;
    private String cliente;
    private String proveedor;
    private String almacen;
    private String articulo;
    private String familia;
    private String agrupado;
    private Boolean mostrarAlbaranesAnulados;
    private LocalDateTime primerDiaSemana;
    private LocalDateTime ultimoDiaSemana;
    private Boolean masDeUnaPlanta;
}
