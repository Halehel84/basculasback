package euroatomizado.idea.gestpes.ticket.parrillaSemanal.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.SeccionesConstants.obtenerDescripcionSeccion;
import static euroatomizado.idea.gestpes._configuracion.utils.DateUtils.*;

@Service
public class ParrillaSemanalManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;
    @PersistenceContext
    private EntityManager em;

    @Transactional(rollbackFor = Exception.class)
    public ParrillaSemanalInfoDto obtenerParrillaSemanal(FiltrosParrillaSemanalFormDto filtros, Boolean hayMasDeUnaPlanta) {
        obtenerFechasSemana(filtros);
        Boolean agrupadoArticulo = false;
        Boolean agrupadoCliente = false;
        Boolean agrupadoProveedor = false;
        Boolean agrupadoAlmacen = false;
        if (filtros.getAgrupado().equals("C")) {
            agrupadoCliente = true;
        } else if (filtros.getAgrupado().equals("A")) {
            agrupadoArticulo = true;
        } else if (filtros.getAgrupado().equals("P")) {
            agrupadoProveedor = true;
        } else if (filtros.getAgrupado().equals("L")) {
            agrupadoAlmacen = true;
        }
        List<ParrillaSemanal> datos = daoManager.getParrillaSemanalDao().obtenerParrillaSemanal(filtros, em, agrupadoArticulo, agrupadoCliente, agrupadoProveedor, agrupadoAlmacen);
        return generarParrillaSemanal(datos, filtros, hayMasDeUnaPlanta);
    }

    private void obtenerFechasSemana(FiltrosParrillaSemanalFormDto filtros) {
        LocalDate fechaRecibida = filtros.getSemana();
        filtros.setPrimerDiaSemana(localDateToLocalDateTime(obtenerFechaPrimerDiaSemana(fechaRecibida)));
        filtros.setUltimoDiaSemana(localDateToLocalDateTime(obtenerFechaUltimoDiaSemana(fechaRecibida)));
    }

    private ParrillaSemanalInfoDto generarParrillaSemanal(List<ParrillaSemanal> datos, FiltrosParrillaSemanalFormDto filtros, Boolean hayMasDeUnaPlanta) {
        ParrillaSemanalInfoDto parrilla = new ParrillaSemanalInfoDto();
        parrilla = generarPadresParrilla(datos, parrilla, filtros, hayMasDeUnaPlanta);
        generarRegistrosHijos(parrilla.getLineas(), filtros);
        return parrilla;
    }

    private void generarRegistrosPlanta(List<ParrillaSemanalLineasInfoDto> lista, Boolean hayMasDeUnaPlanta, String codigoSeccion) {
        if (hayMasDeUnaPlanta) {
            ParrillaSemanalLineasInfoDto planta = new ParrillaSemanalLineasInfoDto();
            String seccionDescripcion = obtenerDescripcionSeccion(codigoSeccion);
            planta.setCodigo(seccionDescripcion);
            planta.setDescripcion(seccionDescripcion);
            lista.add(planta);
            hayMasDeUnaPlanta = true;
        }
    }

    private ParrillaSemanalInfoDto generarPadresParrilla(List<ParrillaSemanal> datos, ParrillaSemanalInfoDto parrilla, FiltrosParrillaSemanalFormDto filtros, Boolean hayMasDeUnaPlanta) {
        ParrillaSemanalTotalInfoDto parrillaTotal = iniciarParrillaSemanalTotales();
        ParrillaSemanalLineasInfoDto registroSemanal = new ParrillaSemanalLineasInfoDto();
        List<ParrillaSemanalLineasInfoDto> lista = new ArrayList<>();
        String codigoRegistroAnterior = "";
        for (ParrillaSemanal registro : datos) {
            String codigo = registro.getPlanta() + "-" + registro.getCodigo();
            if (codigoRegistroAnterior.equals("")) {
                registroSemanal.setDescripcion(registro.getDescripcion());
                registroSemanal.setCodigo(codigo);
                registroSemanal.setPlanta(obtenerDescripcionSeccion(registro.getPlanta()));
                registroSemanal.setHijo(false);
                generarRegistrosPlanta(lista, hayMasDeUnaPlanta, registro.getPlanta());
                if (hayMasDeUnaPlanta) {
                    registroSemanal.setRelacion(obtenerDescripcionSeccion(registro.getPlanta()));
                }
                codigoRegistroAnterior = codigo;
                anyadirValorEnDiaSemana(registro, registroSemanal, parrillaTotal);
            } else if (codigoRegistroAnterior.equals(codigo)) {
                anyadirValorEnDiaSemana(registro, registroSemanal, parrillaTotal);
            } else if (!codigoRegistroAnterior.equals(codigo)) {
                lista.add(registroSemanal);
                registroSemanal = new ParrillaSemanalLineasInfoDto();
                codigoRegistroAnterior = codigo;
                registroSemanal.setDescripcion(registro.getDescripcion());
                registroSemanal.setCodigo(codigo);
                registroSemanal.setPlanta(obtenerDescripcionSeccion(registro.getPlanta()));
                registroSemanal.setHijo(false);
                if (hayMasDeUnaPlanta) {
                    registroSemanal.setRelacion(obtenerDescripcionSeccion(registro.getPlanta()));
                }
                anyadirValorEnDiaSemana(registro, registroSemanal, parrillaTotal);
            }
        }
        if (datos.size() > 0) {
            lista.add(registroSemanal);
            parrilla.setLineas(lista);
            actualizarTotalSemana(parrillaTotal);
            parrilla.setTotales(parrillaTotal);
        }
        return parrilla;
    }

    private void anyadirValorEnDiaSemana(ParrillaSemanal registro, ParrillaSemanalLineasInfoDto registroParrilla, ParrillaSemanalTotalInfoDto parrillaTotal) {
        DayOfWeek diaSemana = registro.getFecha().getDayOfWeek();
        BigDecimal toneladasDia = (registro.getValor() != null) ? new BigDecimal(registro.getValor()) : new BigDecimal(0);
        // Actualizamos el total del registro
        BigDecimal total;
        if (registroParrilla.getTotal() != null) {
            total = registroParrilla.getTotal().add(toneladasDia);
        } else {
            registroParrilla.setTotal(new BigDecimal(0));
            total = registroParrilla.getTotal().add(toneladasDia);
        }
        registroParrilla.setTotal(total);
        // Asignamos las toneladas al dia correspondiente
        if (diaSemana.getValue() == 1) {
            registroParrilla.setLunes(toneladasDia);
            if (parrillaTotal != null) parrillaTotal.setLunes(parrillaTotal.getLunes().add(toneladasDia));
        } else if (diaSemana.getValue() == 2) {
            registroParrilla.setMartes(toneladasDia);
            if (parrillaTotal != null) parrillaTotal.setMartes(parrillaTotal.getMartes().add(toneladasDia));
        } else if (diaSemana.getValue() == 3) {
            registroParrilla.setMiercoles(toneladasDia);
            if (parrillaTotal != null) parrillaTotal.setMiercoles(parrillaTotal.getMiercoles().add(toneladasDia));
        } else if (diaSemana.getValue() == 4) {
            registroParrilla.setJueves(toneladasDia);
            if (parrillaTotal != null) parrillaTotal.setJueves(parrillaTotal.getJueves().add(toneladasDia));
        } else if (diaSemana.getValue() == 5) {
            registroParrilla.setViernes(toneladasDia);
            if (parrillaTotal != null) parrillaTotal.setViernes(parrillaTotal.getViernes().add(toneladasDia));
        } else if (diaSemana.getValue() == 6) {
            registroParrilla.setSabado(toneladasDia);
            if (parrillaTotal != null) parrillaTotal.setSabado(parrillaTotal.getSabado().add(toneladasDia));
        } else if (diaSemana.getValue() == 7) {
            registroParrilla.setDomingo(toneladasDia);
            if (parrillaTotal != null) parrillaTotal.setDomingo(parrillaTotal.getDomingo().add(toneladasDia));
        }
    }

    private ParrillaSemanalTotalInfoDto iniciarParrillaSemanalTotales() {
        ParrillaSemanalTotalInfoDto parrillaTotal = new ParrillaSemanalTotalInfoDto();
        parrillaTotal.setLunes(new BigDecimal(0));
        parrillaTotal.setMartes(new BigDecimal(0));
        parrillaTotal.setMiercoles(new BigDecimal(0));
        parrillaTotal.setJueves(new BigDecimal(0));
        parrillaTotal.setViernes(new BigDecimal(0));
        parrillaTotal.setSabado(new BigDecimal(0));
        parrillaTotal.setDomingo(new BigDecimal(0));
        parrillaTotal.setTotal(new BigDecimal(0));
        return parrillaTotal;
    }

    private void actualizarTotalSemana(ParrillaSemanalTotalInfoDto parrillaTotal) {
        BigDecimal total = new BigDecimal(0);
        total = total.add(parrillaTotal.getLunes())
                .add(parrillaTotal.getMartes())
                .add(parrillaTotal.getMiercoles())
                .add(parrillaTotal.getJueves())
                .add(parrillaTotal.getViernes())
                .add(parrillaTotal.getSabado())
                .add(parrillaTotal.getDomingo());
        parrillaTotal.setTotal(total);
    }

    private void generarRegistrosHijos(List<ParrillaSemanalLineasInfoDto> lista, FiltrosParrillaSemanalFormDto filtros) {
        Boolean agrupadoArticulo = false;
        Boolean agrupadoCliente = false;
        Boolean agrupadoProveedor = false;
        Boolean agrupadoAlmacen = false;
        if (filtros.getAgrupado().equals("C") || filtros.getAgrupado().equals("P") || filtros.getAgrupado().equals("L")) {
            agrupadoArticulo = true;
        } else if (filtros.getAgrupado().equals("A") && (filtros.getTipoOperacion().equals("C") || filtros.getTipoOperacion().equals("R"))) {
            agrupadoProveedor = true;
        } else if (filtros.getAgrupado().equals("A") && filtros.getTipoOperacion().equals("T")) {
            agrupadoAlmacen = true;
        } else if (filtros.getAgrupado().equals("A") && (filtros.getTipoOperacion().equals("V") || filtros.getTipoOperacion().equals("S") || filtros.getTipoOperacion().equals("P"))) {
            agrupadoCliente = true;
        }
        List<ParrillaSemanalLineasInfoDto> listaHijos = new ArrayList<>();
        if (lista != null && lista.size() > 0) {
            for (ParrillaSemanalLineasInfoDto registroSemanal : lista) {
                String codigo = registroSemanal.getCodigo();
                if (filtros.getAgrupado().equals("C")) {
                    filtros.setCliente(codigo);
                } else if (filtros.getAgrupado().equals("A")) {
                    filtros.setArticulo(codigo);
                } else if (filtros.getAgrupado().equals("P")) {
                    filtros.setProveedor(codigo);
                } else if (filtros.getAgrupado().equals("L")) {
                    filtros.setAlmacen(codigo);
                }
                List<ParrillaSemanal> datos = daoManager.getParrillaSemanalDao().obtenerParrillaSemanal(filtros, em, agrupadoArticulo, agrupadoCliente, agrupadoProveedor, agrupadoAlmacen);
                listaHijos.addAll(generarLineasHijos(datos, codigo));
            }
            lista.addAll(listaHijos);
        }
    }

    private List<ParrillaSemanalLineasInfoDto> generarLineasHijos(List<ParrillaSemanal> datos, String codigoRelacion) {
        ParrillaSemanalLineasInfoDto registroSemanal = new ParrillaSemanalLineasInfoDto();
        List<ParrillaSemanalLineasInfoDto> lista = new ArrayList<>();
        String codigoRegistroAnterior = "";
        for (ParrillaSemanal registro : datos) {
            if (codigoRegistroAnterior.equals("")) {
                registroSemanal.setDescripcion(registro.getDescripcion());
                registroSemanal.setCodigo(registro.getCodigo() + " - " + codigoRelacion);
                registroSemanal.setRelacion(codigoRelacion);
                registroSemanal.setHijo(true);
                codigoRegistroAnterior = registro.getCodigo();
                anyadirValorEnDiaSemana(registro, registroSemanal, null);
            } else if (codigoRegistroAnterior.equals(registro.getCodigo())) {
                anyadirValorEnDiaSemana(registro, registroSemanal, null);
            } else if (!codigoRegistroAnterior.equals(registro.getCodigo())) {
                lista.add(registroSemanal);
                registroSemanal = new ParrillaSemanalLineasInfoDto();
                codigoRegistroAnterior = registro.getCodigo();
                registroSemanal.setDescripcion(registro.getDescripcion());
                registroSemanal.setCodigo(registro.getCodigo() + " - " + codigoRelacion);
                registroSemanal.setRelacion(codigoRelacion);
                registroSemanal.setHijo(true);
                anyadirValorEnDiaSemana(registro, registroSemanal, null);
            }
        }
        if (datos != null && datos.size() > 0) {
            lista.add(registroSemanal);
        }
        return lista;
    }
}
