package euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ParrillaSemanalTotalInfoDto {

    private BigDecimal total;
    private BigDecimal lunes;
    private BigDecimal martes;
    private BigDecimal miercoles;
    private BigDecimal jueves;
    private BigDecimal viernes;
    private BigDecimal sabado;
    private BigDecimal domingo;

}
