package euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ParrillaSemanalLineasInfoDto {

    private String codigo;
    private String descripcion;
    private String relacion;
    private  String planta;
    private Boolean hijo;
    private BigDecimal total;
    private BigDecimal lunes;
    private BigDecimal martes;
    private BigDecimal miercoles;
    private BigDecimal jueves;
    private BigDecimal viernes;
    private BigDecimal sabado;
    private BigDecimal domingo;

}
