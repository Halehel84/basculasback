package euroatomizado.idea.gestpes.ticket.parrillaSemanal.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.FiltrosParrillaSemanalFormDto;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.ParrillaSemanalInfoDto;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.managers.ParrillaSemanalManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/parrilla-semanal")
public class ParrillaSemanalController {

    @Autowired
    private ParrillaSemanalManager parrillaSemanalManager;

    @ResponseStatus(OK)
    @PostMapping(value = "parrilla")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public ParrillaSemanalInfoDto obtenerParrillaSemanal(@RequestBody @Valid FiltrosParrillaSemanalFormDto datos) {
        Boolean hayMasDeUnaPlanta = false;
        if (datos.getMasDeUnaPlanta()) {
            hayMasDeUnaPlanta = true;
            return parrillaSemanalManager.obtenerParrillaSemanal(datos, hayMasDeUnaPlanta);
        }else{
            return parrillaSemanalManager.obtenerParrillaSemanal(datos, hayMasDeUnaPlanta);
        }
    }

}