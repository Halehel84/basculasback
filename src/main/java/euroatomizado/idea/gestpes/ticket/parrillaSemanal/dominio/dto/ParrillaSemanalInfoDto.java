package euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto;

import lombok.Data;

import java.util.List;

@Data
public class ParrillaSemanalInfoDto {

    private List<ParrillaSemanalLineasInfoDto> lineas;
    private ParrillaSemanalTotalInfoDto totales;

}
