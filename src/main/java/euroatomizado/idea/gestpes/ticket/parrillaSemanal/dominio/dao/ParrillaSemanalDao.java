package euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dao;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.FiltrosParrillaSemanalFormDto;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto.ParrillaSemanal;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.QTicketEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntityPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public interface ParrillaSemanalDao extends CrudRepository<TicketEntity, TicketEntityPk> {

    default List<ParrillaSemanal> obtenerParrillaSemanal(FiltrosParrillaSemanalFormDto filtros, EntityManager em, Boolean agrupadoArticulo, Boolean agrupadoCliente, Boolean agrupadoProveedor, Boolean agrupadoAlmacen) {
        QTicketEntity ticketEntity = QTicketEntity.ticketEntity;
        /*FROM*/
        JPQLQuery queryPrincipal = new JPAQuery(em);
        queryPrincipal.from(ticketEntity);
        /*WHERE*/
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        String codigo;
        if (filtros.getSemana() != null) {
            booleanBuilder.and(ticketEntity.fecha.between(filtros.getPrimerDiaSemana(), filtros.getUltimoDiaSemana()));
        }
        if (filtros.getAlmacen() != null) {
            codigo = filtros.getAlmacen().substring(2);
            booleanBuilder.and(ticketEntity.almacenOrigen.eq(codigo));
        }
        if (filtros.getCliente() != null) {
            codigo = filtros.getCliente().substring(2);
            booleanBuilder.and(ticketEntity.cliente.clientePk.clienteId.eq(codigo));
        }
        if (filtros.getProveedor() != null) {
            codigo = filtros.getProveedor().substring(2);
            booleanBuilder.and(ticketEntity.proveedor.proveedorPk.proveedorId.eq(codigo));
        }
        if (filtros.getArticulo() != null) {
            codigo = filtros.getArticulo().substring(2);
            booleanBuilder.and(ticketEntity.articulo.articuloPk.articuloId.eq(codigo));
        }
        if (filtros.getEntidad() != null) {
            booleanBuilder.and(ticketEntity.entidad.eq(filtros.getEntidad()));
        }
        if (filtros.getPlanta() != null) {
            booleanBuilder.and(ticketEntity.seccion.seccionId.eq(filtros.getPlanta()));
        }
        if (filtros.getTipoOperacion() != null) {
            booleanBuilder.and(ticketEntity.tipoOperacion.eq(filtros.getTipoOperacion()));
        }
        if (filtros.getFamilia() != null) {
            booleanBuilder.and(ticketEntity.articulo.familiaId.eq(filtros.getFamilia()));
        }
        if(!filtros.getMostrarAlbaranesAnulados()){
            booleanBuilder.and(ticketEntity.albaranEstado.notIn("A","B").or(ticketEntity.albaranEstado.isNull()));
        }else if(filtros.getMostrarAlbaranesAnulados()){
            booleanBuilder.and(ticketEntity.albaranEstado.notIn("B").and(ticketEntity.albaranEstado.isNotNull()));
        }
        booleanBuilder.and(ticketEntity.numeroAlbaran.isNotNull());
        queryPrincipal.where(booleanBuilder);
        /* AGRUPADOS */
        if(agrupadoArticulo){
            /* SELECT AGRUPADO ARTICULO*/
            Expression select = Projections.constructor(ParrillaSemanal.class, ticketEntity.pesoNeto.sum(), ticketEntity.ticketPk.seccionId, ticketEntity.articulo.articuloPk.articuloId, ticketEntity.articulo.descripcion, ticketEntity.fecha);
            queryPrincipal.select(select);
            /*ORDENACION*/
            queryPrincipal.groupBy(ticketEntity.fecha, ticketEntity.ticketPk.seccionId, ticketEntity.articulo.articuloPk.articuloId, ticketEntity.articulo.descripcion);
            queryPrincipal.orderBy(ticketEntity.ticketPk.seccionId.asc() ,ticketEntity.articulo.articuloPk.articuloId.asc(), ticketEntity.fecha.asc());

        }else if(agrupadoCliente){
            /* SELECT AGRUPADO CLIENTE*/
            Expression select = Projections.constructor(ParrillaSemanal.class, ticketEntity.pesoNeto.sum(), ticketEntity.ticketPk.seccionId, ticketEntity.cliente.clientePk.clienteId, ticketEntity.cliente.nombre, ticketEntity.fecha);
            queryPrincipal.select(select);
            /*ORDENACION*/
            queryPrincipal.groupBy(ticketEntity.fecha, ticketEntity.ticketPk.seccionId, ticketEntity.cliente.clientePk.clienteId, ticketEntity.cliente.nombre);
            queryPrincipal.orderBy(ticketEntity.ticketPk.seccionId.asc(), ticketEntity.cliente.clientePk.clienteId.asc(), ticketEntity.fecha.asc());
        }else if(agrupadoProveedor){
            /* SELECT AGRUPADO PROVEEDOR*/
            Expression select = Projections.constructor(ParrillaSemanal.class, ticketEntity.pesoNeto.sum(), ticketEntity.ticketPk.seccionId, ticketEntity.proveedor.proveedorPk.proveedorId, ticketEntity.proveedor.nombre, ticketEntity.fecha);
            queryPrincipal.select(select);
            /*ORDENACION*/
            queryPrincipal.groupBy(ticketEntity.fecha, ticketEntity.ticketPk.seccionId, ticketEntity.proveedor.proveedorPk.proveedorId, ticketEntity.proveedor.nombre);
            queryPrincipal.orderBy(ticketEntity.ticketPk.seccionId.asc(), ticketEntity.proveedor.proveedorPk.proveedorId.asc(), ticketEntity.fecha.asc());
        }else if(agrupadoAlmacen){
            /* SELECT AGRUPADO ALMACEN*/
            Expression select = Projections.constructor(ParrillaSemanal.class, ticketEntity.pesoNeto.sum(), ticketEntity.ticketPk.seccionId, ticketEntity.almacenOrigen, ticketEntity.almacenOrigen, ticketEntity.fecha);
            queryPrincipal.select(select);
            /*ORDENACION*/
            queryPrincipal.groupBy(ticketEntity.fecha, ticketEntity.ticketPk.seccionId, ticketEntity.almacenOrigen, ticketEntity.almacenOrigen);
            queryPrincipal.orderBy(ticketEntity.ticketPk.seccionId.asc(), ticketEntity.almacenOrigen.asc(), ticketEntity.fecha.asc());
        }
        return queryPrincipal.fetch();
    }

}
