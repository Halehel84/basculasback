package euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ParrillaSemanal {
    private String codigo;
    private String descripcion;
    private String planta;
    private Long valor;
    private LocalDateTime fecha;

    public ParrillaSemanal(Long valor, String planta, String codigo, String descripcion, LocalDateTime fecha) {
        if (valor == null) {
            this.valor = 0l;
        } else {
            this.valor = valor;
        }
        this.codigo = codigo;
        this.planta = planta;
        this.descripcion = codigo + " - " + descripcion;
        this.fecha = fecha;
    }
}
