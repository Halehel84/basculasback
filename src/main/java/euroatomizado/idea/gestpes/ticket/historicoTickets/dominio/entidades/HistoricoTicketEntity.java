package euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.entidades;

import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto.HistoricoTicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "historico_tickets", schema = "dbo")
@Data
public class HistoricoTicketEntity {

    @EmbeddedId
    private HistoricoTicketEntityPk historicoTicketPk;
    @Column(name = "xentidad")
    private String entidad;
    @Column(name = "tic_tipo_op")
    private String ticTipoOp;
    @Column(name = "tic_numero")
    private String ticNumero;
    @Column(name = "tic_fecha")
    private LocalDateTime ticFecha;
    @Column(name = "xcliente_id")
    private String clienteId;
    @Column(name = "xlocal_id")
    private String localId;
    @Column(name = "xproveedor_id")
    private String proveedorId;
    @Column(name = "xarticulo_id")
    private String articuloId;
    @Column(name = "xtransportista_id")
    private String transportistaId;
    @Column(name = "xoptransportista_id")
    private String optransportistaId;
    @Column(name = "xmatricula")
    private String matricula;
    @Column(name = "xmatricula_rem")
    private String matriculaRem;
    @Column(name = "xconductor_id")
    private String conductorId;
    @Column(name = "tic_usu_id")
    private String ticUsuId;
    @Column(name = "tic_fecha_entrada")
    private LocalDateTime ticFechaEntrada;
    @Column(name = "tic_peso_entrada", columnDefinition = "real")
    private Float ticPesoEntrada;
    @Column(name = "tic_bascula_entrada")
    private String ticBasculaEntrada;
    @Column(name = "tic_observaciones")
    private String ticObservaciones;
    @Column(name = "tic_alb_entrada")
    private String ticAlbEntrada;
    @Column(name = "tic_peso_alb_entrada")
    private String ticPesoAlbEntrada;
    @Column(name = "tic_referencia")
    private String ticReferencia;
    @Column(name = "tic_fecha_salida")
    private LocalDateTime ticFechaSalida;
    @Column(name = "tic_peso_salida", columnDefinition = "real")
    private Float ticPesoSalida;
    @Column(name = "tic_bascula_salida")
    private String ticBasculaSalida;
    @Column(name = "tic_peso_neto", columnDefinition = "real")
    private Float ticPesoNeto;
    @Column(name = "tic_almacen_salida")
    private String ticAlmacenSalida;
    @Column(name = "tic_box_salida")
    private String ticBoxSalida;
    @Column(name = "tic_n_albaran")
    private String ticNAlbaran;
    @Column(name = "tic_alb_fecha_captura")
    private LocalDateTime ticAlbFechaCaptura;
    @Column(name = "tic_alb_estado")
    private String ticAlbEstado;
    @Column(name = "tic_n_bigbags_entrada")
    private String ticNBigbagsEntrada;
    @Column(name = "tic_a_bigbags_entrada")
    private String ticABigbagsEntrada;
    @Column(name = "tic_n_palets_entrada")
    private String ticNPaletsEntrada;
    @Column(name = "tic_a_palets_entrada")
    private String ticAPaletsEntrada;
    @Column(name = "tic_n_bigbags_salida")
    private String ticNBigbagsSalida;
    @Column(name = "tic_a_bigbags_salida")
    private String ticABigbagsSalida;
    @Column(name = "tic_n_palets_salida")
    private String ticNPaletsSalida;
    @Column(name = "tic_a_palets_salida")
    private String ticAPaletsSalida;
    @Column(name = "tic_carga_estimada", columnDefinition = "real")
    private Float ticCargaEstimada;
    @Column(name = "tic_tiempo_carga")
    private LocalDateTime ticTiempoCarga;
    @Column(name = "tic_tipo_pesaje")
    private String ticTipoPesaje;
    @Column(name = "xcliente_desc")
    private String clienteDesc;
    @Column(name = "xproveedor_desc")
    private String proveedorDesc;
    @Column(name = "xarticulo_desc")
    private String articuloDesc;
    @Column(name = "xcliente_direccion")
    private String clienteDireccion;
    @Column(name = "xcliente_poblacion")
    private String clientePoblacion;
    @Column(name = "xcliente_telefono")
    private String clienteTelefono;
    @Column(name = "xcliente_fax")
    private String clienteFax;
    @Column(name = "xproveedor_direccion")
    private String proveedorDireccion;
    @Column(name = "xproveedor_poblacion")
    private String proveedorPoblacion;
    @Column(name = "xproveedor_telefono")
    private String proveedorTelefono;
    @Column(name = "xproveedor_fax")
    private String proveedorFax;
    @Column(name = "xalmacen_origen")
    private String almacenOrigen;
    @Column(name = "xmarca_dev")
    private Integer marcaDev;
    @Column(name = "xbarco_id")
    private Integer barcoId;
    @Column(name = "gcon_codigo")
    private Integer gconCodigo;
    @Column(name = "tic_tipo_ticket")
    private String ticTipoTicket;
    @Column(name = "tic_observaciones_expedicion")
    private String ticObservacionesExpedicion;
    @Column(name = "sac_id")
    private String sacId;
    /*@Column(name = "ubi_codigo_origen")
    private Integer ubicacionCodigoOrigen;
    @Column(name = "ubi_codigo_destino")
    private Integer ubicacionCodigoDestino;*/
    @Column(name = "tic_ajuste", columnDefinition = "real")
    private Float ticAjuste;

    @Transient
    private HistoricoTicketFormDto historicoTicketFormDto;

}