package euroatomizado.idea.gestpes.ticket.historicoTickets.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto.HistoricoTicketFormDto;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto.HistoricoTicketInfoDto;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.entidades.HistoricoTicketEntity;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.entidades.HistoricoTicketEntityPk;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class HistoricoTicketManager implements PrimaryKeyStringManagerInterface<HistoricoTicketInfoDto, HistoricoTicketEntity,
        HistoricoTicketEntityPk, HistoricoTicketFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<HistoricoTicketInfoDto> obtenerHistoricoTickets() {
        return obtenerDatosSalida(daoManager.getHistoricoTicketDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public HistoricoTicketInfoDto guardar(HistoricoTicketFormDto datos) {
        HistoricoTicketEntity almacen = mapperManager.getMapeadorHistoricoTicket().toEntidad(datos);
        almacen.setHistoricoTicketFormDto(datos);
        try {
            if (!existe(almacen.getHistoricoTicketPk()))
                return obtenerDatosSalida(guardarNuevo(almacen));
            return obtenerDatosSalida(modificar(almacen));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public HistoricoTicketEntity guardarNuevo(HistoricoTicketEntity nuevo) {
        String id = nuevo.getHistoricoTicketFormDto().getHtiId();
        nuevo.setHistoricoTicketPk(obtenerPrimaryKey(id));
        daoManager.getHistoricoTicketDao().save(nuevo);
        return nuevo;
    }

    @Override
    public HistoricoTicketEntity modificar(HistoricoTicketEntity nuevo) {
        HistoricoTicketEntity almacen = obtener(nuevo);
        mapperManager.getMapeadorHistoricoTicket().actualizar(nuevo, almacen);
        daoManager.getHistoricoTicketDao().save(almacen);
        return almacen;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        HistoricoTicketEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getHistoricoTicketDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public HistoricoTicketInfoDto obtenerDatosSalida(HistoricoTicketEntity almacen) {
        return mapperManager.getMapeadorHistoricoTicket().toInfoDto(almacen);
    }

    private List<HistoricoTicketInfoDto> obtenerDatosSalida(List<HistoricoTicketEntity> almacenes) {
        return mapperManager.getMapeadorHistoricoTicket().toInfoDto(almacenes);
    }

    @Override
    public boolean existe(HistoricoTicketEntityPk pk) {
        return pk != null && daoManager.getHistoricoTicketDao().exists(pk);
    }

    @Override
    public HistoricoTicketEntity obtener(HistoricoTicketEntity datos) {
        return daoManager.getHistoricoTicketDao().findOne(datos.getHistoricoTicketPk());
    }

    @Override
    public HistoricoTicketEntityPk obtenerPrimaryKey(String id) {
        HistoricoTicketEntityPk pk = new HistoricoTicketEntityPk();
        pk.setHtiId(id);
        pk.setEmpresaId(EMPRESA);
        return pk;
    }

}
