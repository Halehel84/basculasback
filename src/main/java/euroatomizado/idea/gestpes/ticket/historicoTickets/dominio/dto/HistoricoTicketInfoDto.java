package euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class HistoricoTicketInfoDto {

    private String empresaId;
    private String ticId;
    private String seccionId;
    private String htiId;
    private String entidad;
    private String ticTipoOp;
    private String ticNumero;
    private LocalDateTime ticFecha;
    private String clienteId;
    private String localId;
    private String proveedorId;
    private String articuloId;
    private String transportistaId;
    private String optransportistaId;
    private String matricula;
    private String matriculaRem;
    private String conductorId;
    private String ticUsuId;
    private LocalDateTime ticFechaEntrada;
    private Float ticPesoEntrada;
    private String ticBasculaEntrada;
    private String ticObservaciones;
    private String ticAlbEntrada;
    private String ticPesoAlbEntrada;
    private String ticReferencia;
    private LocalDateTime ticFechaSalida;
    private Long ticPesoSalida;
    private String ticBasculaSalida;
    private Float ticPesoNeto;
    private String ticAlmacenSalida;
    private String ticBoxSalida;
    private String ticNAlbaran;
    private LocalDateTime ticAlbFechaCaptura;
    private String ticAlbEstado;
    private String ticNBigbagsEntrada;
    private String ticABigbagsEntrada;
    private String ticNPaletsEntrada;
    private String ticAPaletsEntrada;
    private String ticNBigbagsSalida;
    private String ticABigbagsSalida;
    private String ticNPaletsSalida;
    private String ticAPaletsSalida;
    private Float ticCargaEstimada;
    private LocalDateTime ticTiempoCarga;
    private String ticTipoPesaje;
    private String clienteDesc;
    private String proveedorDesc;
    private String articuloDesc;
    private String clienteDireccion;
    private String clientePoblacion;
    private String clienteTelefono;
    private String clienteFax;
    private String proveedorDireccion;
    private String proveedorPoblacion;
    private String proveedorTelefono;
    private String proveedorFax;
    private String almacenOrigen;
    private Integer marcaDev;
    private Integer barcoId;
    private Integer gconCodigo;
    private String ticTipoTicket;
    private String ticObservacionesExpedicion;
    private String sacId;
    //private Integer ubicacionCodigoOrigen;
    //private Integer ubicacionCodigoDestino;
    private Float ticAjuste;

}
