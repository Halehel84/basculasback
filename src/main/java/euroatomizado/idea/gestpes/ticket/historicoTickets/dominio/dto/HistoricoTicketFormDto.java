package euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class HistoricoTicketFormDto {

    @NotNull
    @Size(min = 1, max = 4)
    private String empresaId;
    @NotNull
    @Size(min = 1, max = 50)
    private String ticId;
    @NotNull
    @Size(min = 1, max = 2)
    private String seccionId;
    @NotNull
    @Size(min = 1, max = 50)
    private String htiId;
    @Size(min = 1, max = 2)
    private String entidad;
    @NotNull
    @Size(min = 1, max = 2)
    private String ticTipoOp;
    @Size(min = 1, max = 50)
    private String ticNumero;
    private LocalDateTime ticFecha;
    @Size(min = 1, max = 20)
    private String clienteId;
    @Size(min = 1, max = 4)
    private String localId;
    @Size(min = 1, max = 20)
    private String proveedorId;
    @Size(min = 1, max = 25)
    private String articuloId;
    @Size(min = 1, max = 5)
    private String transportistaId;
    @Size(min = 1, max = 5)
    private String optransportistaId;
    @Size(min = 1, max = 15)
    private String matricula;
    @Size(min = 1, max = 15)
    private String matriculaRem;
    @Size(min = 1, max = 5)
    private String conductorId;
    @Size(min = 1, max = 50)
    private String ticUsuId;
    private LocalDateTime ticFechaEntrada;
    private Float ticPesoEntrada;
    @Size(min = 1, max = 10)
    private String ticBasculaEntrada;
    @Size(min = 1, max = 250)
    private String ticObservaciones;
    @Size(min = 1, max = 250)
    private String ticAlbEntrada;
    @Size(min = 1, max = 250)
    private String ticPesoAlbEntrada;
    @Size(min = 1, max = 50)
    private String ticReferencia;
    private LocalDateTime ticFechaSalida;
    private Long ticPesoSalida;
    @Size(min = 1, max = 10)
    private String ticBasculaSalida;
    private Float ticPesoNeto;
    @Size(min = 1, max = 5)
    private String ticAlmacenSalida;
    @Size(min = 1, max = 50)
    private String ticBoxSalida;
    @Size(min = 1, max = 250)
    private String ticNAlbaran;
    private LocalDateTime ticAlbFechaCaptura;
    @Size(min = 1, max = 2)
    private String ticAlbEstado;
    @Size(min = 1, max = 50)
    private String ticNBigbagsEntrada;
    @Size(min = 1, max = 25)
    private String ticABigbagsEntrada;
    @Size(min = 1, max = 50)
    private String ticNPaletsEntrada;
    @Size(min = 1, max = 25)
    private String ticAPaletsEntrada;
    @Size(min = 1, max = 50)
    private String ticNBigbagsSalida;
    @Size(min = 1, max = 25)
    private String ticABigbagsSalida;
    @Size(min = 1, max = 50)
    private String ticNPaletsSalida;
    @Size(min = 1, max = 25)
    private String ticAPaletsSalida;
    private Float ticCargaEstimada;
    private LocalDateTime ticTiempoCarga;
    @Size(min = 1, max = 2)
    private String ticTipoPesaje;
    @Size(min = 1, max = 200)
    private String clienteDesc;
    @Size(min = 1, max = 200)
    private String proveedorDesc;
    @Size(min = 1, max = 200)
    private String articuloDesc;
    @Size(min = 1, max = 200)
    private String clienteDireccion;
    @Size(min = 1, max = 200)
    private String clientePoblacion;
    @Size(min = 1, max = 200)
    private String clienteTelefono;
    @Size(min = 1, max = 200)
    private String clienteFax;
    @Size(min = 1, max = 200)
    private String proveedorDireccion;
    @Size(min = 1, max = 200)
    private String proveedorPoblacion;
    @Size(min = 1, max = 200)
    private String proveedorTelefono;
    @Size(min = 1, max = 200)
    private String proveedorFax;
    @Size(min = 1, max = 5)
    private String almacenOrigen;
    private Integer marcaDev;
    private Integer barcoId;
    private Integer gconCodigo;
    @Size(min = 1, max = 1)
    private String ticTipoTicket;
    @Size(min = 1, max = 250)
    private String ticObservacionesExpedicion;
    @Size(min = 1, max = 50)
    private String sacId;
    @Size(min = 1, max = 50)
    //private Integer ubicacionCodigoOrigen;
    //private Integer ubicacionCodigoDestino;
    private Float ticAjuste;

}
