package euroatomizado.idea.gestpes.ticket.historicoTickets.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto.HistoricoTicketFormDto;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto.HistoricoTicketInfoDto;
import euroatomizado.idea.gestpes.ticket.historicoTickets.managers.HistoricoTicketManager;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.managers.TicketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/historico-tickets")
public class HistoricoTicketController {

    @Autowired
    private HistoricoTicketManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<HistoricoTicketInfoDto> obtenerHistoricoTickets() {
        return manager.obtenerHistoricoTickets();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public HistoricoTicketInfoDto crear(@RequestBody @Valid HistoricoTicketFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public HistoricoTicketInfoDto modificar(@RequestBody @Valid HistoricoTicketFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}