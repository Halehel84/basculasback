package euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.daos;

import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.entidades.HistoricoTicketEntity;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.entidades.HistoricoTicketEntityPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoricoTicketDao extends CrudRepository<HistoricoTicketEntity, HistoricoTicketEntityPk> {

    List<HistoricoTicketEntity> findAll();

}
