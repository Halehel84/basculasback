package euroatomizado.idea.gestpes.ticket.historicoTickets.mapeadores;

import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto.HistoricoTicketFormDto;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.dto.HistoricoTicketInfoDto;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.entidades.HistoricoTicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorHistoricoTicket {

    @Mapping(source = "historicoTicketPk.empresaId", target = "empresaId")
    @Mapping(source = "historicoTicketPk.ticId", target = "ticId")
    @Mapping(source = "historicoTicketPk.seccionId", target = "seccionId")
    @Mapping(source = "historicoTicketPk.htiId", target = "htiId")
    HistoricoTicketInfoDto toInfoDto(HistoricoTicketEntity datos);

    List<HistoricoTicketInfoDto> toInfoDto(List<HistoricoTicketEntity> datos);

    HistoricoTicketEntity toEntidad(HistoricoTicketFormDto dto);

    void actualizar(HistoricoTicketEntity datosNueva, @MappingTarget HistoricoTicketEntity datos);

}