package euroatomizado.idea.gestpes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EntityScan(basePackageClasses = {GestpesAplicationDev.class, Jsr310JpaConverters.class})
@SpringBootApplication(exclude = {GestpesAplication.class, DataSourceAutoConfiguration.class})
@EnableResourceServer
@EnableTransactionManagement(proxyTargetClass = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class GestpesAplicationDev {
    public static void main(String[] args) {
        SpringApplication.run(GestpesAplicationDev.class, args);
    }
}
