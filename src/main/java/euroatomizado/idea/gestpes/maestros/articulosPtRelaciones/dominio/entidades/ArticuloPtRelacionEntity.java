package euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.entidades;

import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "articulos_pt_relaciones", schema = "dbo")
@Data
public class ArticuloPtRelacionEntity {

    @Id
    @Column(name = "apr_codigo", columnDefinition = "int")
    private Integer aprCodigo;
    @Column(name = "apr_empresa", columnDefinition = "text")
    private String aprEmpresa;
    @Column(name = "apt_codigo", columnDefinition = "int")
    private Integer aptCodigo;
    @Column(name = "xarticulo_id", columnDefinition = "text")
    private String xarticuloId;
    @Column(name = "xarticulo_id_principal", columnDefinition = "text")
    private String xarticuloIdPrincipal;

}