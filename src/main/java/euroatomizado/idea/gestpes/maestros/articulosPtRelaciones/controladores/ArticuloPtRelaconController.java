package euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.dto.ArticuloPtRelacionInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.managers.ArticuloPtRelacionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/articulos-pt-relaciones")
public class ArticuloPtRelaconController {

    @Autowired
    private ArticuloPtRelacionManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ArticuloPtRelacionInfoDto> obtenerArticulos() {
        return manager.obtenerArticulos();
    }

}
