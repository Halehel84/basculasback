package euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.mapeadores;

import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.dto.ArticuloPtInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.entidades.ArticuloPtEntity;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.dto.ArticuloPtRelacionInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.entidades.ArticuloPtRelacionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorArticuloPtRelacion {

    ArticuloPtRelacionInfoDto toInfoDto(ArticuloPtRelacionEntity datos);

    List<ArticuloPtRelacionInfoDto> toInfoDto(List<ArticuloPtRelacionEntity> datos);

    void actualizar(ArticuloPtRelacionEntity datosNueva, @MappingTarget ArticuloPtRelacionEntity datos);

}