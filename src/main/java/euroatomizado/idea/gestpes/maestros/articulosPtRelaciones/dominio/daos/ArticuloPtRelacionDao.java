package euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.daos;

import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.entidades.ArticuloPtRelacionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticuloPtRelacionDao extends CrudRepository<ArticuloPtRelacionEntity, Integer> {
    List<ArticuloPtRelacionEntity> findAll();

    @Query(" SELECT art " +
            " FROM ArticuloPtRelacionEntity as art " +
            " WHERE art.aprCodigo = :codigo ")
    List<ArticuloPtRelacionEntity> obtenerArticuloPt(@Param("codigo") String codigo);

    @Query(" SELECT art.xarticuloId " +
            " FROM ArticuloPtRelacionEntity as art " +
            " WHERE art.aptCodigo in :articulosPt ")
    List<String> obtenerArticulosComposiciones(@Param("articulosPt") List<Integer> articulosPt);

}
