package euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.dto.ArticuloPtInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPt.managers.ArticuloPtManager;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.dto.ArticuloPtRelacionInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.entidades.ArticuloPtRelacionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ArticuloPtRelacionManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ArticuloPtRelacionInfoDto> obtenerArticulos() {
        List<ArticuloPtRelacionEntity> lista = daoManager.getArticuloPtRelacionDao().findAll();
        return mapperManager.getMapeadorArticuloPtRelacion().toInfoDto(lista);
    }

}
