package euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.dto;

import lombok.Data;

@Data
public class ArticuloPtRelacionInfoDto {

    private Integer aprCodigo;
    private String aprEmpresa;
    private Integer aptCodigo;
    private String xarticuloId;
    private String xarticuloIdPrincipal;

}
