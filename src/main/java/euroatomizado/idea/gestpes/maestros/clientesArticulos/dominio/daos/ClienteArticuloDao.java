package euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.daos;

import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntity;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntityPk;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades.ClienteArticuloEntity;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades.ClienteArticuloEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClienteArticuloDao extends CrudRepository<ClienteArticuloEntity, ClienteArticuloEntityPk> {

    List<ClienteArticuloEntity> findAll();

    @Query(" SELECT cli " +
            " FROM ClienteArticuloEntity as cli " +
            " WHERE cli.clienteArticuloPk.clienteId = :cliente " +
            " and cli.clienteArticuloPk.empresa = 'NPC' ")
    List<ClienteArticuloEntity> obtenerClienteArticuloPorCliente(@Param("cliente") String cliente);

}
