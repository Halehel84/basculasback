package euroatomizado.idea.gestpes.maestros.clientesArticulos.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.dto.ClienteArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades.ClienteArticuloEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteArticuloManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ClienteArticuloInfoDto> obtenerClientesArticulos() {
        List<ClienteArticuloEntity> lista = daoManager.getClienteArticuloDao().findAll();
        return mapperManager.getMapeadorClienteArticulo().toInfoDto(lista);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<ClienteArticuloInfoDto> obtenerClientesArticulosPorCliente(String id) {
        List<ClienteArticuloEntity> lista = daoManager.getClienteArticuloDao().obtenerClienteArticuloPorCliente(id);
        return mapperManager.getMapeadorClienteArticulo().toInfoDto(lista);
    }
}
