package euroatomizado.idea.gestpes.maestros.clientesArticulos.mapeadores;

import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.dto.ClienteArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades.ClienteArticuloEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorClienteArticulo {

    @Mapping(source = "clienteArticuloPk.clienteId", target = "clienteId")
    @Mapping(source = "clienteArticuloPk.articuloId", target = "articuloId")
    @Mapping(source = "articulo.articuloPk.articuloId", target = "id")
    @Mapping(source = "articulo.descripcion", target = "descripcion")
    ClienteArticuloInfoDto toInfoDto(ClienteArticuloEntity datos);

    List<ClienteArticuloInfoDto> toInfoDto(List<ClienteArticuloEntity> datos);

    void actualizar(ClienteArticuloEntity datosNueva, @MappingTarget ClienteArticuloEntity datos);

}