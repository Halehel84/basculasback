package euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ClienteArticuloEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xcliente_id")
    private String clienteId;
    @Column(name = "xarticulo_id")
    private String articuloId;

}
