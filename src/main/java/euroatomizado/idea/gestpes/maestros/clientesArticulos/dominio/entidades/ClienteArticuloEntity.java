package euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntity;
import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "clientes_articulos", schema = "dbo")
@Data
public class ClienteArticuloEntity {

    @EmbeddedId
    private ClienteArticuloEntityPk clienteArticuloPk;
    @Column(name = "xsituacion")
    private String situacion;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xarticulo_id", referencedColumnName = "xarticulo_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private ArticuloEntity articulo;
}
