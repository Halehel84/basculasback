package euroatomizado.idea.gestpes.maestros.clientesArticulos.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.dto.ClienteArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.managers.ClienteArticuloManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/clientes-articulos")
public class ClienteArticuloController {

    @Autowired
    private ClienteArticuloManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ClienteArticuloInfoDto> obtenerClientesArticulos() {
        return manager.obtenerClientesArticulos();
    }

    @ResponseStatus(OK)
    @GetMapping(value = "por-cliente")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ClienteArticuloInfoDto> obtenerClientesArticulosPorCliente(@PathParam("id") String id, @PathParam("empresa") String empresa) {
        return manager.obtenerClientesArticulosPorCliente(id);
    }

}
