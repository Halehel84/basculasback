package euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.dto;

import lombok.Data;

@Data
public class ClienteArticuloInfoDto {

    private String id;
    private String empresa;
    private String clienteId;
    private String articuloId;
    private String situacion;
    private String descripcion;
}
