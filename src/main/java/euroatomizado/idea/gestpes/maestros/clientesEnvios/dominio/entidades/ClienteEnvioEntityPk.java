package euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ClienteEnvioEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xcliente_id")
    private String clienteId;
    @Column(name = "xlocal_id")
    private String localId;

}
