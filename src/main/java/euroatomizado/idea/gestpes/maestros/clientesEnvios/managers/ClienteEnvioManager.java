package euroatomizado.idea.gestpes.maestros.clientesEnvios.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.dto.ClienteEnvioInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.entidades.ClienteEnvioEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteEnvioManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ClienteEnvioInfoDto> obtenerClientesEnvios() {
        List<ClienteEnvioEntity> lista = daoManager.getClienteEnvioDao().findAll();
        return mapperManager.getMapeadorClienteEnvio().toInfoDto(lista);
    }

}
