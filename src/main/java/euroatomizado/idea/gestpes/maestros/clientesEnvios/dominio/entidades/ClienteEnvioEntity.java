package euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntityPk;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity()
@Table(name = "clientes_envios", schema = "dbo")
@Data
public class ClienteEnvioEntity {

    @EmbeddedId
    private ClienteEnvioEntityPk clienteEnvioPk;
    @Column(name = "xnombre")
    private String nombre;
    @Column(name = "xhabitual")
    private Integer habitual;
    @Column(name = "xdomicilio")
    private String domicilio;
    @Column(name = "xpoblacion")
    private String poblacion;
    @Column(name = "xcod_postal")
    private String codigoPostal;
    @Column(name = "xprovincia_id")
    private String provinciaId;
    @Column(name = "xprovincia")
    private String provincia;
    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xpais")
    private String pais;
    @Column(name = "xtelefono")
    private String telefono;
    @Column(name = "xfax")
    private String fax;
    @Column(name = "xemail")
    private String email;

}
