package euroatomizado.idea.gestpes.maestros.clientesEnvios.controllers;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.dto.ClienteEnvioInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.managers.ClienteEnvioManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/clientes-envios")
public class ClienteEnvioController {

    @Autowired
    private ClienteEnvioManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ClienteEnvioInfoDto> obtenerClientesEnvios() {
        return manager.obtenerClientesEnvios();
    }

}
