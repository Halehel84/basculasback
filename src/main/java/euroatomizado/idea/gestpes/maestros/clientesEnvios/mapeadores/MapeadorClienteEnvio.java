package euroatomizado.idea.gestpes.maestros.clientesEnvios.mapeadores;

import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.dto.ClienteEnvioInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.entidades.ClienteEnvioEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorClienteEnvio {

    @Mapping(source = "clienteEnvioPk.clienteId", target = "clienteId")
    @Mapping(source = "clienteEnvioPk.localId", target = "localId")
    ClienteEnvioInfoDto toInfoDto(ClienteEnvioEntity datos);

    List<ClienteEnvioInfoDto> toInfoDto(List<ClienteEnvioEntity> datos);

    void actualizar(ClienteEnvioEntity datosNueva, @MappingTarget ClienteEnvioEntity datos);

}