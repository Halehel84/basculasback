package euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.dto;

import lombok.Data;

@Data
public class ClienteEnvioInfoDto {

    private String empresa;
    private String clienteId;
    private String localId;
    private String nombre;
    private Integer habitual;
    private String domicilio;
    private String poblacion;
    private String codigoPostal;
    private String provinciaId;
    private String provincia;
    private String paisId;
    private String pais;
    private String telefono;
    private String fax;
    private String email;

}
