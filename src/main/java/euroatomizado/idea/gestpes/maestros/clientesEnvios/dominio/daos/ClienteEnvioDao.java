package euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.daos;

import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.entidades.ClienteEnvioEntity;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.entidades.ClienteEnvioEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClienteEnvioDao extends CrudRepository<ClienteEnvioEntity, ClienteEnvioEntityPk> {

    List<ClienteEnvioEntity> findAll();

}
