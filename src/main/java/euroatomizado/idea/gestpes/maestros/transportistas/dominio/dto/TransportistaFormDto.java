package euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class TransportistaFormDto {

    @NotNull
    @Size(min = 1, max = 4)
    private String empresa;
    @NotNull
    @Size(min = 1, max = 5)
    private String transportistaId;
    @Size(max = 20)
    private String nombreAbrev;
    @Size(max = 50)
    private String nombre;
    @Size(max = 250)
    private String domicilio;
    @Size(max = 40)
    private String poblacion;
    @Size(max = 20)
    private String codigoPostal;
    @Size(max = 4)
    private String provinciaId;
    @Size(max = 250)
    private String provincia;
    @Size(max = 4)
    private String paisId;
    @Size(max = 250)
    private String pais;
    @Size(max = 40)
    private String telefono;
    @Size(max = 40)
    private String fax;
    @Size(max = 80)
    private String email;
    @Size(max = 2)
    private String situacion;
    @Size(max = 250)
    private String observaciones;
    @Size(max = 80)
    private String internet;
    private LocalDateTime fechaAlta;
    private LocalDateTime fechaBaja;
    @Size(max = 2)
    private String calificacionId;
    @Size(max = 20)
    private String cif;
    @Size(max = 10)
    private String nima;

}
