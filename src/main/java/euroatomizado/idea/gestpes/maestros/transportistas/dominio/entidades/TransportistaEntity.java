package euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto.TransportistaFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "transportistas", schema = "dbo")
@Data
public class TransportistaEntity {

    @EmbeddedId
    private TransportistaEntityPk transportistaPk;
    @Column(name = "xnomabrev")
    private String nombreAbrev;
    @Column(name = "xnombre")
    private String nombre;
    @Column(name = "xdomicilio")
    private String domicilio;
    @Column(name = "xpoblacion")
    private String poblacion;
    @Column(name = "xcod_postal")
    private String codigoPostal;
    @Column(name = "xprovincia_id")
    private String provinciaId;
    @Column(name = "xprovincia")
    private String provincia;
    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xpais")
    private String pais;
    @Column(name = "xtelefono")
    private String telefono;
    @Column(name = "xfax")
    private String fax;
    @Column(name = "xemail")
    private String email;
    @Column(name = "xsituacion")
    private String situacion;
    @Column(name = "xobservaciones")
    private String observaciones;
    @Column(name = "xinternet")
    private String internet;
    @Column(name = "xfecha_alta")
    private LocalDateTime fechaAlta;
    @Column(name = "xfecha_baja")
    private LocalDateTime fechaBaja;
    @Column(name = "xcalificacion_id")
    private String calificacionId;
    @Column(name = "xcif")
    private String cif;
    @Column(name = "xxnima")
    private String nima;

    @Transient
    private TransportistaFormDto transportistaFormDto;

}