package euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class TransportistaInfoDto {

    private String empresa;
    private String transportistaId;
    private String nombreAbrev;
    private String nombre;
    private String domicilio;
    private String poblacion;
    private String codigoPostal;
    private String provinciaId;
    private String provincia;
    private String paisId;
    private String pais;
    private String telefono;
    private String fax;
    private String email;
    private String situacion;
    private String observaciones;
    private String internet;
    private LocalDateTime fechaAlta;
    private LocalDateTime fechaBaja;
    private String calificacionId;
    private String cnae;
    private String nima;

}
