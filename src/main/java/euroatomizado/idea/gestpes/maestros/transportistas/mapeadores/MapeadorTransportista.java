package euroatomizado.idea.gestpes.maestros.transportistas.mapeadores;

import euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto.TransportistaFormDto;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto.TransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades.TransportistaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorTransportista {

    @Mapping(source = "transportistaPk.transportistaId", target = "transportistaId")
    @Mapping(source = "transportistaPk.empresa", target = "empresa")
    TransportistaInfoDto toInfoDto(TransportistaEntity datos);

    List<TransportistaInfoDto> toInfoDto(List<TransportistaEntity> datos);

    TransportistaEntity toEntidad(TransportistaFormDto dto);

    void actualizar(TransportistaEntity datosNueva, @MappingTarget TransportistaEntity datos);

}