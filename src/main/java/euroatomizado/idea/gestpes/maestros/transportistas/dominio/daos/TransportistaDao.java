package euroatomizado.idea.gestpes.maestros.transportistas.dominio.daos;

import euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades.ProveedorEntity;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades.TransportistaEntity;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades.TransportistaEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransportistaDao extends CrudRepository<TransportistaEntity, TransportistaEntityPk> {

    @Query(" SELECT tra " +
            " FROM TransportistaEntity as tra " +
            " WHERE tra.transportistaPk.empresa = 'NPC' ")
    List<TransportistaEntity> findAll();

    @Query(" SELECT pro " +
            " FROM TransportistaEntity as pro " +
            " WHERE pro.transportistaPk.transportistaId = :id ")
    TransportistaEntity obtenerTransportista(@Param("id") String id);
}
