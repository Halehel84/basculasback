package euroatomizado.idea.gestpes.maestros.transportistas.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto.TransportistaFormDto;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto.TransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades.TransportistaEntity;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades.TransportistaEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class TransportistaManager implements PrimaryKeyStringManagerInterface<TransportistaInfoDto,
        TransportistaEntity, TransportistaEntityPk, TransportistaFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<TransportistaInfoDto> obtenerTransportistas() {
        return obtenerDatosSalida(daoManager.getTransportistaDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TransportistaInfoDto guardar(TransportistaFormDto datos) {
        TransportistaEntity entity = mapperManager.getMapeadorTransportista().toEntidad(datos);
        entity.setTransportistaFormDto(datos);
        try {
            if (!existe(entity.getTransportistaPk()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public TransportistaEntity guardarNuevo(TransportistaEntity nuevo) {
        String id = nuevo.getTransportistaFormDto().getTransportistaId();
        nuevo.setTransportistaPk(obtenerPrimaryKey(id));
        daoManager.getTransportistaDao().save(nuevo);
        return nuevo;
    }

    @Override
    public TransportistaEntity modificar(TransportistaEntity nuevo) {
        TransportistaEntity entity = obtener(nuevo);
        mapperManager.getMapeadorTransportista().actualizar(nuevo, entity);
        daoManager.getTransportistaDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        TransportistaEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getTransportistaDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public TransportistaInfoDto obtenerDatosSalida(TransportistaEntity entity) {
        return mapperManager.getMapeadorTransportista().toInfoDto(entity);
    }

    private List<TransportistaInfoDto> obtenerDatosSalida(List<TransportistaEntity> entidades) {
        return mapperManager.getMapeadorTransportista().toInfoDto(entidades);
    }

    @Override
    public boolean existe(TransportistaEntityPk pk) {
        return pk != null && daoManager.getTransportistaDao().exists(pk);
    }

    @Override
    public TransportistaEntity obtener(TransportistaEntity datos) {
        return daoManager.getTransportistaDao().findOne(datos.getTransportistaPk());
    }

    @Override
    public TransportistaEntityPk obtenerPrimaryKey(String id) {
        TransportistaEntityPk pk = new TransportistaEntityPk();
        pk.setTransportistaId(id);
        pk.setEmpresa(EMPRESA);
        return pk;
    }

}
