package euroatomizado.idea.gestpes.maestros.transportistas.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto.TransportistaFormDto;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.dto.TransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistas.managers.TransportistaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/transportistas")
public class TransportistaController {

    @Autowired
    private TransportistaManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<TransportistaInfoDto> obtenerTransportistas() {
        return manager.obtenerTransportistas();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public TransportistaInfoDto crear(@RequestBody @Valid TransportistaFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public TransportistaInfoDto modificar(@RequestBody @Valid TransportistaFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
