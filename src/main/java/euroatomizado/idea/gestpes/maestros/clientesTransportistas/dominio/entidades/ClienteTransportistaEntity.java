package euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.transportistas.dominio.entidades.TransportistaEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "clientes_transportistas", schema = "dbo")
@Data
public class ClienteTransportistaEntity {

    @EmbeddedId
    private ClienteTransportistaEntityPk clienteTransportistaPk;
    @Column(name = "xhabitual")
    private Integer habitual;
    @Column(name = "xfecha_modif")
    private LocalDateTime fechaModif;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xtransportista_id", referencedColumnName = "xtransportista_id", insertable = false, updatable = false),
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private TransportistaEntity transportista;
}