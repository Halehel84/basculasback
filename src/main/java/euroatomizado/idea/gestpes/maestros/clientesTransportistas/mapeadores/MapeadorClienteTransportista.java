package euroatomizado.idea.gestpes.maestros.clientesTransportistas.mapeadores;

import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.dto.ClienteTransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.entidades.ClienteTransportistaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorClienteTransportista {

    @Mapping(source = "clienteTransportistaPk.empresa", target = "empresa")
    @Mapping(source = "clienteTransportistaPk.clienteId", target = "clienteId")
    @Mapping(source = "clienteTransportistaPk.transportistaId", target = "transportistaId")
    @Mapping(source = "transportista.transportistaPk.transportistaId", target = "id")
    @Mapping(source = "transportista.nombre", target = "nombre")
    ClienteTransportistaInfoDto toInfoDto(ClienteTransportistaEntity datos);

    List<ClienteTransportistaInfoDto> toInfoDto(List<ClienteTransportistaEntity> datos);

    void actualizar(ClienteTransportistaEntity datosNueva, @MappingTarget ClienteTransportistaEntity datos);

}