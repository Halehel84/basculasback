package euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.daos;

import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades.ClienteArticuloEntity;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.entidades.ClienteTransportistaEntity;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.entidades.ClienteTransportistaEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClienteTransportistaDao extends CrudRepository<ClienteTransportistaEntity, ClienteTransportistaEntityPk>

    {

    List<ClienteTransportistaEntity> findAll();

    @Query(" SELECT cli " +
            " FROM ClienteTransportistaEntity as cli " +
            " WHERE cli.clienteTransportistaPk.clienteId = :cliente " +
            "and cli.clienteTransportistaPk.empresa = 'NPC' ")
    List<ClienteTransportistaEntity> obtenerClienteTransportistaPorCliente(@Param("cliente") String cliente);
}