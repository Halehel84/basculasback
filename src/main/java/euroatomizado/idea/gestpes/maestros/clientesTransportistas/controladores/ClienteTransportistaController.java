package euroatomizado.idea.gestpes.maestros.clientesTransportistas.controladores;


import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.dto.ClienteArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.dto.ClienteTransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.managers.ClienteTransportistaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.PathParam;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/clientes-transportistas")
public class ClienteTransportistaController {

    @Autowired
    private ClienteTransportistaManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<ClienteTransportistaInfoDto> obtenerClientesTransportistas() {
        return manager.obtenerClientesTranportistas();
    }

    @ResponseStatus(OK)
    @GetMapping(value = "por-cliente")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<ClienteTransportistaInfoDto> obtenerClientesTranportistasPorCliente(@PathParam("id") String id, @PathParam("empresa") String empresa) {
        return manager.obtenerClientesTransportistasPorCliente(id);
    }
}
