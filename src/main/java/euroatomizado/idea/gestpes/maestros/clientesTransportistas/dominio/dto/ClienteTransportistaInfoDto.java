package euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ClienteTransportistaInfoDto {

    private String empresa;
    private String id;
    private String nombre;
    private String clienteId;
    private String transportistaId;
    private Integer habitual;
    private LocalDateTime fechaModif;

}
