package euroatomizado.idea.gestpes.maestros.clientesTransportistas.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.dto.ClienteArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.entidades.ClienteArticuloEntity;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.dto.ClienteTransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.entidades.ClienteTransportistaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteTransportistaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ClienteTransportistaInfoDto> obtenerClientesTranportistas() {
        List<ClienteTransportistaEntity> lista = daoManager.getClienteTransportistaDao().findAll();
        return mapperManager.getMapeadorClienteTransportista().toInfoDto(lista);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<ClienteTransportistaInfoDto> obtenerClientesTransportistasPorCliente(String id) {
        List<ClienteTransportistaEntity> lista = daoManager.getClienteTransportistaDao().obtenerClienteTransportistaPorCliente(id);
        return mapperManager.getMapeadorClienteTransportista().toInfoDto(lista);
    }

}
