package euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.almacenes.dominio.dto.AlmacenFormDto;
import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "almacenes", schema = "dbo")
@Data
public class AlmacenEntity {
    @EmbeddedId
    private AlmacenEntityPk almacenesPk;
    @Column(name = "xdescripcion")
    private String descripcion;
    @Column(name = "xdomicilio")
    private String domicilio;
    @Column(name = "xpoblacion")
    private String poblacion;
    @Column(name = "xcod_postal")
    private String codigoPostal;
    @Column(name = "xpronvincia_id")
    private String pronvinciaId;
    @Column(name = "xpronvincia")
    private String pronvincia;
    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xpais")
    private String pais;
    @Column(name = "xtelefono")
    private String telefono;
    @Column(name = "xfax")
    private String fax;
    @Column(name = "xemail")
    private String email;
    @Column(name = "xsituacion")
    private String situacion;
    @Column(name = "xobservaciones")
    private String observaciones;
    @Column(name = "xpesajes")
    private Short pesajes;
    @Column(name = "alm_por_defecto")
    private String almacenPorDefecto;
    @Column(name = "xproveedor_id")
    private String proveedorId;
    @Column(name = "xproveedor_id2")
    private String proveedorId2;

    @Transient
    private AlmacenFormDto almacenFormDto;
}