package euroatomizado.idea.gestpes.maestros.almacenes.dominio.dto;

import lombok.Data;

@Data
public class AlmacenInfoDto {
    private String empresa;
    private String almacenId;
    private String seccion;
    private String descripcion;
    private String domicilio;
    private String poblacion;
    private String codigoPostal;
    private String pronvinciaId;
    private String pronvincia;
    private String paisId;
    private String pais;
    private String telefono;
    private String fax;
    private String email;
    private String situacion;
    private String observaciones;
    private Short pesajes;
    private String almacenPorDefecto;
    private String proveedorId;
    private String proveedorId2;

}
