package euroatomizado.idea.gestpes.maestros.almacenes.dominio.daos;

import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntity;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlmacenesDao extends CrudRepository<AlmacenEntity, AlmacenEntityPk> {

    @Query(" SELECT alm " +
            " FROM AlmacenEntity as alm " +
            " WHERE alm.almacenesPk.empresa = 'NPC' ")
    List<AlmacenEntity> findAll();

    @Query(" SELECT alm " +
            " FROM AlmacenEntity as alm " +
            " WHERE alm.almacenesPk.almacenId = :almacen" +
            " and alm.almacenesPk.empresa = 'NPC' ")
    AlmacenEntity obtenerAlmacen(@Param("almacen") String almacen);

}
