package euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class AlmacenEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xalmacen_id")
    private String almacenId;
    @Column(name = "xseccion_id")
    private String seccion;

}
