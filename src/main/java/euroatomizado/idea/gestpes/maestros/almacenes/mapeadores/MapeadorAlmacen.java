package euroatomizado.idea.gestpes.maestros.almacenes.mapeadores;

import euroatomizado.idea.gestpes.maestros.almacenes.dominio.dto.AlmacenFormDto;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.dto.AlmacenInfoDto;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorAlmacen {

    @Mapping(source = "almacenesPk.almacenId", target = "almacenId")
    @Mapping(source = "almacenesPk.seccion", target = "seccion")
    AlmacenInfoDto toInfoDto(AlmacenEntity datos);

    List<AlmacenInfoDto> toInfoDto(List<AlmacenEntity> datos);

    AlmacenEntity toEntidad(AlmacenFormDto dto);

    void actualizar(AlmacenEntity datosNueva, @MappingTarget AlmacenEntity datos);

}