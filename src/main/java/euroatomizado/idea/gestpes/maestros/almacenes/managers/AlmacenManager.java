package euroatomizado.idea.gestpes.maestros.almacenes.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.dto.AlmacenInfoDto;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AlmacenManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<AlmacenInfoDto> obtenerAlmacenes() {
        List<AlmacenEntity> lista = daoManager.getAlmacenesDao().findAll();
        return mapperManager.getMapeadorAlmacenes().toInfoDto(lista);
    }

}
