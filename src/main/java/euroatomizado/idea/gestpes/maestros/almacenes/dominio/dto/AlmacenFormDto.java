package euroatomizado.idea.gestpes.maestros.almacenes.dominio.dto;

import lombok.Data;

import javax.validation.constraints.Size;

@Data

public class AlmacenFormDto {
    @Size(min = 1, max = 4)
    private String empresa;
    @Size(min = 1, max = 5)
    private String almacenId;
    @Size(min = 1, max = 2)
    private String seccion;
    @Size(min = 1, max = 40)
    private String descripcion;
    @Size(min = 1, max = 250)
    private String domicilio;
    @Size(min = 1, max = 40)
    private String poblacion;
    @Size(min = 1, max = 20)
    private String codigoPostal;
    @Size(min = 1, max = 4)
    private String pronvinciaId;
    @Size(min = 1, max = 250)
    private String pronvincia;
    @Size(min = 1, max = 4)
    private String paisId;
    @Size(min = 1, max = 250)
    private String pais;
    @Size(min = 1, max = 40)
    private String telefono;
    @Size(min = 1, max = 40)
    private String fax;
    @Size(min = 1, max = 80)
    private String email;
    @Size(min = 1, max = 2)
    private String situacion;
    @Size(min = 1, max = 250)
    private String observaciones;
    private Short pesajes;
    @Size(min = 1, max = 1)
    private String almacenPorDefecto;
    @Size(min = 1, max = 50)
    private String proveedorId;
    private String proveedorId2;
}
