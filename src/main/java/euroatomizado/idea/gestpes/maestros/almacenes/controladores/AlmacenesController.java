package euroatomizado.idea.gestpes.maestros.almacenes.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.dto.AlmacenInfoDto;
import euroatomizado.idea.gestpes.maestros.almacenes.managers.AlmacenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/almacenes")
public class AlmacenesController {

    @Autowired
    private AlmacenManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<AlmacenInfoDto> obtenerAlmacenes() {
        return manager.obtenerAlmacenes();
    }

}
