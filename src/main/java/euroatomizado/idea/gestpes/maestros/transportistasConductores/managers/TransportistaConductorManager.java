package euroatomizado.idea.gestpes.maestros.transportistasConductores.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto.TransportistaConductorFormDto;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto.TransportistaConductorInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.entidades.TransportistaConductorEntity;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.entidades.TransportistaConductorEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class TransportistaConductorManager implements PrimaryKeyStringManagerInterface<TransportistaConductorInfoDto,
        TransportistaConductorEntity, TransportistaConductorEntityPk, TransportistaConductorFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<TransportistaConductorInfoDto> obtenerTransportistas() {
        return obtenerDatosSalida(daoManager.getTransportistaConductorDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TransportistaConductorInfoDto guardar(TransportistaConductorFormDto datos) {
        TransportistaConductorEntity entity = mapperManager.getMapeadorTransportistaConductor().toEntidad(datos);
        entity.setTransportistaConductorFormDto(datos);
        try {
            if (!existe(entity.getTransportistaConductorPk()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public TransportistaConductorEntity guardarNuevo(TransportistaConductorEntity nuevo) {
        String id = nuevo.getTransportistaConductorFormDto().getTransportistaId();
        nuevo.setTransportistaConductorPk(obtenerPrimaryKey(id));
        daoManager.getTransportistaConductorDao().save(nuevo);
        return nuevo;
    }

    @Override
    public TransportistaConductorEntity modificar(TransportistaConductorEntity nuevo) {
        TransportistaConductorEntity entity = obtener(nuevo);
        mapperManager.getMapeadorTransportistaConductor().actualizar(nuevo, entity);
        daoManager.getTransportistaConductorDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        TransportistaConductorEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getTransportistaConductorDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public TransportistaConductorInfoDto obtenerDatosSalida(TransportistaConductorEntity entity) {
        return mapperManager.getMapeadorTransportistaConductor().toInfoDto(entity);
    }

    private List<TransportistaConductorInfoDto> obtenerDatosSalida(List<TransportistaConductorEntity> entidades) {
        return mapperManager.getMapeadorTransportistaConductor().toInfoDto(entidades);
    }

    @Override
    public boolean existe(TransportistaConductorEntityPk pk) {
        return pk != null && daoManager.getTransportistaConductorDao().exists(pk);
    }

    @Override
    public TransportistaConductorEntity obtener(TransportistaConductorEntity datos) {
        return daoManager.getTransportistaConductorDao().findOne(datos.getTransportistaConductorPk());
    }

    @Override
    public TransportistaConductorEntityPk obtenerPrimaryKey(String id) {
        TransportistaConductorEntityPk pk = new TransportistaConductorEntityPk();
        pk.setTransportistaId(id);
        pk.setEmpresa(EMPRESA);
        return pk;
    }

}

