package euroatomizado.idea.gestpes.maestros.transportistasConductores.mapeadores;

import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto.TransportistaConductorFormDto;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto.TransportistaConductorInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.entidades.TransportistaConductorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorTransportistaConductor {

    @Mapping(source = "transportistaConductorPk.transportistaId", target = "transportistaId")
    @Mapping(source = "transportistaConductorPk.conductorId", target = "conductorId")
    @Mapping(source = "transportistaConductorPk.empresa", target = "empresa")
    TransportistaConductorInfoDto toInfoDto(TransportistaConductorEntity datos);

    List<TransportistaConductorInfoDto> toInfoDto(List<TransportistaConductorEntity> datos);

    TransportistaConductorEntity toEntidad(TransportistaConductorFormDto dto);

    void actualizar(TransportistaConductorEntity datosNueva, @MappingTarget TransportistaConductorEntity datos);

}