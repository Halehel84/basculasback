package euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TransportistaConductorInfoDto {

    private String empresa;
    private String transportistaId;
    private String conductorId;
    private LocalDateTime fechaModif;

}
