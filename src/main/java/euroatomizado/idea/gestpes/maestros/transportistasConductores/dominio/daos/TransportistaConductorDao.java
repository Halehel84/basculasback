package euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.daos;

import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.entidades.TransportistaConductorEntity;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.entidades.TransportistaConductorEntityPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransportistaConductorDao extends
        CrudRepository<TransportistaConductorEntity, TransportistaConductorEntityPk> {

    List<TransportistaConductorEntity> findAll();

}
