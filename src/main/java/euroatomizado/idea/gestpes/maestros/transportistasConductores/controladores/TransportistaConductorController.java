package euroatomizado.idea.gestpes.maestros.transportistasConductores.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto.TransportistaConductorFormDto;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto.TransportistaConductorInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.managers.TransportistaConductorManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/transportistas-conductores")
public class TransportistaConductorController {

    @Autowired
    private TransportistaConductorManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<TransportistaConductorInfoDto> obtenerTransportistas() {
        return manager.obtenerTransportistas();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public TransportistaConductorInfoDto crear(@RequestBody @Valid TransportistaConductorFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public TransportistaConductorInfoDto modificar(@RequestBody @Valid TransportistaConductorFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
