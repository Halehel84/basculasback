package euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.dto.TransportistaConductorFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "transportistas_conductores", schema = "dbo")
@Data
public class TransportistaConductorEntity {

    @EmbeddedId
    private TransportistaConductorEntityPk transportistaConductorPk;
    @Column(name = "xfecha_modif")
    private LocalDateTime fechaModif;

    @Transient
    private TransportistaConductorFormDto transportistaConductorFormDto;

}
