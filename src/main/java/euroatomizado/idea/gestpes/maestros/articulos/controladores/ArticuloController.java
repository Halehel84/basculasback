package euroatomizado.idea.gestpes.maestros.articulos.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.dto.ArticuloFormDto;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.dto.ArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.articulos.managers.ArticuloManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/articulos")
public class ArticuloController {

    @Autowired
    private ArticuloManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ArticuloInfoDto> obtenerArticulos() {
        return manager.obtenerArticulos();
    }

    @ResponseStatus(OK)
    @GetMapping(value = "cantidad")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public long cantidadArticulos() {
        return manager.contarArticulos();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public ArticuloInfoDto crear(@RequestBody @Valid ArticuloFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public ArticuloInfoDto modificar(@RequestBody @Valid ArticuloFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
