package euroatomizado.idea.gestpes.maestros.articulos.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.dto.ArticuloFormDto;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.dto.ArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntity;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class ArticuloManager implements PrimaryKeyStringManagerInterface<ArticuloInfoDto, ArticuloEntity, ArticuloEntityPk, ArticuloFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ArticuloInfoDto> obtenerArticulos() {
        return obtenerDatosSalida(daoManager.getArticuloDao().findAll());
    }

    public long contarArticulos(){
        return this.daoManager.getArticuloDao().count();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ArticuloInfoDto guardar(ArticuloFormDto datos) {
        ArticuloEntity entity = mapperManager.getMapeadorArticulo().toEntidad(datos);
        entity.setArticuloFormDto(datos);
        try {
            if (!existe(entity.getArticuloPk()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public ArticuloEntity guardarNuevo(ArticuloEntity nuevo) {
        String id = nuevo.getArticuloFormDto().getArticuloId();
        nuevo.setArticuloPk(obtenerPrimaryKey(id));
        daoManager.getArticuloDao().save(nuevo);
        return nuevo;
    }

    @Override
    public ArticuloEntity modificar(ArticuloEntity nuevo) {
        ArticuloEntity entity = obtener(nuevo);
        mapperManager.getMapeadorArticulo().actualizar(nuevo, entity);
        daoManager.getArticuloDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        ArticuloEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getArticuloDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public ArticuloInfoDto obtenerDatosSalida(ArticuloEntity entity) {
        return mapperManager.getMapeadorArticulo().toInfoDto(entity);
    }

    private List<ArticuloInfoDto> obtenerDatosSalida(List<ArticuloEntity> entidades) {
        return mapperManager.getMapeadorArticulo().toInfoDto(entidades);
    }

    @Override
    public boolean existe(ArticuloEntityPk pk) {
        return pk != null && daoManager.getArticuloDao().exists(pk);
    }

    @Override
    public ArticuloEntity obtener(ArticuloEntity datos) {
        return daoManager.getArticuloDao().findOne(datos.getArticuloPk());
    }

    @Override
    public ArticuloEntityPk obtenerPrimaryKey(String id) {
        ArticuloEntityPk pk = new ArticuloEntityPk();
        pk.setArticuloId(id);
        pk.setEmpresa(EMPRESA);
        return pk;
    }

}

