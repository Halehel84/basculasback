package euroatomizado.idea.gestpes.maestros.articulos.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data

public class ArticuloFormDto {
    @Size(min = 1, max = 4)
    private String empresa;
    @NotNull
    @Size(min = 1, max = 25)
    private String articuloId;
    @Size(min = 1, max = 40)
    private String descripcion;
    @Size(min = 1, max = 20)
    private String nombreAbreviado;
    @Size(min = 1, max = 5)
    private String familiaId;
    @Size(min = 1, max = 5)
    private String xssfamiliaId;

    @Size(min = 1, max = 50)
    private String tipoArticulo;
    @Size(min = 1, max = 2)
    private String seccionId;
    @Size(min = 1, max = 2)
    private String almPrincComp;
    private LocalDateTime fechaAlta;
    private LocalDateTime fechaBajaComp;
    private LocalDateTime fechaBajaVtas;
    @Size(min = 1, max = 2)
    private String situacion;
    @Size(min = 1, max = 250)
    private String observaciones;
    private Short gestLotes;
    @Size(min = 1, max = 1)
    private String generico;
    private Integer artCodigo;
    @Size(min = 1, max = 25)
    private String articuloIdRel;
    @Size(min = 1, max = 25)
    private String articuloIdBase;
}
