package euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.articulos.dominio.dto.ArticuloFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "articulos", schema = "dbo")
@Data
public class ArticuloEntity {
    @EmbeddedId
    private ArticuloEntityPk articuloPk;
    @Column(name = "xdescripcion")
    private String descripcion;
    @Column(name = "xnom_abreviado")
    private String nombreAbreviado;
    @Column(name = "xfamilia_id")
    private String familiaId;
    @Column(name = "xssfamilia_id")
    private String xssfamiliaId;
    @Column(name = "xtipo_articulo")
    private String tipoArticulo;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "xalm_princ_comp")
    private String almPrincComp;
    @Column(name = "xfecha_alta")
    private LocalDateTime fechaAlta;
    @Column(name = "xfecha_baja_comp")
    private LocalDateTime fechaBajaComp;
    @Column(name = "xfecha_baja_vtas")
    private LocalDateTime fechaBajaVtas;
    @Column(name = "xsituacion")
    private String situacion;
    @Column(name = "xobservaciones")
    private String observaciones;
    @Column(name = "xgest_lotes")
    private Short  gestLotes;
    @Column(name = "xgenerico")
    private String generico;
    @Column(name = "art_codigo")
    private Integer artCodigo;
    @Column(name = "xarticulo_id_rel")
    private String articuloIdRel;
    @Column(name = "xarticulo_id_base")
    private String articuloIdBase;

    @Transient
    private ArticuloFormDto articuloFormDto;

}