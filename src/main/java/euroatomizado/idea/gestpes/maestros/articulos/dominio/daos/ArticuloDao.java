package euroatomizado.idea.gestpes.maestros.articulos.dominio.daos;

import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntity;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntityPk;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional
public interface ArticuloDao extends CrudRepository<ArticuloEntity, ArticuloEntityPk> {

    @Query(" SELECT art " +
            " FROM ArticuloEntity as art " +
            " WHERE art.articuloPk.empresa = 'NPC' ")
    List<ArticuloEntity> findAll();

    @Query(" SELECT art " +
            " FROM ArticuloEntity as art " +
            " WHERE art.articuloPk.articuloId = :articulo " +
            " and art.articuloPk.empresa = :empresa ")
    ArticuloEntity obtenerArticulo(@Param("articulo") String articulo,
                                   @Param("empresa") String empresa);
}
