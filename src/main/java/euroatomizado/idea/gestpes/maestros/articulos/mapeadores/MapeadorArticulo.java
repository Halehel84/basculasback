package euroatomizado.idea.gestpes.maestros.articulos.mapeadores;

import euroatomizado.idea.gestpes.maestros.articulos.dominio.dto.ArticuloFormDto;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.dto.ArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntity;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoFormDto;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoInfoDto;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades.BarcoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorArticulo {

    @Mapping(source = "articuloPk.articuloId", target = "articuloId")
    ArticuloInfoDto toInfoDto(ArticuloEntity datos);

    List<ArticuloInfoDto> toInfoDto(List<ArticuloEntity> datos);

    ArticuloEntity toEntidad(ArticuloFormDto dto);

    void actualizar(ArticuloEntity datosNueva, @MappingTarget ArticuloEntity datos);
}
