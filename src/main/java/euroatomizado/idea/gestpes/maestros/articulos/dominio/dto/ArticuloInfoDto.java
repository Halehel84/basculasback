package euroatomizado.idea.gestpes.maestros.articulos.dominio.dto;

import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntityPk;
import lombok.Data;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Data

public class ArticuloInfoDto {

    private String empresa;
    private String articuloId;
    private String descripcion;
    private String nombreAbreviado;
    private String familiaId;
    private String xssfamiliaId;
    private String tipoArticulo;
    private String seccionId;
    private String almPrincComp;
    private LocalDateTime fechaAlta;
    private LocalDateTime fechaBajaComp;
    private LocalDateTime fechaBajaVtas;
    private String situacion;
    private String observaciones;
    private Short gestLotes;
    private String generico;
    private Integer artCodigo;
    private String articuloIdRel;
    private String articuloIdBase;

}
