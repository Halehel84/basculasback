package euroatomizado.idea.gestpes.maestros.conductores.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.conductores.dominio.dto.ConductorInfoDto;
import euroatomizado.idea.gestpes.maestros.conductores.dominio.entidades.ConductorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ConductorManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ConductorInfoDto> obtenerConductores() {
        List<ConductorEntity> lista = daoManager.getConductorDao().findAll();
        return mapperManager.getMapeadorConductor().toInfoDto(lista);
    }

}
