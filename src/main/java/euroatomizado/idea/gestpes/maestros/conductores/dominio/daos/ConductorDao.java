package euroatomizado.idea.gestpes.maestros.conductores.dominio.daos;

import euroatomizado.idea.gestpes.maestros.conductores.dominio.entidades.ConductorEntity;
import euroatomizado.idea.gestpes.maestros.conductores.dominio.entidades.ConductorEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConductorDao extends CrudRepository<ConductorEntity, ConductorEntityPk> {

    List<ConductorEntity> findAll();

    @Query(" SELECT con " +
            " FROM ConductorEntity as con " +
            " WHERE con.conductorPk.conductorId = :conductor ")
    List<ConductorEntity> obtenerConductor(@Param("conductor") String conductor);

}
