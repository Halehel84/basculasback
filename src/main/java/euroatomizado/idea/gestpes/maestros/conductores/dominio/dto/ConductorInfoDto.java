package euroatomizado.idea.gestpes.maestros.conductores.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ConductorInfoDto {

    private String empresa;
    private String conductorId;
    private String nombre;
    private String nif;
    private String domicilio;
    private String poblacion;
    private String codigoPostal;
    private String provinciaId;
    private String provincia;
    private String paisId;
    private String pais;
    private String telefono;
    private String fax;
    private String email;
    private String situacion;
    private String calificacionId;
    private String observaciones;
    private LocalDateTime fechaModif;

}
