package euroatomizado.idea.gestpes.maestros.conductores.mapeadores;

import euroatomizado.idea.gestpes.maestros.conductores.dominio.dto.ConductorInfoDto;
import euroatomizado.idea.gestpes.maestros.conductores.dominio.entidades.ConductorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorConductor {

    @Mapping(source = "conductorPk.empresa", target = "empresa")
    @Mapping(source = "conductorPk.conductorId", target = "conductorId")
    ConductorInfoDto toInfoDto(ConductorEntity datos);

    List<ConductorInfoDto> toInfoDto(List<ConductorEntity> datos);

    void actualizar(ConductorEntity datosNueva, @MappingTarget ConductorEntity datos);

}