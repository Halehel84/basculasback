package euroatomizado.idea.gestpes.maestros.personal.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.personal.dominio.dto.PersonalInfoDto;
import euroatomizado.idea.gestpes.maestros.personal.dominio.entidades.PersonalEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonalManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<PersonalInfoDto> obtenerPersonal() {
        List<PersonalEntity> lista = daoManager.getPersonalDao().findAll();
        return mapperManager.getMapeadorPersonal().toInfoDto(lista);
    }

}