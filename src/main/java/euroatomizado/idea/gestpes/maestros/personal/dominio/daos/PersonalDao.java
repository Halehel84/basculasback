package euroatomizado.idea.gestpes.maestros.personal.dominio.daos;

import euroatomizado.idea.gestpes.maestros.personal.dominio.entidades.PersonalEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonalDao extends CrudRepository<PersonalEntity, Integer> {

    List<PersonalEntity> findAll();

    @Query(" SELECT per " +
            " FROM PersonalEntity as per " +
            " WHERE per.codigo = :codigo ")
    PersonalEntity obtenerPersonal(@Param("codigo") String codigo);

}