package euroatomizado.idea.gestpes.maestros.personal.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name = "personal", schema = "dbo")
@Data
public class PersonalEntity {

    @Id
    @Column(name = "per_codigo")
    private Integer codigo;
    @Column(name = "per_empresa")
    private String empresa;
    @Column(name = "per_nombre")
    private String nombre;
    @Column(name = "cat_codigo")
    private Integer categoriaCodigo;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "per_baja")
    private String baja;
    @Column(name = "per_operario_expedicion")
    private String operarioExpedicion;

}
