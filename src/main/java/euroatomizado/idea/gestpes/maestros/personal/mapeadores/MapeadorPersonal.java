package euroatomizado.idea.gestpes.maestros.personal.mapeadores;

import euroatomizado.idea.gestpes.maestros.personal.dominio.dto.PersonalInfoDto;
import euroatomizado.idea.gestpes.maestros.personal.dominio.entidades.PersonalEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorPersonal {

    PersonalInfoDto toInfoDto(PersonalEntity datos);

    List<PersonalInfoDto> toInfoDto(List<PersonalEntity> datos);

    void actualizar(PersonalEntity datosNueva, @MappingTarget PersonalEntity datos);

}