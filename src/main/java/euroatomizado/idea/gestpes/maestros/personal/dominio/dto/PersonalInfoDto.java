package euroatomizado.idea.gestpes.maestros.personal.dominio.dto;

import lombok.Data;

@Data
public class PersonalInfoDto {

    private Integer codigo;
    private String empresa;
    private String nombre;
    private Integer categoriaCodigo;
    private String seccionId;
    private String baja;
    private String operarioExpedicion;

}
