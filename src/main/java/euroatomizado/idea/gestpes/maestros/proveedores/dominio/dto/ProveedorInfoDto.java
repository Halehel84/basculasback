package euroatomizado.idea.gestpes.maestros.proveedores.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ProveedorInfoDto {

    private String empresa;
    private String proveedorId;
    private String nombre;
    private String nombreAbrev;
    private String nif;
    private String domicilio;
    private String poblacion;
    private String codigoPostal;
    private String provinciaId;
    private String provincia;
    private String paisId;
    private String pais;
    private String telefono;
    private String fax;
    private String email;
    private String situacion;
    private String observaciones;
    private String internet;
    private LocalDateTime fechaAlta;
    private LocalDateTime fechaBaja;
    private String seccionId;
    private String calificacionId;
    private String cnae;
    private String nima;

}
