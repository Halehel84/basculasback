package euroatomizado.idea.gestpes.maestros.proveedores.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.proveedores.dominio.dto.ProveedorInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedores.managers.ProveedorManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/proveedores")
public class ProveedorController {

    @Autowired
    private ProveedorManager manager;

    @ResponseStatus(OK)
    @GetMapping
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ProveedorInfoDto> obtenerProveedores() {
        return manager.obtenerProveedores();
    }

}
