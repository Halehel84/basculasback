package euroatomizado.idea.gestpes.maestros.proveedores.dominio.daos;

import euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades.ProveedorEntity;
import euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades.ProveedorEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProveedorDao extends CrudRepository<ProveedorEntity, ProveedorEntityPk> {

    List<ProveedorEntity> findAll();

    @Query(" SELECT pro " +
            " FROM ProveedorEntity as pro " +
            " WHERE pro.proveedorPk.proveedorId = :id ")
    ProveedorEntity obtenerProveedor(@Param("id") String id);

}
