package euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.almacenes.dominio.entidades.AlmacenEntityPk;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity()
@Table(name = "proveedores", schema = "dbo")
@Data
public class ProveedorEntity {

    @EmbeddedId
    private ProveedorEntityPk proveedorPk;
    @Column(name = "xnombre")
    private String nombre;
    @Column(name = "xnomabrev")
    private String nombreAbrev;
    @Column(name = "xnif")
    private String nif;
    @Column(name = "xdomicilio")
    private String domicilio;
    @Column(name = "xpoblacion")
    private String poblacion;
    @Column(name = "xcod_postal")
    private String codigoPostal;
    @Column(name = "xprovincia_id")
    private String provinciaId;
    @Column(name = "xprovincia")
    private String provincia;
    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xpais")
    private String pais;
    @Column(name = "xtelefono")
    private String telefono;
    @Column(name = "xfax")
    private String fax;
    @Column(name = "xemail")
    private String email;
    @Column(name = "xsituacion")
    private String situacion;
    @Column(name = "xobservaciones")
    private String observaciones;
    @Column(name = "xinternet")
    private String internet;
    @Column(name = "xfecha_alta")
    private LocalDateTime fechaAlta;
    @Column(name = "xfecha_baja")
    private LocalDateTime fechaBaja;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "xcalificacion_id")
    private String calificacionId;
    @Column(name = "xxcnae")
    private String cnae;
    @Column(name = "xxnima")
    private String nima;

}
