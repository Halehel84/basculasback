package euroatomizado.idea.gestpes.maestros.proveedores.mapeadores;

import euroatomizado.idea.gestpes.maestros.proveedores.dominio.dto.ProveedorInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades.ProveedorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorProveedor {

    @Mapping(source = "proveedorPk.proveedorId", target = "proveedorId")
    @Mapping(source = "proveedorPk.empresa", target = "empresa")
    ProveedorInfoDto toInfoDto(ProveedorEntity datos);

    List<ProveedorInfoDto> toInfoDto(List<ProveedorEntity> datos);

    void actualizar(ProveedorEntity datosNueva, @MappingTarget ProveedorEntity datos);

}