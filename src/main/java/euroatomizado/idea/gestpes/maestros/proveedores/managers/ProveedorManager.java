package euroatomizado.idea.gestpes.maestros.proveedores.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.proveedores.dominio.dto.ProveedorInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades.ProveedorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProveedorManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ProveedorInfoDto> obtenerProveedores() {
        List<ProveedorEntity> lista = daoManager.getProveedorDao().findAll();
        return mapperManager.getMapeadorProveedor().toInfoDto(lista);
    }

}