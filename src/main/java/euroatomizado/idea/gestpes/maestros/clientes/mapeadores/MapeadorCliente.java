package euroatomizado.idea.gestpes.maestros.clientes.mapeadores;

import euroatomizado.idea.gestpes.maestros.clientes.dominio.dto.ClienteInfoDto;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorCliente {

    @Mapping(source = "clientePk.clienteId", target = "clienteId")
    ClienteInfoDto toInfoDto(ClienteEntity datos);

    List<ClienteInfoDto> toInfoDto(List<ClienteEntity> datos);

    void actualizar(ClienteEntity datosNueva, @MappingTarget ClienteEntity datos);

}