package euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity()
@Table(name = "clientes", schema = "dbo")
@Data
public class ClienteEntity {

    @EmbeddedId
    private ClienteEntityPk clientePk;
    @Column(name = "xnombre")
    private String nombre;
    @Column(name = "xnomabrev")
    private String nombreAbreviatura;
    @Column(name = "xnif")
    private String nif;
    @Column(name = "xdomicilio")
    private String domicilio;
    @Column(name = "xpoblacion")
    private String poblacion;
    @Column(name = "xcod_postal")
    private String codigoPostal;
    @Column(name = "xprovincia_id")
    private String provinciaId;
    @Column(name = "xprovincia")
    private String provincia;
    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xpais")
    private String pais;
    @Column(name = "xtelefono")
    private String telefono;
    @Column(name = "xfax")
    private String fax;
    @Column(name = "xemail")
    private String email;
    @Column(name = "xinternet")
    private String internet;
    @Column(name = "xfecha_alta")
    private LocalDateTime fechaAlta;
    @Column(name = "xfecha_baja")
    private LocalDateTime fechaBaja;
    @Column(name = "xsecpref_id")
    private String secprefId;
    @Column(name = "xcopias_alb")
    private Integer copiasAlb;
    @Column(name = "xcalificacion_id")
    private String calificacionId;
    @Column(name = "xsituacion")
    private String situacion;
    @Column(name = "xobservaciones")
    private String observaciones;

}
