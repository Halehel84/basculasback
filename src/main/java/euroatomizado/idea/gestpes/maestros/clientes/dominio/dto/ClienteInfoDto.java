package euroatomizado.idea.gestpes.maestros.clientes.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ClienteInfoDto {

    private String empresa;
    private String clienteId;
    private String nombre;
    private String nombreAbreviatura;
    private String nif;
    private String domicilio;
    private String poblacion;
    private String codigoPostal;
    private String provinciaId;
    private String provincia;
    private String paisId;
    private String pais;
    private String telefono;
    private String fax;
    private String email;
    private String internet;
    private LocalDateTime fechaAlta;
    private LocalDateTime fechaBaja;
    private String secprefId;
    private Integer copiasAlb;
    private String calificacionId;
    private String situacion;
    private String observaciones;

}
