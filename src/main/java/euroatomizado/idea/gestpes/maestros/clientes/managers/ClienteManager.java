package euroatomizado.idea.gestpes.maestros.clientes.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.dto.ClienteInfoDto;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ClienteInfoDto> obtenerClientes() {
        List<ClienteEntity> lista = daoManager.getClienteDao().findAll();
        return mapperManager.getMapeadorCliente().toInfoDto(lista);
    }

}
