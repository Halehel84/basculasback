package euroatomizado.idea.gestpes.maestros.clientes.dominio.daos;

import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteDao extends CrudRepository<ClienteEntity, ClienteEntityPk> {

    @Query(" SELECT cli " +
            " FROM ClienteEntity as cli " +
            " WHERE cli.clientePk.empresa = 'NPC' ")
    List<ClienteEntity> findAll();

    @Query(" SELECT cli " +
            " FROM ClienteEntity as cli " +
            " WHERE cli.clientePk.clienteId = :cliente " +
            " and cli.clientePk.empresa = :empresa ")
    ClienteEntity obtenerCliente(@Param("cliente") String cliente,
                                 @Param("empresa") String empresa);

}
