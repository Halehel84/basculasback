package euroatomizado.idea.gestpes.maestros.articulosPt.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.dto.ArticuloPtInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.entidades.ArticuloPtEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ArticuloPtManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ArticuloPtInfoDto> obtenerArticulos() {
        List<ArticuloPtEntity> lista = daoManager.getArticuloPtDao().findAll();
        return mapperManager.getMapeadorArticuloPt().toInfoDto(lista);
    }

}
