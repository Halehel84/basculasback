package euroatomizado.idea.gestpes.maestros.articulosPt.dominio.dto;

import lombok.Data;
import java.util.Date;

@Data
public class ArticuloPtInfoDto {

    private Integer aptCodigo;
    private String aptEmpresa;
    private String aptDescripcion;
    private String aptAlias;
    private String aptSecciones;
    private Integer tarCodigo;
    private Boolean aptBaja;
    private String aptColor;
    private String aptUsuAlta;
    private Date aptFechaAlta;
    private String aptUsuMod;
    private Date aptFechaMod;
    private Integer aptBase;
    private Integer aptColorEuroelettra;

}
