package euroatomizado.idea.gestpes.maestros.articulosPt.mapeadores;

import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.dto.ArticuloPtInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.entidades.ArticuloPtEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorArticuloPt {

    ArticuloPtInfoDto toInfoDto(ArticuloPtEntity datos);

    List<ArticuloPtInfoDto> toInfoDto(List<ArticuloPtEntity> datos);

    void actualizar(ArticuloPtEntity datosNueva, @MappingTarget ArticuloPtEntity datos);

}
