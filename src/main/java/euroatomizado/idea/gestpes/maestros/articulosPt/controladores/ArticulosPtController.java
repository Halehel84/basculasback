package euroatomizado.idea.gestpes.maestros.articulosPt.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.dto.ArticuloPtInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPt.managers.ArticuloPtManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/articulos-pt")
public class ArticulosPtController {

    @Autowired
    private ArticuloPtManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ArticuloPtInfoDto> obtenerArticulos() {
        return manager.obtenerArticulos();
    }

}
