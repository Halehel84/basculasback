package euroatomizado.idea.gestpes.maestros.articulosPt.dominio.daos;

import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.entidades.ArticuloPtEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticuloPtDao extends CrudRepository<ArticuloPtEntity, Integer> {
    List<ArticuloPtEntity> findAll();

    @Query(" SELECT art " +
            " FROM ArticuloPtEntity as art " +
            " WHERE art.aptCodigo = :codigo ")
    List<ArticuloPtEntity> obtenerArticuloPt(@Param("codigo") String codigo);

    @Query(" SELECT art.aptCodigo " +
            " FROM ArticuloPtEntity as art " +
            " WHERE art.tarCodigo = :codigo ")
    List<Integer> obtenerArticulosPorComposicion(@Param("codigo") int codigo);

}