package euroatomizado.idea.gestpes.maestros.articulosPt.dominio.entidades;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity()
@Table(name = "articulos_pt", schema = "dbo")
@Data
public class ArticuloPtEntity {

    @Id
    @Column(name = "apt_codigo")
    private Integer aptCodigo;
    @Column(name = "apt_empresa")
    private String aptEmpresa;
    @Column(name = "apt_descripcion")
    private String aptDescripcion;
    @Column(name = "apt_alias")
    private String aptAlias;
    @Column(name = "apt_secciones")
    private String aptSecciones;
    @Column(name = "tar_codigo")
    private Integer tarCodigo;
    @Column(name = "apt_baja")
    private Boolean aptBaja;
    @Column(name = "apt_color")
    private String aptColor;
    @Column(name = "apt_usu_alta")
    private String aptUsuAlta;
    @Column(name = "apt_fecha_alta")
    private Date aptFechaAlta;
    @Column(name = "apt_usu_mod")
    private String aptUsuMod;
    @Column(name = "apt_fecha_mod")
    private Date aptFechaMod;
    @Column(name = "apt_base")
    private Integer aptBase;
    @Column(name = "apt_color_euroelettra")
    private Integer aptColorEuroelettra;

}
