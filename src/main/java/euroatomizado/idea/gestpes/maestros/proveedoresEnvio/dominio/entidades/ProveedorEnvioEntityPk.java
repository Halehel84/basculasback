package euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ProveedorEnvioEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xproveedor_id")
    private String proveedorId;
    @Column(name = "xlocal_id")
    private String localId;

}
