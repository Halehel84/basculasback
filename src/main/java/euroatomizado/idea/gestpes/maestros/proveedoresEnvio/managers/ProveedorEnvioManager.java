package euroatomizado.idea.gestpes.maestros.proveedoresEnvio.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.dto.ProveedorEnvioInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.entidades.ProveedorEnvioEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProveedorEnvioManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ProveedorEnvioInfoDto> obtenerProveedoresEnvio() {
        List<ProveedorEnvioEntity> lista = daoManager.getProveedorEnvioDao().findAll();
        return mapperManager.getMapeadorProveedorEnvio().toInfoDto(lista);
    }

}