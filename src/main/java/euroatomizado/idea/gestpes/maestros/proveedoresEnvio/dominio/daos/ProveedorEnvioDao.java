package euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.daos;

import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.entidades.ProveedorEnvioEntity;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.entidades.ProveedorEnvioEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProveedorEnvioDao extends CrudRepository<ProveedorEnvioEntity, ProveedorEnvioEntityPk> {

    List<ProveedorEnvioEntity> findAll();

    @Query(" SELECT pro " +
            " FROM ProveedorEnvioEntity as pro " +
            " WHERE pro.proveedorEnvioPk.proveedorId = :id ")
    List<ProveedorEnvioEntity> obtenerProveedorEnvio(@Param("id") String id);

}
