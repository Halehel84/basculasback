package euroatomizado.idea.gestpes.maestros.proveedoresEnvio.mapeadores;

import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.dto.ProveedorEnvioInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.entidades.ProveedorEnvioEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorProveedorEnvio {

    @Mapping(source = "proveedorEnvioPk.proveedorId", target = "proveedorId")
    @Mapping(source = "proveedorEnvioPk.localId", target = "localId")
    @Mapping(source = "proveedorEnvioPk.empresa", target = "empresa")
    ProveedorEnvioInfoDto toInfoDto(ProveedorEnvioEntity datos);

    List<ProveedorEnvioInfoDto> toInfoDto(List<ProveedorEnvioEntity> datos);

    void actualizar(ProveedorEnvioEntity datosNueva, @MappingTarget ProveedorEnvioEntity datos);

}