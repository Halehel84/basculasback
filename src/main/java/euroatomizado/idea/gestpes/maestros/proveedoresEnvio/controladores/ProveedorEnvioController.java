package euroatomizado.idea.gestpes.maestros.proveedoresEnvio.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.dto.ProveedorEnvioInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.managers.ProveedorEnvioManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/proveedores-envio")
public class ProveedorEnvioController {

    @Autowired
    private ProveedorEnvioManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ProveedorEnvioInfoDto> obtenerProveedoresEnvio() {
        return manager.obtenerProveedoresEnvio();
    }

}