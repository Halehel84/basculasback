package euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.proveedores.dominio.entidades.ProveedorEntityPk;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity()
@Table(name = "proveedores_envio", schema = "dbo")
@Data
public class ProveedorEnvioEntity {

    @EmbeddedId
    private ProveedorEnvioEntityPk proveedorEnvioPk;
    @Column(name = "xnombre")
    private String nombre;
    @Column(name = "xhabitual")
    private Integer habitual;
    @Column(name = "xdomicilio")
    private String domicilio;
    @Column(name = "xpoblacion_id")
    private String poblacion;
    @Column(name = "xcod_postal")
    private String codigoPostal;
    @Column(name = "xprovincia_id")
    private String provinciaId;
    @Column(name = "xprovincia")
    private String provincia;
    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xpais")
    private String pais;
    @Column(name = "xtelefono")
    private String telefono;
    @Column(name = "xfax")
    private String fax;
    @Column(name = "xemail")
    private String email;
    @Column(name = "xxcnae")
    private String cnae;
    @Column(name = "xxnima")
    private String nima;

}
