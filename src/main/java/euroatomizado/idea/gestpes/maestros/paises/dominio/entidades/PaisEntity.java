package euroatomizado.idea.gestpes.maestros.paises.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name = "paises", schema = "dbo")
@Data
public class PaisEntity {

    @Id
    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xnombre")
    private String nombre;

}
