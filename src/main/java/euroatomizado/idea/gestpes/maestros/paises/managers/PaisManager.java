package euroatomizado.idea.gestpes.maestros.paises.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.paises.dominio.dto.PaisInfoDto;
import euroatomizado.idea.gestpes.maestros.paises.dominio.entidades.PaisEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PaisManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<PaisInfoDto> obtenerPaises() {
        List<PaisEntity> lista = daoManager.getPaisDao().findAll();
        return mapperManager.getMapeadorPais().toInfoDto(lista);
    }

}
