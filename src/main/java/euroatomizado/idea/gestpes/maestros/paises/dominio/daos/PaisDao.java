package euroatomizado.idea.gestpes.maestros.paises.dominio.daos;

import euroatomizado.idea.gestpes.maestros.paises.dominio.entidades.PaisEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisDao extends CrudRepository<PaisEntity, String> {

    List<PaisEntity> findAll();

    @Query(" SELECT pai " +
            " FROM PaisEntity as pai " +
            " WHERE pai.paisId = :id ")
    PaisEntity obtenerPais(@Param("id") String id);

}
