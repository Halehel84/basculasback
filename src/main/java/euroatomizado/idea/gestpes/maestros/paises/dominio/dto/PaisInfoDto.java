package euroatomizado.idea.gestpes.maestros.paises.dominio.dto;

import lombok.Data;

@Data
public class PaisInfoDto {

    private String paisId;
    private String nombre;

}
