package euroatomizado.idea.gestpes.maestros.paises.mapeadores;

import euroatomizado.idea.gestpes.maestros.paises.dominio.dto.PaisInfoDto;
import euroatomizado.idea.gestpes.maestros.paises.dominio.entidades.PaisEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorPais {

    PaisInfoDto toInfoDto(PaisEntity datos);

    List<PaisInfoDto> toInfoDto(List<PaisEntity> datos);

    void actualizar(PaisEntity datosNueva, @MappingTarget PaisEntity datos);

}