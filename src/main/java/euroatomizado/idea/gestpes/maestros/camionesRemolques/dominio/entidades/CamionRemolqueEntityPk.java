package euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class CamionRemolqueEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xmatricula")
    private String matricula;
    @Column(name = "xmatricula_rem")
    private String matriculaRemolque;

}
