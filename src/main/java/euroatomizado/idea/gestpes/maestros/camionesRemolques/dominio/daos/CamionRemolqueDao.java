package euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.daos;

import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.entidades.CamionRemolqueEntity;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.entidades.CamionRemolqueEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CamionRemolqueDao extends CrudRepository<CamionRemolqueEntity, CamionRemolqueEntityPk> {

    List<CamionRemolqueEntity> findAll();

    @Query(" SELECT cam " +
            " FROM CamionRemolqueEntity as cam " +
            " WHERE cam.camionRemolquePk.matricula = :matricula ")
    List<CamionRemolqueEntity> obtenerCamionRemolque(@Param("matricula") String matricula);

}
