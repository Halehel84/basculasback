package euroatomizado.idea.gestpes.maestros.camionesRemolques.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.dto.CamionRemolqueInfoDto;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.entidades.CamionRemolqueEntity;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades.TransportistaCamionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CamionRemolqueManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<CamionRemolqueInfoDto> obtenerCamionesRemolque() {
        List<CamionRemolqueEntity> lista = daoManager.getCamionRemolqueDao().findAll();
        return mapperManager.getMapeadorCamionRemolque().toInfoDto(lista);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<CamionRemolqueInfoDto> obtenerRemolquesPorCamion(String id) {
        List<CamionRemolqueEntity> lista = daoManager.getCamionRemolqueDao().obtenerCamionRemolque(id);
        return mapperManager.getMapeadorCamionRemolque().toInfoDto(lista);
    }
}
