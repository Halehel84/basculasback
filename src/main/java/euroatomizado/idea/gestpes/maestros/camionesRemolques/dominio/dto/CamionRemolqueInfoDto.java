package euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class CamionRemolqueInfoDto {

    private String empresa;
    private String matricula;
    private String matriculaRemolque;
    private Integer habitual;
    private Integer ejes;
    private Float pma;
    private String id;
    private LocalDateTime fecha_modif;

}
