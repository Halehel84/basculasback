package euroatomizado.idea.gestpes.maestros.camionesRemolques.mapeadores;

import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.dto.CamionRemolqueInfoDto;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.entidades.CamionRemolqueEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorCamionRemolque {

    @Mapping(source = "camionRemolquePk.matricula", target = "matricula")
    @Mapping(source = "camionRemolquePk.empresa", target = "empresa")
    @Mapping(source = "camionRemolquePk.matriculaRemolque", target = "matriculaRemolque")
    CamionRemolqueInfoDto toInfoDto(CamionRemolqueEntity datos);

    List<CamionRemolqueInfoDto> toInfoDto(List<CamionRemolqueEntity> datos);

    void actualizar(CamionRemolqueEntity datosNueva, @MappingTarget CamionRemolqueEntity datos);

}
