package euroatomizado.idea.gestpes.maestros.camionesRemolques.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.dto.CamionRemolqueInfoDto;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.managers.CamionRemolqueManager;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.PathParam;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/camiones-remolques")
public class CamionRemolqueController {

    @Autowired
    private CamionRemolqueManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<CamionRemolqueInfoDto> obtenerCamionesRemolque() {
        return manager.obtenerCamionesRemolque();
    }

    @ResponseStatus(OK)
    @GetMapping(value = "por-camion")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<CamionRemolqueInfoDto> obtenerRemolquePorCamion(@PathParam("id") String id, @PathParam("empresa") String empresa) {
        return manager.obtenerRemolquesPorCamion(id);
    }
}
