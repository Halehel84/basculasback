package euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "camiones_remolques", schema = "dbo")
@Data
public class CamionRemolqueEntity {

    @EmbeddedId
    private CamionRemolqueEntityPk camionRemolquePk;
    @Column(name = "xhabitual")
    private Integer habitual;
    @Column(name = "xejes")
    private Integer ejes;
    @Column(name = "xpma", columnDefinition = "real")
    private Float pma;
    @Column(name = "xfecha_modif")
    private LocalDateTime fecha_modif;

}
