package euroatomizado.idea.gestpes.maestros.articulosPruebas.mapeadores;

import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.dto.ArticuloPruebaFormDto;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.dto.ArticuloPruebaInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.entidades.ArticuloPruebaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorArticuloPrueba {

    @Mapping(source = "articuloPruebaPk.articuloId", target = "articuloId")
    @Mapping(source = "articuloPruebaPk.pruebaId", target = "pruebaId")
    ArticuloPruebaInfoDto toInfoDto(ArticuloPruebaEntity datos);

    List<ArticuloPruebaInfoDto> toInfoDto(List<ArticuloPruebaEntity> datos);

    ArticuloPruebaEntity toEntidad(ArticuloPruebaFormDto dto);

    void actualizar(ArticuloPruebaEntity datosNueva, @MappingTarget ArticuloPruebaEntity datos);

}
