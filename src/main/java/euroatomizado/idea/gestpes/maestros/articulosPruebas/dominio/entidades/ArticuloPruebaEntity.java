package euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity()
@Table(name = "articulos_pruebas", schema = "dbo")
@Data
public class ArticuloPruebaEntity {

    @EmbeddedId
    private ArticuloPruebaEntityPk articuloPruebaPk;
    @Column(name = "prueba_desc")
    private String pruebaDescripcion;

}
