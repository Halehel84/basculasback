package euroatomizado.idea.gestpes.maestros.articulosPruebas.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.dto.ArticuloPruebaInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.entidades.ArticuloPruebaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ArticuloPruebaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ArticuloPruebaInfoDto> obtenerArticulos() {
        List<ArticuloPruebaEntity> lista = daoManager.getArticuloPruebaDao().findAll();
        return mapperManager.getMapeadorArticuloPrueba().toInfoDto(lista);
    }

}
