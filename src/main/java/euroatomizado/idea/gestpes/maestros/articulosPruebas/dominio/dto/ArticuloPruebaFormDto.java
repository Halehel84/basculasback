package euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.dto;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class ArticuloPruebaFormDto {

    @Size(min = 1, max = 4)
    private String empresa;
    @Size(min = 1, max = 25)
    private String articuloId;
    @Size(min = 1, max = 25)
    private String pruebaId;
    @Size(min = 1, max = 200)
    private String pruebaDescripcion;

}
