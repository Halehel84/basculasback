package euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.daos;

import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.entidades.ArticuloPruebaEntity;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.entidades.ArticuloPruebaEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticuloPruebaDao extends CrudRepository<ArticuloPruebaEntity, ArticuloPruebaEntityPk> {
    List<ArticuloPruebaEntity> findAll();

    @Query(" SELECT art " +
            " FROM ArticuloPruebaEntity as art " +
            " WHERE art.articuloPruebaPk.articuloId = :articulo ")
    List<ArticuloPruebaEntity> obtenerArticuloPrueba(@Param("articulo") String articulo);
}
