package euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ArticuloPruebaEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xarticulo_id")
    private String articuloId;
    @Column(name = "prueba_id")
    private String pruebaId;

}
