package euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.dto;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class ArticuloPruebaInfoDto {

    private String empresa;
    private String articuloId;
    private String pruebaId;
    private String pruebaDescripcion;

}
