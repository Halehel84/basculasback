package euroatomizado.idea.gestpes.maestros.articulosPruebas.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.dto.ArticuloPruebaInfoDto;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.managers.ArticuloPruebaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/articulos-pruebas")
public class ArticuloPruebaController {

    @Autowired
    private ArticuloPruebaManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ArticuloPruebaInfoDto> obtenerArticulos() {
        return manager.obtenerArticulos();
    }

}
