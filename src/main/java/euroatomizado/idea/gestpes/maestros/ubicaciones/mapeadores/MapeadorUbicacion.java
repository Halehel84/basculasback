package euroatomizado.idea.gestpes.maestros.ubicaciones.mapeadores;

import euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.dto.UbicacionInfoDto;
import euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.entidades.UbicacionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorUbicacion {

    UbicacionInfoDto toInfoDto(UbicacionEntity datos);

    List<UbicacionInfoDto> toInfoDto(List<UbicacionEntity> datos);

    void actualizar(UbicacionEntity datosNueva, @MappingTarget UbicacionEntity datos);

}