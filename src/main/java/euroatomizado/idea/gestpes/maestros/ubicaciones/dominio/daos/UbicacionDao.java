package euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.daos;

import euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.entidades.UbicacionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UbicacionDao extends CrudRepository<UbicacionEntity, Integer> {

    List<UbicacionEntity> findAll();

    @Query(" SELECT ubi " +
            " FROM UbicacionEntity as ubi " +
            " WHERE ubi.ubiCodigo = :id ")
    UbicacionEntity obtenerUbicacion(@Param("id") String id);

}