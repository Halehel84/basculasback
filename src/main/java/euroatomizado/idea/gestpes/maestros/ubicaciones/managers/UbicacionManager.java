package euroatomizado.idea.gestpes.maestros.ubicaciones.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.dto.UbicacionInfoDto;
import euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.entidades.UbicacionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UbicacionManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<UbicacionInfoDto> obtenerUbicaciones() {
        List<UbicacionEntity> lista = daoManager.getUbicacionDao().findAll();
        return mapperManager.getMapeadorUbicacion().toInfoDto(lista);
    }

}