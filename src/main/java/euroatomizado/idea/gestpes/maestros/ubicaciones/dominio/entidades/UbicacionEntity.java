package euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity()
@Table(name = "ubicaciones", schema = "dbo")
@Data
public class UbicacionEntity {

    @Id
    @Column(name = "ubi_codigo")
    private Integer ubiCodigo;
    @Column(name = "ubi_empresa")
    private String ubiEmpresa;
    @Column(name = "ubi_padre")
    private Integer ubiPadre;
    @Column(name = "ubi_cam_desc")
    private String ubiCaminoDesc;
    @Column(name = "ubi_camino")
    private String ubiCamino;
    @Column(name = "ubi_descripcion")
    private String ubiDescripcion;
    @Column(name = "ubn_codigo")
    private Integer ubnCodigo;
    @Column(name = "ubi_orden")
    private Integer ubiOrden;
    @Column(name = "xseccion_id")
    private String xseccionId;
    @Column(name = "alm_codigo")
    private String almCodigo;
    @Column(name = "ubi_baja")
    private String ubiBaja;
    @Column(name = "ubi_usu_alta")
    private String ubiUsuarioAlta;
    @Column(name = "ubi_fecha_alta")
    private LocalDateTime ubiFechaAlta;
    @Column(name = "ubi_fecha_mod")
    private LocalDateTime ubiFechaMod;
    @Column(name = "ubi_usu_mod")
    private String ubiUsuarioMod;
    @Column(name = "ubi_camino_orden")
    private String ubiCaminoOrden;

}