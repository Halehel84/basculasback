package euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UbicacionInfoDto {

    private Integer ubiCodigo;
    private String ubiEmpresa;
    private Integer ubiPadre;
    private String ubiCaminoDesc;
    private String ubiCamino;
    private String ubiDescripcion;
    private Integer ubnCodigo;
    private Integer ubiOrden;
    private String xseccionId;
    private String almCodigo;
    private String ubiBaja;
    private String ubiUsuarioAlta;
    private LocalDateTime ubiFechaAlta;
    private LocalDateTime ubiFechaMod;
    private String ubiUsuarioMod;
    private String ubiCaminoOrden;

}
