package euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.dto;

import lombok.Data;

@Data
public class ProveedorArticuloInfoDto {

    private String empresa;
    private String proveedorId;
    private String articuloId;
    private String situacion;
    private String descripcionProv;

}
