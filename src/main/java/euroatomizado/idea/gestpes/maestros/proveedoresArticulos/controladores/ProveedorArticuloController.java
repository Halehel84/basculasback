package euroatomizado.idea.gestpes.maestros.proveedoresArticulos.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.dto.ProveedorArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.managers.ProveedorArticuloManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/proveedores-articulo")
public class ProveedorArticuloController {

    @Autowired
    private ProveedorArticuloManager manager;

    @ResponseStatus(OK)
    @GetMapping
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ProveedorArticuloInfoDto> obtenerProveedoresArticulo() {
        return manager.obtenerProveedoresArticulo();
    }

}
