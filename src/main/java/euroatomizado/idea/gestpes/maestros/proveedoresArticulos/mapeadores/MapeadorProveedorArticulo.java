package euroatomizado.idea.gestpes.maestros.proveedoresArticulos.mapeadores;

import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.dto.ProveedorArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.entidades.ProveedorArticuloEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorProveedorArticulo {

    @Mapping(source = "proveedorArticuloPk.proveedorId", target = "proveedorId")
    @Mapping(source = "proveedorArticuloPk.articuloId", target = "articuloId")
    @Mapping(source = "proveedorArticuloPk.empresa", target = "empresa")
    ProveedorArticuloInfoDto toInfoDto(ProveedorArticuloEntity datos);

    List<ProveedorArticuloInfoDto> toInfoDto(List<ProveedorArticuloEntity> datos);

    void actualizar(ProveedorArticuloEntity datosNueva, @MappingTarget ProveedorArticuloEntity datos);

}