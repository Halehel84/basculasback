package euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity()
@Table(name = "proveedores_articulos", schema = "dbo")
@Data
public class ProveedorArticuloEntity {

    @EmbeddedId
    private ProveedorArticuloEntityPk proveedorArticuloPk;
    @Column(name = "xsituacion")
    private String situacion;
    @Column(name = "xdescripcion_prov")
    private String descripcionProv;

}
