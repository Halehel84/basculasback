package euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.daos;

import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.entidades.ProveedorArticuloEntity;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.entidades.ProveedorArticuloEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProveedorArticuloDao extends CrudRepository<ProveedorArticuloEntity, ProveedorArticuloEntityPk> {

    List<ProveedorArticuloEntity> findAll();

    @Query(" SELECT pro " +
            " FROM ProveedorArticuloEntity as pro " +
            " WHERE pro.proveedorArticuloPk.proveedorId = :id ")
    List<ProveedorArticuloEntity> obtenerProveedor(@Param("id") String id);

}
