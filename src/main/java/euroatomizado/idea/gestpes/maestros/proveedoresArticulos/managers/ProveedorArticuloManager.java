package euroatomizado.idea.gestpes.maestros.proveedoresArticulos.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.dto.ProveedorArticuloInfoDto;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.entidades.ProveedorArticuloEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class ProveedorArticuloManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ProveedorArticuloInfoDto> obtenerProveedoresArticulo() {
        List<ProveedorArticuloEntity> lista = daoManager.getProveedorArticuloDao().findAll();
        return mapperManager.getMapeadorProveedorArticulo().toInfoDto(lista);
    }

}