package euroatomizado.idea.gestpes.maestros.provincias.mapeadores;

import euroatomizado.idea.gestpes.maestros.provincias.dominio.dto.ProvinciaInfoDto;
import euroatomizado.idea.gestpes.maestros.provincias.dominio.entidades.ProvinciaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorProvincia {

    @Mapping(source = "provinciaPk.provinciaId", target = "provinciaId")
    @Mapping(source = "provinciaPk.paisId", target = "paisId")
    ProvinciaInfoDto toInfoDto(ProvinciaEntity datos);

    List<ProvinciaInfoDto> toInfoDto(List<ProvinciaEntity> datos);

    void actualizar(ProvinciaEntity datosNueva, @MappingTarget ProvinciaEntity datos);

}