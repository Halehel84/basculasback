package euroatomizado.idea.gestpes.maestros.provincias.dominio.dto;

import lombok.Data;

@Data
public class ProvinciaInfoDto {

    private String paisId;
    private String provinciaId;
    private String nombre;

}
