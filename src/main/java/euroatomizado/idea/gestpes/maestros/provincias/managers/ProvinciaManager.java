package euroatomizado.idea.gestpes.maestros.provincias.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.provincias.dominio.dto.ProvinciaInfoDto;
import euroatomizado.idea.gestpes.maestros.provincias.dominio.entidades.ProvinciaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProvinciaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ProvinciaInfoDto> obtenerProvincias() {
        List<ProvinciaEntity> lista = daoManager.getProvinciaDao().findAll();
        return mapperManager.getMapeadorProvincia().toInfoDto(lista);
    }

}
