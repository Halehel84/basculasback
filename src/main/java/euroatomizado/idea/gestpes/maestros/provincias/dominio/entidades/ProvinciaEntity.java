package euroatomizado.idea.gestpes.maestros.provincias.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity()
@Table(name = "provincias", schema = "dbo")
@Data
public class ProvinciaEntity {

    @EmbeddedId
    private ProvinciaEntityPk provinciaPk;
    @Column(name = "xnombre")
    private String nombre;

}
