package euroatomizado.idea.gestpes.maestros.provincias.dominio.daos;

import euroatomizado.idea.gestpes.maestros.provincias.dominio.entidades.ProvinciaEntity;
import euroatomizado.idea.gestpes.maestros.provincias.dominio.entidades.ProvinciaEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProvinciaDao extends CrudRepository<ProvinciaEntity, ProvinciaEntityPk> {

    List<ProvinciaEntity> findAll();

    @Query(" SELECT pro " +
            " FROM ProvinciaEntity as pro " +
            " WHERE pro.provinciaPk.provinciaId = :id ")
    ProvinciaEntity obtenerProvincia(@Param("id") String id);

}
