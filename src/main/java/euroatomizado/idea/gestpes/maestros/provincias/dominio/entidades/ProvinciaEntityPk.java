package euroatomizado.idea.gestpes.maestros.provincias.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ProvinciaEntityPk implements Serializable {

    @Column(name = "xpais_id")
    private String paisId;
    @Column(name = "xprovincia_id")
    private String provinciaId;

}