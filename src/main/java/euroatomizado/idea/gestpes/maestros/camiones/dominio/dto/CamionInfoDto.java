package euroatomizado.idea.gestpes.maestros.camiones.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CamionInfoDto {

    private String empresa;
    private String matricula;
    private String conductorId;
    private String calificacionId;
    private String situacion;
    private String observaciones;
    private LocalDateTime fechaModificacion;

}
