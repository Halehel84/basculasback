package euroatomizado.idea.gestpes.maestros.camiones.mapeadores;

import euroatomizado.idea.gestpes.maestros.camiones.dominio.dto.CamionInfoDto;
import euroatomizado.idea.gestpes.maestros.camiones.dominio.entidades.CamionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorCamion {

    @Mapping(source = "camionPk.empresa", target = "empresa")
    @Mapping(source = "camionPk.matricula", target = "matricula")
    CamionInfoDto toInfoDto(CamionEntity datos);

    List<CamionInfoDto> toInfoDto(List<CamionEntity> datos);

    void actualizar(CamionEntity datosNueva, @MappingTarget CamionEntity datos);

}
