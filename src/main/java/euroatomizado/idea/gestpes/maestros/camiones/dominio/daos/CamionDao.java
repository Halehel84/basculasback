package euroatomizado.idea.gestpes.maestros.camiones.dominio.daos;

import euroatomizado.idea.gestpes.maestros.camiones.dominio.entidades.CamionEntity;
import euroatomizado.idea.gestpes.maestros.camiones.dominio.entidades.CamionEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CamionDao extends CrudRepository<CamionEntity, CamionEntityPk> {

    List<CamionEntity> findAll();

    @Query(" SELECT cam " +
            " FROM CamionEntity as cam " +
            " WHERE cam.camionPk.matricula = :camion ")
    List<CamionEntity> obtenerCamion(@Param("camion") String camion);

}
