package euroatomizado.idea.gestpes.maestros.camiones.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Entity()
@Table(name = "camiones", schema = "dbo")
@Data
public class CamionEntity {

    @EmbeddedId
    private CamionEntityPk camionPk;
    @Column(name = "xconductor_id")
    private String conductorId;
    @Column(name = "xcalificacion_id")
    private String calificacionId;
    @Column(name = "xsituacion")
    private String situacion;
    @Column(name = "xobservaciones")
    private String observaciones;
    @Column(name = "xfecha_modif")
    private LocalDateTime fechaModificacion;

}
