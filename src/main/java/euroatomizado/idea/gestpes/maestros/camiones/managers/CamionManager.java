package euroatomizado.idea.gestpes.maestros.camiones.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.camiones.dominio.dto.CamionInfoDto;
import euroatomizado.idea.gestpes.maestros.camiones.dominio.entidades.CamionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CamionManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<CamionInfoDto> obtenerCamiones() {
        List<CamionEntity> lista = daoManager.getCamionDao().findAll();
        return mapperManager.getMapeadorCamion().toInfoDto(lista);
    }

}