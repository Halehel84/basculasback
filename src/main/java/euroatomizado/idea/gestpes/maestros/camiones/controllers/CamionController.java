package euroatomizado.idea.gestpes.maestros.camiones.controllers;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.camiones.dominio.dto.CamionInfoDto;
import euroatomizado.idea.gestpes.maestros.camiones.managers.CamionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/camiones")
public class CamionController {

    @Autowired
    private CamionManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<CamionInfoDto> obtenerCamiones() {
        return manager.obtenerCamiones();
    }

}
