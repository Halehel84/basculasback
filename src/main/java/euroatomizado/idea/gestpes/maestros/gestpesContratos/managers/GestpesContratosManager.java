package euroatomizado.idea.gestpes.maestros.gestpesContratos.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.dto.GestpesContratosInfoDto;
import euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.entidades.GestpesContratosEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GestpesContratosManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<GestpesContratosInfoDto> obtenerContratos() {
        List<GestpesContratosEntity> lista = daoManager.getGestpesContratosDao().findAll();
        return mapperManager.getMapeadorGestpesContratos().toInfoDto(lista);
    }

}