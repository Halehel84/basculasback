package euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.dto;

import lombok.Data;

import java.util.Date;

@Data
public class GestpesContratosInfoDto {

    private Integer gconCodigo;
    private String gconEmpresa;
    private String xseccionId;
    private String xproveedorId;
    private String xclienteId;
    private String xarticuloId;
    private Integer gconNumeroContrato;
    private Date gconFechaContrato;
    private String gconTratamiento;
    private String gconVigencia;
    private String xlocalId;
    private String gconEmail;
    private String gconUsuarioAlta;
    private String gconUsuarioModificacion;
    private Date gconFechaAlta;
    private Date gconFechaModificacion;

}
