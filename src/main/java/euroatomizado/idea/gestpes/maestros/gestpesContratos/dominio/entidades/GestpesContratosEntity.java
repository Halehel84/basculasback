package euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.entidades;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity()
@Table(name = "gestpes_contratos", schema = "dbo")
@Data
public class GestpesContratosEntity {

    @Id
    @Column(name = "gcon_codigo")
    private Integer gconCodigo;
    @Column(name = "gcon_empresa")
    private String gconEmpresa;
    @Column(name = "xseccion_id")
    private String xseccionId;
    @Column(name = "xproveedor_id")
    private String xproveedorId;
    @Column(name = "xcliente_id")
    private String xclienteId;
    @Column(name = "xarticulo_id")
    private String xarticuloId;
    @Column(name = "gcon_numero_contrato")
    private Integer gconNumeroContrato;
    @Column(name = "gcon_fecha_contrato")
    private Date gconFechaContrato;
    @Column(name = "gcon_tratamiento")
    private String gconTratamiento;
    @Column(name = "gcon_vigencia")
    private String gconVigencia;
    @Column(name = "xlocal_id")
    private String xlocalId;
    @Column(name = "gcon_email")
    private String gconEmail;
    @Column(name = "gcon_usu_alta")
    private String gconUsuarioAlta;
    @Column(name = "gcon_usu_mod")
    private String gconUsuarioModificacion;
    @Column(name = "gcon_fecha_alta")
    private Date gconFechaAlta;
    @Column(name = "gcon_fecha_mod")
    private Date gconFechaModificacion;

}