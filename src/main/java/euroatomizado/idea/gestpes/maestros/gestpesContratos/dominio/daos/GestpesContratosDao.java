package euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.daos;

import euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.entidades.GestpesContratosEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GestpesContratosDao extends CrudRepository<GestpesContratosEntity, Integer> {

    List<GestpesContratosEntity> findAll();

    @Query(" SELECT con " +
            " FROM GestpesContratosEntity as con " +
            " WHERE con.gconCodigo = :codigo ")
    GestpesContratosEntity obtenerContrato(@Param("codigo") String codigo);

}