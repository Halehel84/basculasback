package euroatomizado.idea.gestpes.maestros.gestpesContratos.mapeadores;

import euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.dto.GestpesContratosInfoDto;
import euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.entidades.GestpesContratosEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorGestpesContratos {

    GestpesContratosInfoDto toInfoDto(GestpesContratosEntity datos);

    List<GestpesContratosInfoDto> toInfoDto(List<GestpesContratosEntity> datos);

    void actualizar(GestpesContratosEntity datosNueva, @MappingTarget GestpesContratosEntity datos);

}