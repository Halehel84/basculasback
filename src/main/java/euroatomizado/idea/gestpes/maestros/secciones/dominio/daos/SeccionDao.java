package euroatomizado.idea.gestpes.maestros.secciones.dominio.daos;

import euroatomizado.idea.gestpes.maestros.secciones.dominio.entidades.SeccionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeccionDao extends CrudRepository<SeccionEntity, String> {

    List<SeccionEntity> findAll();

    @Query(" SELECT sec " +
            " FROM SeccionEntity as sec " +
            " WHERE sec.seccionId = :id ")
    SeccionEntity obtenerSeccion(@Param("id") String id);

}