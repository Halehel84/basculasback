package euroatomizado.idea.gestpes.maestros.secciones.dominio.dto;

import lombok.Data;

@Data
public class SeccionInfoDto {

    private String seccionId;
    private String nombre;
    private String nombreAbrev;
    private String cif;
    private String domicilio;
    private String poblacion;
    private String codigoPostal;
    private String telefono;
    private String fax;
    private String cnae;
    private String nima;
    private String pieReport;

}
