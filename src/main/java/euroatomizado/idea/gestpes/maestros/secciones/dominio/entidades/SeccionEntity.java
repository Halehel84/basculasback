package euroatomizado.idea.gestpes.maestros.secciones.dominio.entidades;

import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "secciones", schema = "dbo")
@Data
public class SeccionEntity {

    @Id
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "xnombre")
    private String nombre;
    @Column(name = "xnombre_abrev")
    private String nombreAbrev;
    @Column(name = "xcif")
    private String cif;
    @Column(name = "xdomicilio")
    private String domicilio;
    @Column(name = "xpoblacion")
    private String poblacion;
    @Column(name = "xcod_postal")
    private String codigoPostal;
    @Column(name = "xtelefono")
    private String telefono;
    @Column(name = "xfax")
    private String fax;
    @Column(name = "xxcnae")
    private String cnae;
    @Column(name = "xxnima")
    private String nima;
    @Column(name = "xpie_report")
    private String pieReport;

}