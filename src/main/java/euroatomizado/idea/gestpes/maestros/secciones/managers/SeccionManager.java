package euroatomizado.idea.gestpes.maestros.secciones.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.secciones.dominio.dto.SeccionInfoDto;
import euroatomizado.idea.gestpes.maestros.secciones.dominio.entidades.SeccionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SeccionManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<SeccionInfoDto> obtenerSecciones() {
        List<SeccionEntity> lista = daoManager.getSeccionDao().findAll();
        return mapperManager.getMapeadorSeccion().toInfoDto(lista);
    }

}