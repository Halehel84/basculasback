package euroatomizado.idea.gestpes.maestros.secciones.mapeadores;

import euroatomizado.idea.gestpes.maestros.secciones.dominio.dto.SeccionInfoDto;
import euroatomizado.idea.gestpes.maestros.secciones.dominio.entidades.SeccionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorSeccion {

    SeccionInfoDto toInfoDto(SeccionEntity datos);

    List<SeccionInfoDto> toInfoDto(List<SeccionEntity> datos);

    void actualizar(SeccionEntity datosNueva, @MappingTarget SeccionEntity datos);

}