package euroatomizado.idea.gestpes.maestros.clientesLimites.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.dto.ClienteLimiteInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.entidades.ClienteLimiteEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteLimiteManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ClienteLimiteInfoDto> obtenerClientesLimites() {
        List<ClienteLimiteEntity> lista = daoManager.getClienteLimiteDao().findAll();
        return mapperManager.getMapeadorClienteLimite().toInfoDto(lista);
    }

}
