package euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.dto;

import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.entidades.ClienteLimiteEntityPk;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ClienteLimiteInfoDto {

    private String empresa;
    private String clienteId;
    private String articuloId;
    private String seccionId;
    private ClienteLimiteEntityPk clienteLimitePk;
    private LocalDateTime fechaInicio;
    private Float pesajeMax;

}
