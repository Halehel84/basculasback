package euroatomizado.idea.gestpes.maestros.clientesLimites.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.dto.ClienteLimiteInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesLimites.managers.ClienteLimiteManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/clientes-limites")
public class ClienteLimiteController {

    @Autowired
    private ClienteLimiteManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ClienteLimiteInfoDto> obtenerClientesLimites() {
        return manager.obtenerClientesLimites();
    }

}
