package euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity()
@Table(name = "clientes_limites", schema = "dbo")
@Data
public class ClienteLimiteEntity {

    @EmbeddedId
    private ClienteLimiteEntityPk clienteLimitePk;
    @Column(name = "xfecha_inicio")
    private LocalDateTime fechaInicio;
    @Column(name = "xpesaje_max", columnDefinition = "real")
    private Float pesajeMax;

}
