package euroatomizado.idea.gestpes.maestros.clientesLimites.mapeadores;

import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.dto.ClienteLimiteInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.entidades.ClienteLimiteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorClienteLimite {

    @Mapping(source = "clienteLimitePk.empresa", target = "empresa")
    @Mapping(source = "clienteLimitePk.clienteId", target = "clienteId")
    @Mapping(source = "clienteLimitePk.articuloId", target = "articuloId")
    @Mapping(source = "clienteLimitePk.seccionId", target = "seccionId")
    ClienteLimiteInfoDto toInfoDto(ClienteLimiteEntity datos);

    List<ClienteLimiteInfoDto> toInfoDto(List<ClienteLimiteEntity> datos);

    void actualizar(ClienteLimiteEntity datosNueva, @MappingTarget ClienteLimiteEntity datos);

}