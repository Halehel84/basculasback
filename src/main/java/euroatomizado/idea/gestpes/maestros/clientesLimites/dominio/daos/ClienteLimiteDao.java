package euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.daos;

import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.entidades.ClienteLimiteEntity;
import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.entidades.ClienteLimiteEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteLimiteDao extends CrudRepository<ClienteLimiteEntity, ClienteLimiteEntityPk> {

    List<ClienteLimiteEntity> findAll();

    @Query(" SELECT cli " +
            " FROM ClienteLimiteEntity as cli " +
            " WHERE cli.clienteLimitePk.clienteId = :cliente ")
    List<ClienteLimiteEntity> obtenerClienteLimite(@Param("cliente") String cliente);

}