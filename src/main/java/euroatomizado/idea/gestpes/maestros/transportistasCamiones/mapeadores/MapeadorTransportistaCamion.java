package euroatomizado.idea.gestpes.maestros.transportistasCamiones.mapeadores;

import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionFormDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades.TransportistaCamionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorTransportistaCamion {

    @Mapping(source = "transportistaCamionPk.transportistaId", target = "transportistaId")
    @Mapping(source = "transportistaCamionPk.matricula", target = "matricula")
    @Mapping(source = "transportistaCamionPk.transportistaId", target = "id")
    @Mapping(source = "transportistaCamionPk.matricula", target = "descripcion")
    @Mapping(source = "transportistaCamionPk.empresa", target = "empresa")
    TransportistaCamionInfoDto toInfoDto(TransportistaCamionEntity datos);

    List<TransportistaCamionInfoDto> toInfoDto(List<TransportistaCamionEntity> datos);

    TransportistaCamionEntity toEntidad(TransportistaCamionFormDto dto);

    void actualizar(TransportistaCamionEntity datosNueva, @MappingTarget TransportistaCamionEntity datos);

}