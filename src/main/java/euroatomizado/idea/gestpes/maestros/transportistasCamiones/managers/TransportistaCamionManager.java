package euroatomizado.idea.gestpes.maestros.transportistasCamiones.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.dto.ClienteTransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.entidades.ClienteTransportistaEntity;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionFormDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades.TransportistaCamionEntity;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades.TransportistaCamionEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class TransportistaCamionManager implements PrimaryKeyStringManagerInterface<TransportistaCamionInfoDto,
        TransportistaCamionEntity, TransportistaCamionEntityPk, TransportistaCamionFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<TransportistaCamionInfoDto> obtenerTransportistas() {
        return obtenerDatosSalida(daoManager.getTransportistaCamionDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TransportistaCamionInfoDto> obtenerCamionesPorTransportista(String id) {
        List<TransportistaCamionEntity> lista = daoManager.getTransportistaCamionDao().obtenerCamionesPorTransportista(id);
        return mapperManager.getMapeadorTransportistaCamion().toInfoDto(lista);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TransportistaCamionInfoDto guardar(TransportistaCamionFormDto datos) {
        TransportistaCamionEntity entity = mapperManager.getMapeadorTransportistaCamion().toEntidad(datos);
        entity.setTransportistaCamionFormDto(datos);
        try {
            if (!existe(entity.getTransportistaCamionPk()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public TransportistaCamionEntity guardarNuevo(TransportistaCamionEntity nuevo) {
        String id = nuevo.getTransportistaCamionFormDto().getTransportistaId();
        nuevo.setTransportistaCamionPk(obtenerPrimaryKey(id));
        daoManager.getTransportistaCamionDao().save(nuevo);
        return nuevo;
    }

    @Override
    public TransportistaCamionEntity modificar(TransportistaCamionEntity nuevo) {
        TransportistaCamionEntity entity = obtener(nuevo);
        mapperManager.getMapeadorTransportistaCamion().actualizar(nuevo, entity);
        daoManager.getTransportistaCamionDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        TransportistaCamionEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getTransportistaCamionDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public TransportistaCamionInfoDto obtenerDatosSalida(TransportistaCamionEntity entity) {
        return mapperManager.getMapeadorTransportistaCamion().toInfoDto(entity);
    }

    private List<TransportistaCamionInfoDto> obtenerDatosSalida(List<TransportistaCamionEntity> entidades) {
        return mapperManager.getMapeadorTransportistaCamion().toInfoDto(entidades);
    }

    @Override
    public boolean existe(TransportistaCamionEntityPk pk) {
        return pk != null && daoManager.getTransportistaCamionDao().exists(pk);
    }

    @Override
    public TransportistaCamionEntity obtener(TransportistaCamionEntity datos) {
        return daoManager.getTransportistaCamionDao().findOne(datos.getTransportistaCamionPk());
    }

    @Override
    public TransportistaCamionEntityPk obtenerPrimaryKey(String id) {
        TransportistaCamionEntityPk pk = new TransportistaCamionEntityPk();
        pk.setTransportistaId(id);
        pk.setEmpresa(EMPRESA);
        return pk;
    }

}
