package euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class TransportistaCamionEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xtransportista_id")
    private String transportistaId;
    @Column(name = "xmatricula")
    private String matricula;

}
