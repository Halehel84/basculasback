package euroatomizado.idea.gestpes.maestros.transportistasCamiones.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.dto.ClienteTransportistaInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionFormDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionInfoDto;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.managers.TransportistaCamionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.PathParam;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/transportistas-camiones")
public class TransportistaCamionController {

    @Autowired
    private TransportistaCamionManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<TransportistaCamionInfoDto> obtenerTransportistas() {
        return manager.obtenerTransportistas();
    }

    @ResponseStatus(OK)
    @GetMapping(value = "por-transportista")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA + "')")
    public List<TransportistaCamionInfoDto> obtenerCamionesPorTransportista(@PathParam("id") String id, @PathParam("empresa") String empresa) {
        return manager.obtenerCamionesPorTransportista(id);
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public TransportistaCamionInfoDto crear(@RequestBody @Valid TransportistaCamionFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public TransportistaCamionInfoDto modificar(@RequestBody @Valid TransportistaCamionFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
