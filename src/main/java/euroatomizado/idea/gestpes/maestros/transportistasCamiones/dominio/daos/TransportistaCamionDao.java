package euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.daos;

import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.entidades.ClienteTransportistaEntity;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades.TransportistaCamionEntity;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades.TransportistaCamionEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransportistaCamionDao extends CrudRepository<TransportistaCamionEntity, TransportistaCamionEntityPk> {

    List<TransportistaCamionEntity> findAll();

    @Query(" SELECT cli " +
            " FROM TransportistaCamionEntity as cli " +
            " WHERE cli.transportistaCamionPk.transportistaId = :transportista " +
            "and cli.transportistaCamionPk.empresa = 'NPC' ")
    List<TransportistaCamionEntity> obtenerCamionesPorTransportista(@Param("transportista") String transportista);
}
