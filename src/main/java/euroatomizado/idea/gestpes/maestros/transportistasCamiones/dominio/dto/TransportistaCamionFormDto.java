package euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class TransportistaCamionFormDto {

    @NotNull
    @Size(min = 1, max = 4)
    private String empresa;
    @NotNull
    @Size(min = 1, max = 5)
    private String transportistaId;
    @NotNull
    @Size(min = 1, max = 15)
    private String matricula;
    private LocalDateTime fechaModif;

}
