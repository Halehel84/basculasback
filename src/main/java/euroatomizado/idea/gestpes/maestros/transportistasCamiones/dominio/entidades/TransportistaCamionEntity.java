package euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto.TransportistaCamionFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "transportistas_camiones", schema = "dbo")
@Data
public class TransportistaCamionEntity {

    @EmbeddedId
    private TransportistaCamionEntityPk transportistaCamionPk;
    @Column(name = "xfecha_modif")
    private LocalDateTime fechaModif;

    @Transient
    private TransportistaCamionFormDto transportistaCamionFormDto;

}
