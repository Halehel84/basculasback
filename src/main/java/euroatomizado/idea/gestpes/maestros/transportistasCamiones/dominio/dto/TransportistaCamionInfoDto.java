package euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TransportistaCamionInfoDto {

    private String empresa;
    private String transportistaId;
    private String matricula;
    private String descripcion;
    private String id;
    private LocalDateTime fechaModif;

}
