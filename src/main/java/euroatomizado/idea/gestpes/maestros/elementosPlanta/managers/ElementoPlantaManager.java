package euroatomizado.idea.gestpes.maestros.elementosPlanta.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.dto.ElementoPlantaInfoDto;
import euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.entidades.ElementoPlantaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ElementoPlantaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<ElementoPlantaInfoDto> obtenerElementosPlanta() {
        List<ElementoPlantaEntity> lista = daoManager.getElementoPlantaDao().findAll();
        return mapperManager.getMapeadorElementoPlanta().toInfoDto(lista);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<ElementoPlantaInfoDto> obtenerSilos() {
        List<ElementoPlantaEntity> lista = daoManager.getElementoPlantaDao().obtenerSilos(2);
        return mapperManager.getMapeadorElementoPlanta().toInfoDto(lista);
    }

}
