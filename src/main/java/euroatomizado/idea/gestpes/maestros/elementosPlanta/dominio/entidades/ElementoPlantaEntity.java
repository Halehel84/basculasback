package euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name = "elementos_planta", schema = "dbo")
@Data
public class ElementoPlantaEntity {

    @Id
    @Column(name = "epl_codigo")
    private Integer id;
    @Column(name = "epl_empresa")
    private String empresa;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "tep_codigo")
    private Integer tipo;
    @Column(name = "epl_descripcion")
    private String descripcion;

}
