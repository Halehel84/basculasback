package euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.dto;

import lombok.Data;

@Data
public class ElementoPlantaInfoDto {

    private Integer id;
    private String empresa;
    private String seccionId;
    private Integer codigo;
    private String descripcion;

}
