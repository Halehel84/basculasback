package euroatomizado.idea.gestpes.maestros.elementosPlanta.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.dto.ElementoPlantaInfoDto;
import euroatomizado.idea.gestpes.maestros.elementosPlanta.managers.ElementoPlantaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/elementos-planta")
public class ElementoPlantaController {

    @Autowired
    private ElementoPlantaManager manager;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ElementoPlantaInfoDto> obtenerElementosPlanta() {
        return manager.obtenerElementosPlanta();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "silos")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<ElementoPlantaInfoDto> obtenerSilos() {
        return manager.obtenerSilos();
    }

}
