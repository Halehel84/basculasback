package euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.daos;

import euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.entidades.ElementoPlantaEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ElementoPlantaDao extends CrudRepository<ElementoPlantaEntity, Integer> {

    List<ElementoPlantaEntity> findAll();

    @Query(" SELECT ele " +
            " FROM ElementoPlantaEntity as ele " +
            " WHERE ele.id = :id ")
    ElementoPlantaEntity obtenerElementoPlanta(@Param("id") String id);

    @Query(" SELECT ele " +
            " FROM ElementoPlantaEntity as ele " +
            " WHERE ele.tipo = :tipo ")
    List<ElementoPlantaEntity> obtenerSilos(@Param("tipo") Integer tipo);

}
