package euroatomizado.idea.gestpes.maestros.elementosPlanta.mapeadores;

import euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.dto.ElementoPlantaInfoDto;
import euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.entidades.ElementoPlantaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorElementoPlanta {

    ElementoPlantaInfoDto toInfoDto(ElementoPlantaEntity datos);

    List<ElementoPlantaInfoDto> toInfoDto(List<ElementoPlantaEntity> datos);

    void actualizar(ElementoPlantaEntity datosNueva, @MappingTarget ElementoPlantaEntity datos);

}