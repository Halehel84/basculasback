package euroatomizado.idea.gestpes.maestros.barcos.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyIntegerManagerInterface;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoFormDto;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoInfoDto;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades.BarcoEntity;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades.BarcoEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class BarcoManager implements PrimaryKeyIntegerManagerInterface<BarcoInfoDto, BarcoEntity, BarcoEntityPk, BarcoFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<BarcoInfoDto> obtenerBarcos() {
        return obtenerDatosSalida(daoManager.getBarcoDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public BarcoInfoDto guardar(BarcoFormDto datos) {
        BarcoEntity almacen = mapperManager.getMapeadorBarco().toEntidad(datos);
        almacen.setBarcoFormDto(datos);
        try {
            if (!existe(almacen.getBarcoPk()))
                return obtenerDatosSalida(guardarNuevo(almacen));
            return obtenerDatosSalida(modificar(almacen));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public BarcoEntity guardarNuevo(BarcoEntity nuevo) {
        Integer id = nuevo.getBarcoFormDto().getBarcoId();
        nuevo.setBarcoPk(obtenerPrimaryKey(id));
        daoManager.getBarcoDao().save(nuevo);
        return nuevo;
    }

    @Override
    public BarcoEntity modificar(BarcoEntity nuevo) {
        BarcoEntity almacen = obtener(nuevo);
        mapperManager.getMapeadorBarco().actualizar(nuevo, almacen);
        daoManager.getBarcoDao().save(almacen);
        return almacen;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(Integer id) {
        BarcoEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getBarcoDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public BarcoInfoDto obtenerDatosSalida(BarcoEntity almacen) {
        return mapperManager.getMapeadorBarco().toInfoDto(almacen);
    }

    private List<BarcoInfoDto> obtenerDatosSalida(List<BarcoEntity> almacenes) {
        return mapperManager.getMapeadorBarco().toInfoDto(almacenes);
    }

    @Override
    public boolean existe(BarcoEntityPk pk) {
        return pk != null && daoManager.getBarcoDao().exists(pk);
    }

    @Override
    public BarcoEntity obtener(BarcoEntity datos) {
        return daoManager.getBarcoDao().findOne(datos.getBarcoPk());
    }

    @Override
    public BarcoEntityPk obtenerPrimaryKey(Integer id) {
        BarcoEntityPk pk = new BarcoEntityPk();
        pk.setBarcoId(id);
        pk.setEmpresa(EMPRESA);
        return pk;
    }

}

