package euroatomizado.idea.gestpes.maestros.barcos.dominio.daos;

import euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades.BarcoEntity;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades.BarcoEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarcoDao extends CrudRepository<BarcoEntity, BarcoEntityPk> {

    List<BarcoEntity> findAll();

}
