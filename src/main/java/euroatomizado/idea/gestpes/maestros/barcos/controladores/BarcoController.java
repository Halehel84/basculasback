package euroatomizado.idea.gestpes.maestros.barcos.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoFormDto;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoInfoDto;
import euroatomizado.idea.gestpes.maestros.barcos.managers.BarcoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/barcos")
public class BarcoController {

    @Autowired
    private BarcoManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<BarcoInfoDto> obtenerBarcos() {
        return manager.obtenerBarcos();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public BarcoInfoDto crear(@RequestBody @Valid BarcoFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public BarcoInfoDto modificar(@RequestBody @Valid BarcoFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable Integer id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
