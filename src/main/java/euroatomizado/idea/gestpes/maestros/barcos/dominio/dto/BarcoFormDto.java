package euroatomizado.idea.gestpes.maestros.barcos.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data

public class BarcoFormDto {
    @Size(min = 1, max = 4)
    private String empresa;
    @NotNull
    private Integer barcoId;
    @Size(min = 1, max = 250)
    private String descripcion;
    @Size(min = 1, max = 250)
    private String nombre;
    @Size(min = 1, max = 50)
    private String articuloId;
    @Size(min = 1, max = 50)
    private String proveedorId;
}
