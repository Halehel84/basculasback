package euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class BarcoEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresa;
    @Column(name = "xbarco_id")
    private Integer barcoId;

}
