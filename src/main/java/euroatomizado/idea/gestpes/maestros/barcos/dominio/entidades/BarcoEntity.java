package euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoFormDto;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "barcos", schema = "dbo")
@Data
public class BarcoEntity {

    @EmbeddedId
    private BarcoEntityPk barcoPk;
    @Column(name = "xdescripcion")
    private String descripcion;
    @Column(name = "xnombre")
    private String nombre;
    @Column(name = "xfecha_alta")
    private LocalDateTime fechaAlta;
    @Column(name = "xfecha_baja")
    private LocalDateTime fechaBaja;
    @Column(name = "xarticulo_id")
    private String articuloId;
    @Column(name = "xproveedor_id")
    private String proveedorId;

    @Transient
    private BarcoFormDto barcoFormDto;

}