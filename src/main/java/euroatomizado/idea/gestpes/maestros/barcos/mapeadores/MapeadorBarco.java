package euroatomizado.idea.gestpes.maestros.barcos.mapeadores;

import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoFormDto;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.dto.BarcoInfoDto;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.entidades.BarcoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorBarco {

    @Mapping(source = "barcoPk.barcoId", target = "barcoId")
    BarcoInfoDto toInfoDto(BarcoEntity datos);

    List<BarcoInfoDto> toInfoDto(List<BarcoEntity> datos);

    BarcoEntity toEntidad(BarcoFormDto dto);

    void actualizar(BarcoEntity datosNueva, @MappingTarget BarcoEntity datos);

}