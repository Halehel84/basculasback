package euroatomizado.idea.gestpes.maestros.barcos.dominio.dto;

import lombok.Data;

@Data

public class BarcoInfoDto {
    private Integer barcoId;
    private String descripcion;
    private String nombre;
    private String articuloId;
    private String proveedorId;

}
