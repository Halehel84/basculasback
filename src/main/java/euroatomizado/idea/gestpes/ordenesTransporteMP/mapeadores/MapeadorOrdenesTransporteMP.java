package euroatomizado.idea.gestpes.ordenesTransporteMP.mapeadores;

import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto.OrdenesTransporteMPFormDto;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto.OrdenesTransporteMPInfoDto;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.entidades.OrdenesTransporteMPEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorOrdenesTransporteMP {

    OrdenesTransporteMPInfoDto toInfoDto(OrdenesTransporteMPEntity datos);

    List<OrdenesTransporteMPInfoDto> toInfoDto(List<OrdenesTransporteMPEntity> datos);

    OrdenesTransporteMPEntity toEntidad(OrdenesTransporteMPFormDto dto);

    void actualizar(OrdenesTransporteMPEntity datosNueva, @MappingTarget OrdenesTransporteMPEntity datos);

}