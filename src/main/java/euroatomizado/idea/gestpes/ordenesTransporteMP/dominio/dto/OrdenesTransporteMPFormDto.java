package euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class OrdenesTransporteMPFormDto {

    @NotNull
    private Integer otmpCodigo;
    @NotNull
    @Size(min = 1, max = 4)
    private String otmpEmpresa;
    @NotNull
    @Size(min = 1, max = 2)
    private String seccionId;
    @NotNull
    private Integer totmpCodigo;
    private LocalDateTime otmpFecha;
    @Size(min = 1, max = 500)
    private String otmpNumeroCarga;
    @Size(min = 1, max = 2)
    private String otmpEmpresaOrigen;
    private Integer almCodigoOrigen;
    @Size(min = 1, max = 20)
    private String articuloId;
    private Integer ubiCodigoOrigen;
    @Size(min = 1, max = 20)
    private String otmpProveedorOrigen;
    @Size(min = 1, max = 15)
    private String otmpDireccionProveedorOrigen;
    private BigDecimal otmpCantidad;
    private Integer otmpNumeroViajes;
    @Size(min = 1, max = 2)
    private String otmpEmpresaDestino;
    private Integer almCodigoDestino;
    private Integer ubiCodigoDestino;
    @Size(min = 1, max = 20)
    private String proveedorId;
    private Integer estCodigo;
    @Size(min = 1, max = 50)
    private String otmpLote;
    private LocalDateTime otmpFechaFin;

}
