package euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.entidades;

import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto.OrdenesTransporteMPFormDto;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "ordenes_transporte_mp", schema = "dbo")
@Data
public class OrdenesTransporteMPEntity {

    @Id
    @Column(name = "otmp_codigo")
    private Integer otmpCodigo;
    @Column(name = "otmp_empresa")
    private String otmpEmpresa;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "totmp_codigo")
    private Integer totmpCodigo;
    @Column(name = "otmp_fecha")
    private LocalDateTime otmpFecha;
    @Column(name = "otmp_numero_carga")
    private String otmpNumeroCarga;
    @Column(name = "otmp_empresa_origen")
    private String otmpEmpresaOrigen;
    @Column(name = "alm_codigo_origen")
    private Integer almCodigoOrigen;
    @Column(name = "xarticulo_id")
    private String articuloId;
    @Column(name = "ubi_codigo_origen")
    private Integer ubiCodigoOrigen;
    @Column(name = "otmp_proveedor_origen")
    private String otmpProveedorOrigen;
    @Column(name = "otmp_direccion_proveedor_origen")
    private String otmpDireccionProveedorOrigen;
    @Column(name = "otmp_cantidad", columnDefinition = "decimal")
    private BigDecimal otmpCantidad;
    @Column(name = "otmp_numero_viajes")
    private Integer otmpNumeroViajes;
    @Column(name = "otmp_empresa_destino")
    private String otmpEmpresaDestino;
    @Column(name = "alm_codigo_destino")
    private Integer almCodigoDestino;
    @Column(name = "ubi_codigo_destino")
    private Integer ubiCodigoDestino;
    @Column(name = "xproveedor_id")
    private String proveedorId;
    @Column(name = "est_codigo")
    private Integer estCodigo;
    @Column(name = "otmp_lote")
    private String otmpLote;
    @Column(name = "otmp_fecha_fin")
    private LocalDateTime otmpFechaFin;

    @Transient
    private OrdenesTransporteMPFormDto ordenesTransporteMPFormDto;

}