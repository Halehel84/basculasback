package euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class OrdenesTransporteMPInfoDto {

    private Integer otmpCodigo;
    private String otmpEmpresa;
    private String seccionId;
    private Integer totmpCodigo;
    private LocalDateTime otmpFecha;
    private String otmpNumeroCarga;
    private String otmpEmpresaOrigen;
    private Integer almCodigoOrigen;
    private String articuloId;
    private Integer ubiCodigoOrigen;
    private String otmpProveedorOrigen;
    private String otmpDireccionProveedorOrigen;
    private BigDecimal otmpCantidad;
    private Integer otmpNumeroViajes;
    private String otmpEmpresaDestino;
    private Integer almCodigoDestino;
    private Integer ubiCodigoDestino;
    private String proveedorId;
    private Integer estCodigo;
    private String otmpLote;
    private LocalDateTime otmpFechaFin;

}
