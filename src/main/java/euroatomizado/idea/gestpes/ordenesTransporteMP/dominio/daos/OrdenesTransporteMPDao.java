package euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.daos;

import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.entidades.OrdenesTransporteMPEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdenesTransporteMPDao extends CrudRepository<OrdenesTransporteMPEntity, Integer> {

    List<OrdenesTransporteMPEntity> findAll();

}
