package euroatomizado.idea.gestpes.ordenesTransporteMP.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto.OrdenesTransporteMPFormDto;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto.OrdenesTransporteMPInfoDto;
import euroatomizado.idea.gestpes.ordenesTransporteMP.managers.OrdenesTransporteMPManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/ordenes-transporte-mp")
public class OrdenesTransporteMPController {

    @Autowired
    private OrdenesTransporteMPManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<OrdenesTransporteMPInfoDto> obtenerOrdenes() {
        return manager.obtenerOrdenes();
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public OrdenesTransporteMPInfoDto crear(@RequestBody @Valid OrdenesTransporteMPFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public OrdenesTransporteMPInfoDto modificar(@RequestBody @Valid OrdenesTransporteMPFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable Integer id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
