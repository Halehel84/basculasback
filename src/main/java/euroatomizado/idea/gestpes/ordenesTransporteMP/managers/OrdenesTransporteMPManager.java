package euroatomizado.idea.gestpes.ordenesTransporteMP.managers;

import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto.OrdenesTransporteMPFormDto;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.dto.OrdenesTransporteMPInfoDto;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.entidades.OrdenesTransporteMPEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class OrdenesTransporteMPManager  {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<OrdenesTransporteMPInfoDto> obtenerOrdenes() {
        return obtenerDatosSalida(daoManager.getOrdenesTransporteMPDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public OrdenesTransporteMPInfoDto guardar(OrdenesTransporteMPFormDto datos) {
        OrdenesTransporteMPEntity entity = mapperManager.getMapeadorOrdenesTransporteMP().toEntidad(datos);
        entity.setOrdenesTransporteMPFormDto(datos);
        try {
            if (!existe(entity.getOtmpCodigo()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    public OrdenesTransporteMPEntity guardarNuevo(OrdenesTransporteMPEntity nuevo) {
        Integer id = nuevo.getOrdenesTransporteMPFormDto().getOtmpCodigo();
        nuevo.setOtmpCodigo(id);
        nuevo.setOtmpEmpresa(EMPRESA);
        daoManager.getOrdenesTransporteMPDao().save(nuevo);
        return nuevo;
    }

    public OrdenesTransporteMPEntity modificar(OrdenesTransporteMPEntity nuevo) {
        OrdenesTransporteMPEntity entity = obtener(nuevo);
        mapperManager.getMapeadorOrdenesTransporteMP().actualizar(nuevo, entity);
        daoManager.getOrdenesTransporteMPDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(Integer id) {
        if (existe(id))
            daoManager.getOrdenesTransporteMPDao().delete(id);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    public OrdenesTransporteMPInfoDto obtenerDatosSalida(OrdenesTransporteMPEntity entity) {
        return mapperManager.getMapeadorOrdenesTransporteMP().toInfoDto(entity);
    }

    private List<OrdenesTransporteMPInfoDto> obtenerDatosSalida(List<OrdenesTransporteMPEntity> entidades) {
        return mapperManager.getMapeadorOrdenesTransporteMP().toInfoDto(entidades);
    }

    public boolean existe(Integer id) {
        return id != null && daoManager.getOrdenesTransporteMPDao().exists(id);
    }

    public OrdenesTransporteMPEntity obtener(OrdenesTransporteMPEntity datos) {
        return daoManager.getOrdenesTransporteMPDao().findOne(datos.getOtmpCodigo());
    }

}

