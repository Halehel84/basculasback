package euroatomizado.idea.gestpes._configuracion.excepciones;

import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import lombok.Data;

// Lanzada cuando una petición contiene datos incorrectos (bien directamente o bien por referencia)
@Data
public class BadRequestException extends RuntimeException {
    private InfoExceptionEnum infoError;

    public BadRequestException(InfoExceptionEnum infoError) {
        this.infoError = infoError;
    }
}
