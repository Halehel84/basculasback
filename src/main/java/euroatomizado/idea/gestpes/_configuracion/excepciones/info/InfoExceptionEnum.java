package euroatomizado.idea.gestpes._configuracion.excepciones.info;

public enum InfoExceptionEnum {

    // GENÉRICOS
    ERROR_SERVIDOR(001, "ERROR_SERVIDOR", "Error interno en el servidor"),
    METODO_NO_SOPORTADO(002, "METODO_NO_SOPORTADO", "El método no está soportado por el servidor"),
    PARAM_NO_ENCONTRADO(003, "PARAM_NO_ENCONTRADO", "Falta algún query param obligatorio"),
    JSON_INVALIDO(004, "JSON_INVALIDO", "El JSON en inválido"),
    DATOS_INVALIDOS(005, "DATOS_INVALIDOS", "Uno o más campos básicos son inválidos"),
    REGISTRO_NO_ENCONTRADO(006, "NO_ENCONTRADO", "Datos no encontrados"),
    PERMISOS_INSUFICIENTES(006, "PERMISOS_INSUFICIENTES", "Permisos insuficientes"),

    LEG_NO_ENCONTRADO(007, "NO_ENCONTRADO", "Datos no encontrados"),
    LER_NUMERO_REQUISITO(007, "EXISTE_REQUISITO", "El número de requisito ya existe");

    public final Integer errorId;
    public final String errorTag;
    public final String errorInfo;

    InfoExceptionEnum(Integer errorId, String errorTag, String errorInfo) {
        this.errorId = errorId;
        this.errorTag = errorTag;
        this.errorInfo = errorInfo;
    }
}
