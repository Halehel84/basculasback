package euroatomizado.idea.gestpes._configuracion.excepciones;

import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import lombok.Data;

// Lanzada cuando el elemento principal sobre el que se quiere actuar no existe
@Data
public class ErrorException extends RuntimeException {
    private InfoExceptionEnum infoError;

    public ErrorException(InfoExceptionEnum infoError) {
        this.infoError = infoError;
    }
}
