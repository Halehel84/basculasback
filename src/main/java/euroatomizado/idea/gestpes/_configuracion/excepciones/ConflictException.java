package euroatomizado.idea.gestpes._configuracion.excepciones;

import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import lombok.Data;

// Lanzada cuando se quiere realizar una acción que no esta permitida por una regla de negocio
@Data
public class ConflictException extends RuntimeException {
    private InfoExceptionEnum infoError;

    public ConflictException(InfoExceptionEnum infoError) {
        this.infoError = infoError;
    }
}
