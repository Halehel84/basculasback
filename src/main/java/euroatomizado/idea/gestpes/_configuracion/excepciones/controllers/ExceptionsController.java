package euroatomizado.idea.gestpes._configuracion.excepciones.controllers;

import euroatomizado.idea.gestpes._configuracion.excepciones.BadRequestException;
import euroatomizado.idea.gestpes._configuracion.excepciones.ConflictException;
import euroatomizado.idea.gestpes._configuracion.excepciones.ForbiddenException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionDto;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.nio.file.AccessDeniedException;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class ExceptionsController {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionsController.class);

    @ResponseStatus(NOT_FOUND)
    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    public InfoExceptionDto notFound(NotFoundException ex) {
        return new InfoExceptionDto(ex.getInfoError());
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(BadRequestException.class)
    public InfoExceptionDto badRequest(BadRequestException ex) {
        return new InfoExceptionDto(ex.getInfoError());
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public InfoExceptionDto badRequestParameter(Exception ex) {
        return new InfoExceptionDto(InfoExceptionEnum.PARAM_NO_ENCONTRADO);
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public InfoExceptionDto badRequestJson(Exception ex) {
        ex.printStackTrace();
        return new InfoExceptionDto(InfoExceptionEnum.JSON_INVALIDO);
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public InfoExceptionDto badRequestInvalid(Exception ex) {
        return new InfoExceptionDto(InfoExceptionEnum.DATOS_INVALIDOS);
    }

    @ResponseStatus(BAD_REQUEST)/*(FORBIDDEN)*/ // Por motivos de seguridad
    @ResponseBody
    @ExceptionHandler(ForbiddenException.class)
    public InfoExceptionDto forbbiden(ForbiddenException ex) {
        return new InfoExceptionDto(ex.getInfoError());
    }

    @ResponseStatus(FORBIDDEN)
    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
    public InfoExceptionDto forbbidenSecurity(Exception ex) {
        ex.printStackTrace();
        return new InfoExceptionDto(InfoExceptionEnum.PERMISOS_INSUFICIENTES);
    }

    @ResponseStatus(CONFLICT)
    @ResponseBody
    @ExceptionHandler(ConflictException.class)
    public InfoExceptionDto conflict(ConflictException ex) {
        return new InfoExceptionDto(ex.getInfoError());
    }

    @ResponseStatus(METHOD_NOT_ALLOWED)
    @ResponseBody
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public InfoExceptionDto methodNotAllowed(Exception ex) {
        return new InfoExceptionDto(InfoExceptionEnum.METODO_NO_SOPORTADO);
    }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public InfoExceptionDto internalServerError(Exception ex) {
        logger.error("Error inesperado en el servidor: ", ex);
        return new InfoExceptionDto(InfoExceptionEnum.ERROR_SERVIDOR);
    }
}
