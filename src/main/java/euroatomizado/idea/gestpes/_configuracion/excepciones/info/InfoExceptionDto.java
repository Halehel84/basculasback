package euroatomizado.idea.gestpes._configuracion.excepciones.info;

import lombok.Data;

@Data
public class InfoExceptionDto {

    private Integer errorId;
    private String errorTag;
    private String errorInfo;

    public InfoExceptionDto(InfoExceptionEnum info) {
        this.errorId = info.errorId;
        this.errorTag = info.errorTag;
        this.errorInfo = info.errorInfo;
    }
}
