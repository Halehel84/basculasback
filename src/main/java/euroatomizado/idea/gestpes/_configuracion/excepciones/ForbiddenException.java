package euroatomizado.idea.gestpes._configuracion.excepciones;

import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import lombok.Data;

// Lanzada cuando el acceso al elemento principal sobre el que se quiere actuar requiere permisos que no se tienen
@Data
public class ForbiddenException extends RuntimeException {
    private InfoExceptionEnum infoError;

    public ForbiddenException(InfoExceptionEnum infoError) {
        this.infoError = infoError;
    }
}
