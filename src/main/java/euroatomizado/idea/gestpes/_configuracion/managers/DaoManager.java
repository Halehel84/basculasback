package euroatomizado.idea.gestpes._configuracion.managers;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.daos.InstruccionProductivaDao;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.daos.InstruccionProductivaHumedadDao;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.daos.InstruccionProductivaLineaDao;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.daos.ConsultaExpedicionTicketDao;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.daos.TicketExpedicionDao;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.daos.TicketExpedicionHumedadDao;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.daos.TicketExpedicionSilosDao;
import euroatomizado.idea.gestpes.maestros.almacenes.dominio.daos.AlmacenesDao;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.daos.ArticuloDao;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.dominio.daos.ArticuloPruebaDao;
import euroatomizado.idea.gestpes.maestros.articulosPt.dominio.daos.ArticuloPtDao;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.dominio.daos.ArticuloPtRelacionDao;
import euroatomizado.idea.gestpes.maestros.barcos.dominio.daos.BarcoDao;
import euroatomizado.idea.gestpes.maestros.camiones.dominio.daos.CamionDao;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.dominio.daos.CamionRemolqueDao;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.daos.ClienteDao;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.dominio.daos.ClienteArticuloDao;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.dominio.daos.ClienteEnvioDao;
import euroatomizado.idea.gestpes.maestros.clientesLimites.dominio.daos.ClienteLimiteDao;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.dominio.daos.ClienteTransportistaDao;
import euroatomizado.idea.gestpes.maestros.conductores.dominio.daos.ConductorDao;
import euroatomizado.idea.gestpes.maestros.elementosPlanta.dominio.daos.ElementoPlantaDao;
import euroatomizado.idea.gestpes.maestros.gestpesContratos.dominio.daos.GestpesContratosDao;
import euroatomizado.idea.gestpes.maestros.paises.dominio.daos.PaisDao;
import euroatomizado.idea.gestpes.maestros.personal.dominio.daos.PersonalDao;
import euroatomizado.idea.gestpes.maestros.proveedores.dominio.daos.ProveedorDao;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.dominio.daos.ProveedorArticuloDao;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.dominio.daos.ProveedorEnvioDao;
import euroatomizado.idea.gestpes.maestros.provincias.dominio.daos.ProvinciaDao;
import euroatomizado.idea.gestpes.maestros.secciones.dominio.daos.SeccionDao;
import euroatomizado.idea.gestpes.maestros.transportistas.dominio.daos.TransportistaDao;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.dominio.daos.TransportistaCamionDao;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.dominio.daos.TransportistaConductorDao;
import euroatomizado.idea.gestpes.maestros.ubicaciones.dominio.daos.UbicacionDao;
import euroatomizado.idea.gestpes.ordenesTransporteMP.dominio.daos.OrdenesTransporteMPDao;
import euroatomizado.idea.gestpes.sacas.dominio.daos.ConsultaSacaDao;
import euroatomizado.idea.gestpes.sacas.dominio.daos.SacaDao;
import euroatomizado.idea.gestpes.sistema.auditoria.dominio.daos.AuditoriaDao;
import euroatomizado.idea.gestpes.sistema.estados.dominio.daos.EstadoDao;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.dominio.daos.SincronizacionDao;
import euroatomizado.idea.gestpes.sistema.usuarios.dominio.daos.UsuarioDao;
import euroatomizado.idea.gestpes.sistema.variablesConf.dominio.daos.VariablesConfDao;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.dominio.daos.AlbaranEstadoDao;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.daos.ContadorDao;
import euroatomizado.idea.gestpes.ticket.historicoTickets.dominio.daos.HistoricoTicketDao;
import euroatomizado.idea.gestpes.ticket.incrementos.dominio.daos.IncrementoDao;
import euroatomizado.idea.gestpes.ticket.lineasTicket.dominio.daos.LineaTicketDao;
import euroatomizado.idea.gestpes.ticket.parrillaSemanal.dominio.dao.ParrillaSemanalDao;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.daos.ConsultaHistoricoTicketDao;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.daos.TicketDao;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data
@Component

public class DaoManager {

    @Autowired
    private AlmacenesDao almacenesDao;
    @Autowired
    private BarcoDao barcoDao;
    @Autowired
    private ArticuloDao articuloDao;
    @Autowired
    private ArticuloPruebaDao articuloPruebaDao;
    @Autowired
    private ArticuloPtDao articuloPtDao;
    @Autowired
    private ArticuloPtRelacionDao articuloPtRelacionDao;
    @Autowired
    private CamionDao camionDao;
    @Autowired
    private CamionRemolqueDao camionRemolqueDao;
    @Autowired
    private ClienteDao clienteDao;
    @Autowired
    private ClienteArticuloDao clienteArticuloDao;
    @Autowired
    private ClienteEnvioDao clienteEnvioDao;
    @Autowired
    private ClienteLimiteDao clienteLimiteDao;
    @Autowired
    private ClienteTransportistaDao clienteTransportistaDao;
    @Autowired
    private ConductorDao conductorDao;
    @Autowired
    private ElementoPlantaDao elementoPlantaDao;
    @Autowired
    private GestpesContratosDao gestpesContratosDao;
    @Autowired
    private PaisDao paisDao;
    @Autowired
    private PersonalDao personalDao;
    @Autowired
    private ProveedorDao proveedorDao;
    @Autowired
    private ProveedorArticuloDao proveedorArticuloDao;
    @Autowired
    private ProveedorEnvioDao proveedorEnvioDao;
    @Autowired
    private ProvinciaDao provinciaDao;
    @Autowired
    private SeccionDao seccionDao;
    @Autowired
    private UbicacionDao ubicacionDao;
    @Autowired
    private TransportistaDao transportistaDao;
    @Autowired
    private TransportistaCamionDao transportistaCamionDao;
    @Autowired
    private TransportistaConductorDao transportistaConductorDao;
    @Autowired
    private OrdenesTransporteMPDao ordenesTransporteMPDao;
    @Autowired
    private SacaDao sacaDao;
    @Autowired
    private ConsultaSacaDao consultaSacaDao;
    @Autowired
    private EstadoDao estadoDao;
    @Autowired
    private AuditoriaDao auditoriaDao;
    @Autowired
    private SincronizacionDao sincronizacionDao;
    @Autowired
    private UsuarioDao usuarioDao;
    @Autowired
    private VariablesConfDao variablesConfDao;
    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private HistoricoTicketDao historicoTicketDao;
    @Autowired
    private AlbaranEstadoDao albaranEstadoDao;
    @Autowired
    private ContadorDao contadorDao;
    @Autowired
    private IncrementoDao incrementoDao;
    @Autowired
    private LineaTicketDao lineaTicketDao;
    @Autowired
    private InstruccionProductivaDao instruccionProductivaDao;
    @Autowired
    private InstruccionProductivaHumedadDao instruccionProductivaHumedadDao;
    @Autowired
    private InstruccionProductivaLineaDao instruccionProductivaLineaDao;
    @Autowired
    private TicketExpedicionDao ticketExpedicionDao;
    @Autowired
    private TicketExpedicionHumedadDao ticketExpedicionHumedadDao;
    @Autowired
    private TicketExpedicionSilosDao ticketExpedicionSilosDao;
    @Autowired
    private ParrillaSemanalDao parrillaSemanalDao;
    @Autowired
    private ConsultaHistoricoTicketDao consultaHistoricoTicketDao;
    @Autowired
    private ConsultaExpedicionTicketDao consultaExpedicionTicketDao;

}
