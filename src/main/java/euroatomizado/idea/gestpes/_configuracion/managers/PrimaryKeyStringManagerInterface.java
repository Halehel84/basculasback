package euroatomizado.idea.gestpes._configuracion.managers;

/**
 * Interfaz de acciones básicas para los managers
 *
 * @param <T> InfoDto
 * @param <U> Entity
 * @param <E> EntityPrimaryKey
 * @param <V> FormDto
 */
public interface PrimaryKeyStringManagerInterface<T, U, E, V> {
    T guardar(V datos);

    U guardarNuevo(U nuevo);

    U modificar(U elemento);

    T obtenerDatosSalida(U elemento);

    boolean existe(E pk);

    U obtener(U elemento);

    E obtenerPrimaryKey(String id);

    void borrar(String id);
}
