package euroatomizado.idea.gestpes._configuracion.managers;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.mapeadores.MapeadorInstruccionProductiva;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.mapeadores.MapeadorInstruccionProductivaHumedad;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.mapeadores.MapeadorInstruccionProductivaLinea;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.mapeadores.MapeadorTicketExpedicion;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.mapeadores.MapeadorTicketExpedicionHumedad;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.mapeadores.MapeadorTicketExpedicionSilos;
import euroatomizado.idea.gestpes.maestros.almacenes.mapeadores.MapeadorAlmacen;
import euroatomizado.idea.gestpes.maestros.articulos.mapeadores.MapeadorArticulo;
import euroatomizado.idea.gestpes.maestros.articulosPruebas.mapeadores.MapeadorArticuloPrueba;
import euroatomizado.idea.gestpes.maestros.articulosPt.mapeadores.MapeadorArticuloPt;
import euroatomizado.idea.gestpes.maestros.articulosPtRelaciones.mapeadores.MapeadorArticuloPtRelacion;
import euroatomizado.idea.gestpes.maestros.barcos.mapeadores.MapeadorBarco;
import euroatomizado.idea.gestpes.maestros.camiones.mapeadores.MapeadorCamion;
import euroatomizado.idea.gestpes.maestros.camionesRemolques.mapeadores.MapeadorCamionRemolque;
import euroatomizado.idea.gestpes.maestros.clientes.mapeadores.MapeadorCliente;
import euroatomizado.idea.gestpes.maestros.clientesArticulos.mapeadores.MapeadorClienteArticulo;
import euroatomizado.idea.gestpes.maestros.clientesEnvios.mapeadores.MapeadorClienteEnvio;
import euroatomizado.idea.gestpes.maestros.clientesLimites.mapeadores.MapeadorClienteLimite;
import euroatomizado.idea.gestpes.maestros.clientesTransportistas.mapeadores.MapeadorClienteTransportista;
import euroatomizado.idea.gestpes.maestros.conductores.mapeadores.MapeadorConductor;
import euroatomizado.idea.gestpes.maestros.elementosPlanta.mapeadores.MapeadorElementoPlanta;
import euroatomizado.idea.gestpes.maestros.gestpesContratos.mapeadores.MapeadorGestpesContratos;
import euroatomizado.idea.gestpes.maestros.paises.mapeadores.MapeadorPais;
import euroatomizado.idea.gestpes.maestros.personal.mapeadores.MapeadorPersonal;
import euroatomizado.idea.gestpes.maestros.proveedores.mapeadores.MapeadorProveedor;
import euroatomizado.idea.gestpes.maestros.proveedoresArticulos.mapeadores.MapeadorProveedorArticulo;
import euroatomizado.idea.gestpes.maestros.proveedoresEnvio.mapeadores.MapeadorProveedorEnvio;
import euroatomizado.idea.gestpes.maestros.provincias.mapeadores.MapeadorProvincia;
import euroatomizado.idea.gestpes.maestros.secciones.mapeadores.MapeadorSeccion;
import euroatomizado.idea.gestpes.maestros.transportistas.mapeadores.MapeadorTransportista;
import euroatomizado.idea.gestpes.maestros.transportistasCamiones.mapeadores.MapeadorTransportistaCamion;
import euroatomizado.idea.gestpes.maestros.transportistasConductores.mapeadores.MapeadorTransportistaConductor;
import euroatomizado.idea.gestpes.maestros.ubicaciones.mapeadores.MapeadorUbicacion;
import euroatomizado.idea.gestpes.ordenesTransporteMP.mapeadores.MapeadorOrdenesTransporteMP;
import euroatomizado.idea.gestpes.sacas.mapeadores.MapeadorSaca;
import euroatomizado.idea.gestpes.sistema.auditoria.mapeadores.MapeadorAudtoria;
import euroatomizado.idea.gestpes.sistema.estados.mapeadores.MapeadorEstado;
import euroatomizado.idea.gestpes.sistema.sincronizaciones.mapeadores.MapeadorSincronizacion;
import euroatomizado.idea.gestpes.sistema.usuarios.mapeadores.MapeadorUsuario;
import euroatomizado.idea.gestpes.sistema.variablesConf.mapeadores.MapeadorVariablesConf;
import euroatomizado.idea.gestpes.ticket.albaranesEstados.mapeadores.MapeadorAlbaranEstado;
import euroatomizado.idea.gestpes.sistema.contadores.mapeadores.MapeadorContador;
import euroatomizado.idea.gestpes.ticket.historicoTickets.mapeadores.MapeadorHistoricoTicket;
import euroatomizado.idea.gestpes.ticket.incrementos.mapeadores.MapeadorIncremento;
import euroatomizado.idea.gestpes.ticket.lineasTicket.mapeadores.MapeadorLineaTicket;
import euroatomizado.idea.gestpes.ticket.tickets.mapeadores.MapeadorTicket;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data
@Component

public class MapperManager {

    @Autowired
    private MapeadorAlmacen mapeadorAlmacenes;
    @Autowired
    private MapeadorBarco mapeadorBarco;
    @Autowired
    private MapeadorArticulo mapeadorArticulo;
    @Autowired
    private MapeadorArticuloPrueba mapeadorArticuloPrueba;
    @Autowired
    private MapeadorArticuloPt mapeadorArticuloPt;
    @Autowired
    private MapeadorArticuloPtRelacion mapeadorArticuloPtRelacion;
    @Autowired
    private MapeadorCamion mapeadorCamion;
    @Autowired
    private MapeadorCamionRemolque mapeadorCamionRemolque;
    @Autowired
    private MapeadorCliente mapeadorCliente;
    @Autowired
    private MapeadorClienteArticulo mapeadorClienteArticulo;
    @Autowired
    private MapeadorClienteEnvio mapeadorClienteEnvio;
    @Autowired
    private MapeadorClienteLimite mapeadorClienteLimite;
    @Autowired
    private MapeadorClienteTransportista mapeadorClienteTransportista;
    @Autowired
    private MapeadorConductor mapeadorConductor;
    @Autowired
    private MapeadorElementoPlanta mapeadorElementoPlanta;
    @Autowired
    private MapeadorGestpesContratos mapeadorGestpesContratos;
    @Autowired
    private MapeadorPais mapeadorPais;
    @Autowired
    private MapeadorPersonal mapeadorPersonal;
    @Autowired
    private MapeadorProveedor mapeadorProveedor;
    @Autowired
    private MapeadorProveedorArticulo mapeadorProveedorArticulo;
    @Autowired
    private MapeadorProveedorEnvio mapeadorProveedorEnvio;
    @Autowired
    private MapeadorProvincia mapeadorProvincia;
    @Autowired
    private MapeadorSeccion mapeadorSeccion;
    @Autowired
    private MapeadorUbicacion mapeadorUbicacion;
    @Autowired
    private MapeadorTransportista mapeadorTransportista;
    @Autowired
    private MapeadorTransportistaCamion mapeadorTransportistaCamion;
    @Autowired
    private MapeadorTransportistaConductor mapeadorTransportistaConductor;
    @Autowired
    private MapeadorOrdenesTransporteMP mapeadorOrdenesTransporteMP;
    @Autowired
    private MapeadorSaca mapeadorSaca;
    @Autowired
    private MapeadorEstado mapeadorEstado;
    @Autowired
    private MapeadorAudtoria mapeadorAudtoria;
    @Autowired
    private MapeadorSincronizacion mapeadorSincronizacion;
    @Autowired
    private MapeadorUsuario mapeadorUsuario;
    @Autowired
    private MapeadorVariablesConf mapeadorVariablesConf;
    @Autowired
    private MapeadorTicket mapeadorTicket;
    @Autowired
    private MapeadorHistoricoTicket mapeadorHistoricoTicket;
    @Autowired
    private MapeadorAlbaranEstado mapeadorAlbaranEstado;
    @Autowired
    private MapeadorContador mapeadorContador;
    @Autowired
    private MapeadorIncremento mapeadorIncremento;
    @Autowired
    private MapeadorLineaTicket mapeadorLineaTicket;
    @Autowired
    private MapeadorInstruccionProductiva mapeadorInstruccionProductiva;
    @Autowired
    private MapeadorInstruccionProductivaHumedad mapeadorInstruccionProductivaHumedad;
    @Autowired
    private MapeadorInstruccionProductivaLinea mapeadorInstruccionProductivaLinea;
    @Autowired
    private MapeadorTicketExpedicion mapeadorTicketExpedicion;
    @Autowired
    private MapeadorTicketExpedicionHumedad mapeadorTicketExpedicionHumedad;
    @Autowired
    private MapeadorTicketExpedicionSilos mapeadorTicketExpedicionSilos;

}
