package euroatomizado.idea.gestpes._configuracion.configs.dataSources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = {"euroatomizado.idea.gestpes"})
public class DSConfig {

    @Autowired
    private Environment env;

    @Autowired
    private DataSourceService dataSourceService;

//    @Primary
//    @Bean(name = "gestpesEuroDataSource")
//    public DataSource dataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(env.getProperty("gestpesEuroConfig.jdbc.sql.driverClassName"));
//        dataSource.setUrl(env.getProperty("gestpesEuroConfig.jdbc.url"));
//        dataSource.setUsername(env.getProperty("gestpesEuroConfig.jdbc.user"));
//        dataSource.setPassword(env.getProperty("gestpesEuroConfig.jdbc.pass"));
//        return dataSource;
//    }

    @Bean
    public DataSource dataSource() {
        CustomRoutingDataSource customDataSource = new CustomRoutingDataSource();
        customDataSource.setTargetDataSources(dataSourceService.getDataSourceHashMap());
        return customDataSource;
    }

//    @Primary
//    @Bean(name = "gestpesEuroEntityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean barEntityManagerFactory(
//            EntityManagerFactoryBuilder builder, @Qualifier("dataSource") DataSource dataSource) {
//
//        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//        em.setDataSource(dataSource);
//        em.setPackagesToScan(new String[]{
//                "euroatomizado.idea.gestpes._configuracion",
//                "euroatomizado.idea.gestpes.maestros",
//                "euroatomizado.idea.gestpes.ordenesTransporteMP",
//                "euroatomizado.idea.gestpes.expedicion",
//                "euroatomizado.idea.gestpes.ticket",
//                "euroatomizado.idea.gestpes.sacas",
//                "euroatomizado.idea.gestpes.sistema",
//        });
////        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
////        em.setJpaVendorAdapter(vendorAdapter);
////        HashMap<String, Object> properties = new HashMap<String, Object>();
////        properties.put("hibernate.hbm2ddl.auto", env.getProperty("gestpesEuroConfig.hibernate.hbm2ddl.auto"));
////        properties.put("hibernate.dialect", env.getProperty("gestpesEuroConfig.hibernate.dialect"));
////        properties.put("hibernate.default_catalog", env.getProperty("gestpesEuroConfig.jdbc.catalog"));
////        properties.put("default_schema", env.getProperty("gestpesEuroConfig.jdbc.schema"));
////        em.setJpaPropertyMap(properties);
////        em.setPersistenceUnitName("gestPesEuroDs");
//
//        return em;
//    }

//    @Primary
//    @Bean(name = "gestpesEuroTransactionManager")
//    public PlatformTransactionManager barTransactionManager(
//            @Qualifier("gestpesEuroEntityManagerFactory") EntityManagerFactory barEntityManagerFactory) {
//        return new JpaTransactionManager(barEntityManagerFactory);
//    }

}
