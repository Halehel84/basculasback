package euroatomizado.idea.gestpes._configuracion.configs.dataSources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class CustomRoutingDataSource extends AbstractRoutingDataSource {

    @Autowired
    private EmpresaService empresaService;

    @Override
    protected Object determineCurrentLookupKey() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();     // get request object
        if (attr != null) {
            String tenantId = attr.getRequest().getParameter("empresa");       // find parameter from request
            if (tenantId == null) {
                empresaService.setEmpresa("1");
                return "1";
            }
            empresaService.setEmpresa(tenantId);
            return tenantId;
        } else {
            empresaService.setEmpresa("1");
            return "1";             // default data source
        }
    }
}
