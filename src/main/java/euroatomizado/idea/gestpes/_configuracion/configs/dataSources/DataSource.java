package euroatomizado.idea.gestpes._configuracion.configs.dataSources;

public enum DataSource {
    ONDA("1"),
    NPC("0");

    private String name;

    DataSource(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
