package euroatomizado.idea.gestpes._configuracion.configs.dataSources;

import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    public String empresa;

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String obtenerCodigoEmpresa() {
        if (this.empresa != null && this.empresa.equals("npc")) {
            return "0";
        } else if (this.empresa != null && this.empresa.equals("euro")) {
            return "1";
        } else {
            return "1";
        }
    }
}
