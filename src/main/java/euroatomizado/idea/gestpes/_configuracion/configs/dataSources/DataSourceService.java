package euroatomizado.idea.gestpes._configuracion.configs.dataSources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DataSourceService {
    @Autowired
    private Environment env;

    public Map<Object, Object> getDataSourceHashMap() {
        DriverManagerDataSource dataSourceEuro = new DriverManagerDataSource();
        dataSourceEuro.setDriverClassName(env.getProperty("gestpesEuroConfig.jdbc.sql.driverClassName"));
        dataSourceEuro.setUrl(env.getProperty("gestpesEuroConfig.jdbc.url"));
        dataSourceEuro.setUsername(env.getProperty("gestpesEuroConfig.jdbc.user"));
        dataSourceEuro.setPassword(env.getProperty("gestpesEuroConfig.jdbc.pass"));
        dataSourceEuro.setCatalog(env.getProperty("gestpesEuroConfig.jdbc.catalog"));
        dataSourceEuro.setSchema(env.getProperty("gestpesEuroConfig.jdbc.schema"));

        DriverManagerDataSource dataSourceNPC = new DriverManagerDataSource();
        dataSourceNPC.setDriverClassName(env.getProperty("gestpesNPCConfig.jdbc.sql.driverClassName"));
        dataSourceNPC.setUrl(env.getProperty("gestpesNPCConfig.jdbc.url"));
        dataSourceNPC.setUsername(env.getProperty("gestpesNPCConfig.jdbc.user"));
        dataSourceNPC.setPassword(env.getProperty("gestpesNPCConfig.jdbc.pass"));
        dataSourceNPC.setCatalog(env.getProperty("gestpesNPCConfig.jdbc.catalog"));
        dataSourceNPC.setSchema(env.getProperty("gestpesNPCConfig.jdbc.schema"));

        HashMap hashMap = new HashMap();
        hashMap.put(DataSource.ONDA.getName(), dataSourceEuro);
        hashMap.put(DataSource.NPC.getName(), dataSourceNPC);
        return hashMap;
    }
    //        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(env.getProperty("gestpesEuroConfig.jdbc.sql.driverClassName"));
//        dataSource.setUrl(env.getProperty("gestpesEuroConfig.jdbc.url"));
//        dataSource.setUsername(env.getProperty("gestpesEuroConfig.jdbc.user"));
//        dataSource.setPassword(env.getProperty("gestpesEuroConfig.jdbc.pass"));
//        return dataSource;
}
