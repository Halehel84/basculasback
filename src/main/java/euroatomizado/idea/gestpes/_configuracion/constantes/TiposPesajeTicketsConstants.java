package euroatomizado.idea.gestpes._configuracion.constantes;

public final class TiposPesajeTicketsConstants {

    public static String BASCULA = "Bascula";
    public static String AAA = "1";
    public static String OOO = "2";
    public static String BBB = "3";

    private TiposPesajeTicketsConstants() {
    }

    public static String obtenerDescripcionTipoPesaje(String tipo){
        if(tipo.equals("0")){
            return BASCULA;
        }else if(tipo.equals("1")){
            return AAA;
        }else if(tipo.equals("2")){
            return OOO;
        }else if(tipo.equals("3")){
            return BBB;
        }else{
            return "";
        }
    }
}
