package euroatomizado.idea.gestpes._configuracion.constantes;

public final class SeccionesConstants {

    public static String NPC = "NPC";
    public static String EURO = "EURO";

    private SeccionesConstants() {
    }

    public static String obtenerDescripcionSeccion(String codigo){
        if(codigo.equals("0")){
            return NPC;
        }else if(codigo.equals("1")){
            return EURO;
        }else{
            return EURO;
        }
    }
}
