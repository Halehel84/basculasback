package euroatomizado.idea.gestpes._configuracion.constantes;

public final class EstadosAlbaranTicketsConstants {

    public static String ABIERTO = "Abierto";
    public static String CERRADO = "Cerrado";
    public static String BORRADO = "Borrado";

    private EstadosAlbaranTicketsConstants() {
    }

    public static String obtenerDescripcionEstado(String tipo){
        if(tipo.equals("A")){
            return ABIERTO;
        }else if(tipo.equals("C")){
            return CERRADO;
        }else if(tipo.equals("B")){
            return BORRADO;
        }else{
            return "";
        }
    }
}
