package euroatomizado.idea.gestpes._configuracion.constantes;

public final class TiposOperacionTicketsConstants {

    public static String TRASPASO = "Traspaso";
    public static String SALIDA_RESIDUOS = "Salida residuos";
    public static String VENTA = "Venta";
    public static String COMPRA = "Compra";
    public static String PRUEBA = "Prueba";
    public static String ENTRADA_RESIDUOS= "Entrada residuos";

    private TiposOperacionTicketsConstants() {
    }

    public static String obtenerDescripcionTipoOperacion(String tipo){
        if(tipo.equals("T")){
            return TRASPASO;
        }else if(tipo.equals("S")){
            return SALIDA_RESIDUOS;
        }else if(tipo.equals("P")){
            return PRUEBA;
        }else if(tipo.equals("R")){
            return ENTRADA_RESIDUOS;
        }else if(tipo.equals("C")){
            return COMPRA;
        }else if(tipo.equals("V")){
            return VENTA;
        }else{
            return "";
        }
    }
}
