package euroatomizado.idea.gestpes._configuracion.constantes;

public final class ConstantesRoles {

    public static final String ADMINISTRADOR = "ROLE_1701";
    public static final String CONSULTA = "ROLE_1702";

    private ConstantesRoles() {
    }
}
