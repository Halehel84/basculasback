package euroatomizado.idea.gestpes._configuracion.dominio.dto.datosBasicos;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class DatosBasicosFormDto {
    @NotNull
    @Size(min = 1, max = 500)
    private String descripcion;
    @Size(min = 1, max = 1000)
    private String descripcionCompleta;
    @NotNull
    private boolean baja;
}
