package euroatomizado.idea.gestpes._configuracion.dominio.dto.datosBasicos;

import lombok.Data;

@Data

public class DatosBasicosInfoDto {
    private Long id;
    private String codigo;
    private String descripcion;
    private boolean baja;
}
