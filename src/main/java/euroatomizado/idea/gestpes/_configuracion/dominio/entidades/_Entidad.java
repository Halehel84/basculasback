package euroatomizado.idea.gestpes._configuracion.dominio.entidades;

import lombok.Data;

import javax.persistence.*;

@MappedSuperclass
@Data
public abstract class _Entidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

}
