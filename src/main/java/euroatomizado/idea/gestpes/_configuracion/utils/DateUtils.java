package euroatomizado.idea.gestpes._configuracion.utils;

import java.sql.Time;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class DateUtils {

    public static LocalDateTime obtenerFechaDeString(String fecha) {
        /*LocalDateTime localDateFecha = LocalDateTime.now();
        if(fecha != null){
            DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
            ZonedDateTime parsed = ZonedDateTime.parse(fecha + "T00:00:00.000Z", formatter.withZone(ZoneId.of("UTC")));
            localDateFecha = parsed.toLocalDateTime();
        }
        return localDateFecha;*/
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        return LocalDateTime.parse(fecha, formatter);
    }

    public static String formatearFecha(LocalDateTime fecha) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        return fecha.format(formatter);
    }

    public static LocalDate obtenerFechaPrimerDiaSemana(LocalDate fecha) {
        DayOfWeek firstDayOfWeek = WeekFields.of(Locale.forLanguageTag("es_ES")).getFirstDayOfWeek();
        return fecha.with(TemporalAdjusters.previousOrSame(firstDayOfWeek)).plusDays(1);
    }

    public static LocalDate obtenerFechaUltimoDiaSemana(LocalDate fecha) {
        DayOfWeek firstDayOfWeek = WeekFields.of(Locale.forLanguageTag("es_ES")).getFirstDayOfWeek();
        fecha.with(TemporalAdjusters.previousOrSame(firstDayOfWeek));
        DayOfWeek lastDayOfWeek = firstDayOfWeek.plus(6);
        return fecha.with(TemporalAdjusters.nextOrSame(lastDayOfWeek)).plusDays(1);
    }

    public static LocalDateTime localDateToLocalDateTime(LocalDate fecha){
        LocalTime hora = LocalTime.of(0,0,0);
        return LocalDateTime.of(fecha, hora);
    }

}
