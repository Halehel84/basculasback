package euroatomizado.idea.gestpes._configuracion.utils;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class GatewaysUtils {
    public static boolean postSinRespuesta(RestTemplate restTemplate, String url, Object requestBody) {
        if (url == null) return false;
        ResponseEntity<Void> response = restTemplate.postForEntity(url, requestBody, Void.class);
        return response.getStatusCodeValue() == 200;
    }
}
