package euroatomizado.idea.gestpes._configuracion.utils;

import org.springframework.web.client.RestTemplate;

public class RestUtils {

    public static Object realizarLlamada(String url){
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url, Object.class);
    }

}
