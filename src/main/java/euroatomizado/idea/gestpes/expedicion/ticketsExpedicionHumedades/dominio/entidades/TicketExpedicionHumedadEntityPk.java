package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class TicketExpedicionHumedadEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresaId;
    @Column(name = "tic_id")
    private String ticketId;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "teh_codigo")
    private Integer codigo;

}