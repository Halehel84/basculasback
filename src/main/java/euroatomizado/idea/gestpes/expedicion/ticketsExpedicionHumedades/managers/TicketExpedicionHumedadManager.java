package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.managers;

import euroatomizado.idea.gestpes._configuracion.configs.dataSources.EmpresaService;
import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades.TicketExpedicionHumedadEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades.TicketExpedicionHumedadEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class TicketExpedicionHumedadManager implements PrimaryKeyStringManagerInterface<TicketExpedicionHumedadInfoDto,
        TicketExpedicionHumedadEntity, TicketExpedicionHumedadEntityPk, TicketExpedicionHumedadFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;
    @Autowired
    private EmpresaService empresaService;

    @Transactional(rollbackFor = Exception.class)
    public List<TicketExpedicionHumedadInfoDto> obtenerHumedades(String ticket) {
        return obtenerDatosSalida(daoManager.getTicketExpedicionHumedadDao().obtenerHumedadesPorTicket(ticket));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TicketExpedicionHumedadInfoDto guardar(TicketExpedicionHumedadFormDto datos) {
        TicketExpedicionHumedadEntity humedad = mapperManager.getMapeadorTicketExpedicionHumedad().toEntidad(datos);
        humedad.setTicketExpedicionHumedadFormDto(datos);
        humedad.setTicketExpedicionHumedadPk(obtenerPrimaryKey(datos.getTicketId(), datos.getCodigo()));
        try {
            return obtenerDatosSalida(guardarNuevo(humedad));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void guardar(List<TicketExpedicionHumedadFormDto> humedades, String ticketId) {
        borrarHumedades(ticketId);
        Integer codigo = 1;
        for (TicketExpedicionHumedadFormDto humedad : humedades) {
            humedad.setCodigo(codigo);
            guardar(humedad);
            codigo = codigo + 1;
        }
    }

    @Override
    public TicketExpedicionHumedadEntity guardarNuevo(TicketExpedicionHumedadEntity nuevo) {
        daoManager.getTicketExpedicionHumedadDao().save(nuevo);
        return nuevo;
    }

    @Override
    public TicketExpedicionHumedadEntity modificar(TicketExpedicionHumedadEntity nuevo) {
        return nuevo;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        TicketExpedicionHumedadEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getTicketExpedicionHumedadDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrarHumedades(String ticketId) {
        List<TicketExpedicionHumedadEntity> lista = daoManager.getTicketExpedicionHumedadDao().obtenerHumedadesPorTicket(ticketId);
        for (TicketExpedicionHumedadEntity humedad : lista) {
            daoManager.getTicketExpedicionHumedadDao().delete(humedad);
        }
    }

    @Override
    public TicketExpedicionHumedadInfoDto obtenerDatosSalida(TicketExpedicionHumedadEntity almacen) {
        return mapperManager.getMapeadorTicketExpedicionHumedad().toInfoDto(almacen);
    }

    private List<TicketExpedicionHumedadInfoDto> obtenerDatosSalida(List<TicketExpedicionHumedadEntity> almacenes) {
        return mapperManager.getMapeadorTicketExpedicionHumedad().toInfoDto(almacenes);
    }

    @Override
    public boolean existe(TicketExpedicionHumedadEntityPk pk) {
        return pk != null && daoManager.getTicketExpedicionHumedadDao().exists(pk);
    }

    @Override
    public TicketExpedicionHumedadEntity obtener(TicketExpedicionHumedadEntity datos) {
        return daoManager.getTicketExpedicionHumedadDao().findOne(datos.getTicketExpedicionHumedadPk());
    }

    @Override
    public TicketExpedicionHumedadEntityPk obtenerPrimaryKey(String id) {
        TicketExpedicionHumedadEntityPk pk = new TicketExpedicionHumedadEntityPk();
        pk.setTicketId(id);
        pk.setEmpresaId(EMPRESA);
        pk.setSeccionId(empresaService.getEmpresa());
        return pk;
    }

    public TicketExpedicionHumedadEntityPk obtenerPrimaryKey(String id, Integer codigo) {
        TicketExpedicionHumedadEntityPk pk = new TicketExpedicionHumedadEntityPk();
        pk.setTicketId(id);
        pk.setEmpresaId(EMPRESA);
        pk.setSeccionId(empresaService.getEmpresa());
        pk.setCodigo(codigo);
        return pk;
    }

}
