package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class TicketExpedicionHumedadFormDto {

    @NotNull
    @Size(max = 50)
    private String ticketId;
    @NotNull
    private Integer codigo;
    @NotNull
    private Float humedad;
    @Size(max = 1)
    private String fueraRango;

}
