package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.mapeadores;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades.TicketExpedicionHumedadEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorTicketExpedicionHumedad {

    @Mapping(source = "ticketExpedicionHumedadPk.ticketId", target = "ticketId")
    @Mapping(source = "ticketExpedicionHumedadPk.empresaId", target = "empresaId")
    @Mapping(source = "ticketExpedicionHumedadPk.seccionId", target = "seccionId")
    @Mapping(source = "ticketExpedicionHumedadPk.codigo", target = "codigo")
    TicketExpedicionHumedadInfoDto toInfoDto(TicketExpedicionHumedadEntity datos);

    List<TicketExpedicionHumedadInfoDto> toInfoDto(List<TicketExpedicionHumedadEntity> datos);

    TicketExpedicionHumedadEntity toEntidad(TicketExpedicionHumedadFormDto dto);

    void actualizar(TicketExpedicionHumedadEntity datosNueva, @MappingTarget TicketExpedicionHumedadEntity datos);

}