package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadFormDto;
import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "tickets_expedicion_humedades", schema = "dbo")
@Data
public class TicketExpedicionHumedadEntity {

    @EmbeddedId
    private TicketExpedicionHumedadEntityPk ticketExpedicionHumedadPk;
    @Column(name = "teh_humedad", columnDefinition = "real")
    private Float humedad;
    @Column(name = "teh_fuera_rango")
    private String fueraRango;

    @Transient
    private TicketExpedicionHumedadFormDto ticketExpedicionHumedadFormDto;

}