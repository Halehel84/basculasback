package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.managers.TicketExpedicionHumedadManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/tickets-expedicion-humedad")
public class TicketExpedicionHumedadController {

    @Autowired
    private TicketExpedicionHumedadManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "obtener-humedades")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<TicketExpedicionHumedadInfoDto> obtenerHumedades(@PathParam("ticket") String ticket) {
        return manager.obtenerHumedades(ticket);
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public TicketExpedicionHumedadInfoDto crear(@RequestBody @Valid TicketExpedicionHumedadFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public TicketExpedicionHumedadInfoDto modificar(@RequestBody @Valid TicketExpedicionHumedadFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}