package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.daos;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades.TicketExpedicionHumedadEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades.TicketExpedicionHumedadEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketExpedicionHumedadDao extends CrudRepository<TicketExpedicionHumedadEntity, TicketExpedicionHumedadEntityPk> {

    List<TicketExpedicionHumedadEntity> findAll();

    @Query(" SELECT tic " +
            " FROM TicketExpedicionHumedadEntity as tic " +
            " WHERE tic.ticketExpedicionHumedadPk.ticketId = :ticket ")
    List<TicketExpedicionHumedadEntity> obtenerHumedadesPorTicket(@Param("ticket") String ticket);

}
