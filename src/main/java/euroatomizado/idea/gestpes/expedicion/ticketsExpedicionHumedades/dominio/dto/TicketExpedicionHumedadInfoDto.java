package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto;

import lombok.Data;

@Data
public class TicketExpedicionHumedadInfoDto {

    private String empresaId;
    private String ticketId;
    private String seccionId;
    private Integer codigo;
    private Float humedad;
    private String fueraRango;

}