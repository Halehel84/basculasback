package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity()
@Table(name = "instrucciones_productivas_humedades", schema = "dbo")
@Data
public class InstruccionProductivaHumedadEntity {

    @Id
    @Column(name = "inh_codigo")
    private Integer inhCodigo;
    @Column(name = "inh_empresa")
    private String inhEmpresa;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "inh_humedad_max")
    private BigDecimal inhHumedadMax;
    @Column(name = "inh_humedad_min")
    private BigDecimal inhHumedadMin;
    @Column(name = "inh_observaciones")
    private String inhObservaciones;
    @Column(name = "inh_fecha_inicio")
    private LocalDateTime inhFechaInicio;
    @Column(name = "inh_fecha_fin")
    private LocalDateTime inhFechaFin;
    @Column(name = "xcliente_id")
    private String clienteId;
    @Column(name = "art_codigo")
    private Integer artCodigo;
    @Column(name = "xarticulo_id")
    private String articuloId;
    @Column(name = "inh_activo")
    private String inhActivo;
    @Column(name = "inh_usu_alta")
    private String inhUsuAlta;
    @Column(name = "inh_fecha_alta")
    private LocalDateTime inhFechaAlta;
    @Column(name = "inh_usu_mod")
    private String inhUsuMod;
    @Column(name = "inh_fecha_mod")
    private LocalDateTime inhFechaMod;
    @Column(name = "inh_traspasado")
    private String inhTraspasado;
    @Column(name = "inh_actualizado")
    private String inhActualizado;

}
