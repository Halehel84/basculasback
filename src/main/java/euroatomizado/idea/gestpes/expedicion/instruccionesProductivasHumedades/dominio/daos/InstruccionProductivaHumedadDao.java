package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.daos;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.entidades.InstruccionProductivaEntity;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.entidades.InstruccionProductivaHumedadEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface InstruccionProductivaHumedadDao extends CrudRepository<InstruccionProductivaHumedadEntity, Integer> {

    @Query(" SELECT hum " +
            " FROM InstruccionProductivaHumedadEntity as hum " +
            " WHERE hum.inhEmpresa = 'NPC' " +
            " AND hum.inhFechaInicio <= :fecha " +
            " AND (hum.inhFechaFin >= :fecha or hum.inhFechaFin is null) " +
            " AND hum.clienteId = :cliente " +
            " AND hum.articuloId = :articulo")
    List<InstruccionProductivaHumedadEntity> obtenerHumedadesPorFecha(@Param("fecha") LocalDateTime fechaInicio, @Param("cliente")String cliente, @Param("articulo")String articulo);

}
