package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.dto.InstruccionProductivaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.dto.InstruccionProductivaHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.managers.InstruccionProductivaHumedadManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.time.LocalDateTime;
import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.utils.DateUtils.obtenerFechaDeString;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/instrucciones-productivas-humedades")
public class InstruccionProductivaHumedadController {

    @Autowired
    private InstruccionProductivaHumedadManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "/por-fecha")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public InstruccionProductivaHumedadInfoDto obtenerInstruccionProductivaPorFecha(@PathParam("fecha") String fecha, @PathParam("articulo") String articulo, @PathParam("cliente") String cliente) {
        LocalDateTime fechaObtenida = obtenerFechaDeString(fecha);
        return manager.obtenerHumedadesPorFecha(fechaObtenida, articulo, cliente);
    }

}
