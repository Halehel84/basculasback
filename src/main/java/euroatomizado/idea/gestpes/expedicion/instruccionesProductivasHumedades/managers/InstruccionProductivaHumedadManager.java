package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.dto.InstruccionProductivaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.entidades.InstruccionProductivaEntity;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.dto.InstruccionProductivaHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.entidades.InstruccionProductivaHumedadEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class InstruccionProductivaHumedadManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;


    @Transactional(rollbackFor = Exception.class)
    public InstruccionProductivaHumedadInfoDto obtenerHumedadesPorFecha(LocalDateTime fecha, String articulo, String cliente) {
        LocalDateTime fechaInicio = fecha;
        List<InstruccionProductivaHumedadEntity> lista = daoManager.getInstruccionProductivaHumedadDao().obtenerHumedadesPorFecha(fechaInicio, cliente, articulo);
        if (lista != null && !lista.isEmpty()) {
            InstruccionProductivaHumedadInfoDto humedad = mapperManager.getMapeadorInstruccionProductivaHumedad().toInfoDto(lista.get(0));
            return humedad;
        }
        return null;
    }
}
