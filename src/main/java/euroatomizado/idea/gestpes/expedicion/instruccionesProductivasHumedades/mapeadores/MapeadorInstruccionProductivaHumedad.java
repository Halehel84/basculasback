package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.mapeadores;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.dto.InstruccionProductivaHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.entidades.InstruccionProductivaHumedadEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorInstruccionProductivaHumedad {

    InstruccionProductivaHumedadInfoDto toInfoDto(InstruccionProductivaHumedadEntity datos);

    List<InstruccionProductivaHumedadInfoDto> toInfoDto(List<InstruccionProductivaHumedadEntity> datos);

    void actualizar(InstruccionProductivaHumedadEntity datosNueva, @MappingTarget InstruccionProductivaHumedadEntity datos);

}