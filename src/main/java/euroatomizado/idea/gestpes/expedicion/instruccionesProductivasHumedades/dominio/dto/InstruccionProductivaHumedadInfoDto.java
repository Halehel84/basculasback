package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasHumedades.dominio.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class InstruccionProductivaHumedadInfoDto {

    private Integer inhCodigo;
    private String inhEmpresa;
    private String seccionId;
    private BigDecimal inhHumedadMax;
    private BigDecimal inhHumedadMin;
    private String inhObservaciones;
    private LocalDateTime inhFechaInicio;
    private LocalDateTime inhFechaFin;
    private String clienteId;
    private Integer artCodigo;
    private String articuloId;
    private String inhActivo;

}
