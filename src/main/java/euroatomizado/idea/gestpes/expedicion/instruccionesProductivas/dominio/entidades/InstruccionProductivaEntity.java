package euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.entidades;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.entidades.InstruccionProductivaLineaEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity()
@Table(name = "instrucciones_productivas", schema = "dbo")
@Data
public class InstruccionProductivaEntity {

    @Id
    @Column(name = "ins_codigo")
    private Integer insCodigo;
    @Column(name = "ins_empresa")
    private String insEmpresa;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "per_codigo")
    private Integer perCodigo;
    @Column(name = "ins_fecha")
    private LocalDateTime insFecha;
    @Column(name = "ins_observaciones")
    private String insObservaciones;
    @OneToMany(mappedBy = "instruccion")
    private List<InstruccionProductivaLineaEntity> lineas = new LinkedList<>();

}
