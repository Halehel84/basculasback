package euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.dto;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.dto.InstruccionProductivaLineaInfoDto;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class InstruccionProductivaInfoDto {

    private Integer insCodigo;
    private String insEmpresa;
    private String seccionId;
    private Integer perCodigo;
    private LocalDateTime insFecha;
    private String insObservaciones;
    private String descripcionInstruccion;
    private List<InstruccionProductivaLineaInfoDto> lineas;
}
