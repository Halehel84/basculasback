package euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.dto.InstruccionProductivaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.entidades.InstruccionProductivaEntity;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.dto.InstruccionProductivaLineaInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class InstruccionProductivaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public InstruccionProductivaInfoDto obtenerInstruccionProductivaPorFecha(LocalDateTime fecha, String articulo, String cliente) {
        LocalDateTime fechaInicio = fecha;
        LocalDateTime fechaFin = fecha.withHour(23).withMinute(59).withSecond(59);
        List<InstruccionProductivaEntity> lista = daoManager.getInstruccionProductivaDao().obtenerInstruccionProductivaPorFecha(fechaInicio, fechaFin);
        if (lista != null && !lista.isEmpty()) {
            InstruccionProductivaInfoDto instruccion = mapperManager.getMapeadorInstruccionProductiva().toInfoDto(lista.get(0));
            instruccion.setDescripcionInstruccion(formatearInstrucciones(instruccion, articulo, cliente));
            return instruccion;
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public InstruccionProductivaInfoDto obtenerInstruccionProductivaUltima(String articulo, String cliente) {
        List<InstruccionProductivaEntity> lista = daoManager.getInstruccionProductivaDao().obtenerInstruccionProductivaOrdenadasPorCodigo();
        if (lista != null && !lista.isEmpty()) {
            InstruccionProductivaInfoDto instruccion = mapperManager.getMapeadorInstruccionProductiva().toInfoDto(lista.get(0));
            instruccion.setDescripcionInstruccion(formatearInstrucciones(instruccion, articulo, cliente));
            return instruccion;
        }
        return null;
    }

    private String formatearInstrucciones(InstruccionProductivaInfoDto instruccion, String articulo, String cliente) {
        String descripcionInstruccion = "";
        for (InstruccionProductivaLineaInfoDto linea : instruccion.getLineas()) {
            if((linea.getArticuloId() != null
                    && linea.getArticuloId().equals(articulo))
                    || (linea.getClienteId() != null
                    && linea.getClienteId().equals(cliente))){
                descripcionInstruccion = descripcionInstruccion + linea.getInslInstruccion();
            }
        }
        return descripcionInstruccion;
    }
}
