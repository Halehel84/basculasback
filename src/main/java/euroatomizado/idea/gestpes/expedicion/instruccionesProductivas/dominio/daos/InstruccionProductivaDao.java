package euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.daos;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.entidades.InstruccionProductivaEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface InstruccionProductivaDao extends CrudRepository<InstruccionProductivaEntity, Integer> {

    @Query(" SELECT ins " +
            " FROM InstruccionProductivaEntity as ins " +
            " WHERE ins.insEmpresa = 'NPC' " +
            " AND ins.insFecha between :fechaInicio and :fechaFin ")
    List<InstruccionProductivaEntity> obtenerInstruccionProductivaPorFecha(@Param("fechaInicio")LocalDateTime fechaInicio, @Param("fechaFin")LocalDateTime fechaFin);

    @Query(" SELECT ins " +
            " FROM InstruccionProductivaEntity as ins " +
            " WHERE ins.insEmpresa = 'NPC' " +
            " ORDER BY ins.insCodigo desc ")
    List<InstruccionProductivaEntity> obtenerInstruccionProductivaOrdenadasPorCodigo();
}
