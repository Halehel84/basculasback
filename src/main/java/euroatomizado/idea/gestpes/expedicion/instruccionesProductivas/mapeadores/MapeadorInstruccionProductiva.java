package euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.mapeadores;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.dto.InstruccionProductivaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.entidades.InstruccionProductivaEntity;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.dto.InstruccionProductivaLineaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.entidades.InstruccionProductivaLineaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorInstruccionProductiva {

    InstruccionProductivaLineaInfoDto toInfoLineaDto(InstruccionProductivaLineaEntity datos);

    List<InstruccionProductivaLineaInfoDto> toInfoLineaDto(List<InstruccionProductivaLineaEntity> datos);

    InstruccionProductivaInfoDto toInfoDto(InstruccionProductivaEntity datos);

    List<InstruccionProductivaInfoDto> toInfoDto(List<InstruccionProductivaEntity> datos);

    void actualizar(InstruccionProductivaEntity datosNueva, @MappingTarget InstruccionProductivaEntity datos);

}