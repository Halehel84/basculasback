package euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.dto.InstruccionProductivaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.managers.InstruccionProductivaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.time.LocalDateTime;
import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.utils.DateUtils.obtenerFechaDeString;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/instrucciones-productivas")
public class InstruccionProductivaController {

    @Autowired
    private InstruccionProductivaManager manager;


    @ResponseStatus(OK)
    @GetMapping(value = "/por-fecha")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public InstruccionProductivaInfoDto obtenerInstruccionProductivaPorFecha(@PathParam("fecha") String fecha, @PathParam("articulo") String articulo, @PathParam("cliente") String cliente) {
        LocalDateTime fechaObtenida = obtenerFechaDeString(fecha);
        return manager.obtenerInstruccionProductivaPorFecha(fechaObtenida, articulo, cliente);
    }

    @ResponseStatus(OK)
    @GetMapping(value = "/ultima")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public InstruccionProductivaInfoDto obtenerInstruccionProductivaUltima(@PathParam("articulo") String articulo, @PathParam("cliente") String cliente) {
        return manager.obtenerInstruccionProductivaUltima(articulo, cliente);
    }

}
