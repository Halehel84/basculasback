package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosInfoDto;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class TicketExpedicionFormDto {

    @NotNull
    @Size(max = 50)
    private String ticketId;
    private Float humedadMinima;
    private Float humedadMaxima;
    private String instruccionesProductivas;
    @Size(max = 1)
    private String fueraRango;
    private String observaciones;
    private LocalDateTime fechaRegistro;
    private LocalDateTime fechaConsulta;
    private LocalDateTime fechaInstrucciones;
    private Integer perCodigo;
    @Size(max = 2000)
    private String perNombre;
    private Float humedadReferencia;
    @Size(max = 25)
    private String articuloIdHumedades;
    @Size(max = 25)
    private String articuloIdInstrucciones;
    @Size(max = 1)
    private String validado;
    private List<TicketExpedicionHumedadFormDto> humedadesGuardar;
    private List<TicketExpedicionSilosFormDto> silosGuardar;

}
