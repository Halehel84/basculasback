package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class TicketExpedicionEntityPk implements Serializable {

    @Column(name = "xempresa_id")
    private String empresaId;
    @Column(name = "tic_id")
    private String ticketId;
    @Column(name = "xseccion_id")
    private String seccionId;

}
