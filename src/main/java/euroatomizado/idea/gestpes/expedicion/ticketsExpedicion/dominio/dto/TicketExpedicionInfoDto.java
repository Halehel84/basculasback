package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class TicketExpedicionInfoDto {

    private String ticketId;
    private String empresaId;
    private String seccionId;
    private Float humedadMinima;
    private Float humedadMaxima;
    private String instruccionesProductivas;
    private String fueraRango;
    private String observaciones;
    private LocalDateTime fechaRegistro;
    private LocalDateTime fechaConsulta;
    private LocalDateTime fechaInstrucciones;
    private String sinConexionIdeaprod;
    private Integer perCodigo;
    private String perNombre;
    private Float humedadReferencia;
    private String articuloIdHumedades;
    private String articuloIdInstrucciones;
    private String validado;
    private String matricula;
    private LocalDateTime fechaEntrada;
    private LocalDateTime fechaSalida;
    private String clienteId;
    private String clienteDescripcion;
    private String articuloId;
    private String articuloDescripcion;
    private String seccionDescripcionCorta;
    private String entidadDescripcion;
    private List<Float> humedades;
    private String silos;
    private LocalDateTime fecha;

}
