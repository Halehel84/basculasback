package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class FiltrosConsultaExpedicionFormDto {

    private String planta;
    private String entidad;
    private String operario;
    @NotNull
    private LocalDateTime inicio;
    private LocalDateTime fin;
    private String cliente;
    private String articulo;
    //private Boolean mostrarAlbaranesAnulados;

}
