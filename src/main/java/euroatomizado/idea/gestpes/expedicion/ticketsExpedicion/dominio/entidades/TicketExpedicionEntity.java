package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionFormDto;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "tickets_expedicion", schema = "dbo")
@Data
public class TicketExpedicionEntity {

    @EmbeddedId
    private TicketExpedicionEntityPk ticketExpedicionPk;
    @Column(name = "tex_humedad_minima", columnDefinition = "real")
    private Float humedadMinima;
    @Column(name = "tex_humedad_maxima", columnDefinition = "real")
    private Float humedadMaxima;
    @Column(name = "tex_instrucciones_productivas")
    private String instruccionesProductivas;
    @Column(name = "tex_fuera_rango")
    private String fueraRango;
    @Column(name = "tex_observaciones")
    private String observaciones;
    @Column(name = "usu_id")
    private String usuarioId;
    @Column(name = "tex_fecha_registro")
    private LocalDateTime fechaRegistro;
    @Column(name = "tex_fecha_consulta")
    private LocalDateTime fechaConsulta;
    @Column(name = "tex_fecha_instrucciones")
    private LocalDateTime fechaInstrucciones;
    @Column(name = "tex_sin_conexion_ideaprod")
    private String sinConexionIdeaprod;
    @Column(name = "per_codigo")
    private Integer perCodigo;
    @Column(name = "per_nombre")
    private String perNombre;
    @Column(name = "tex_humedad_referencia", columnDefinition = "real")
    private Float humedadReferencia;
    @Column(name = "xarticulo_id_humedades")
    private String articuloIdHumedades;
    @Column(name = "xarticulo_id_instrucciones")
    private String articuloIdInstrucciones;
    @Column(name = "tex_validado")
    private String validado;
    @Column(name = "tex_hora_inicio")
    private LocalDateTime horaInicio;
    @Column(name = "tex_hora_fin")
    private LocalDateTime horaFin;
    @Column(name = "tex_caudal_minimo", columnDefinition = "real")
    private Float caudalMinimo;
    @Column(name = "tex_caudal_maximo", columnDefinition = "real")
    private Float caudalMaximo;
    @Column(name = "tex_observaciones_euroelettra")
    private String observacionesEuroelettra;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xempresa_id", referencedColumnName = "xempresa_id", insertable = false, updatable = false),
            @JoinColumn(name = "tic_id", referencedColumnName = "tic_id", insertable = false, updatable = false),
            @JoinColumn(name = "xseccion_id", referencedColumnName = "xseccion_id", insertable = false, updatable = false)
    })
    private TicketEntity ticket;

    @Transient
    private TicketExpedicionFormDto ticketExpedicionFormDto;

}