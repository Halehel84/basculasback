package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.daos;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntityPk;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketExpedicionDao extends CrudRepository<TicketExpedicionEntity, TicketExpedicionEntityPk> {

    @Query(" SELECT tic " +
            " FROM TicketExpedicionEntity as tic " +
            " WHERE tic.ticketExpedicionPk.empresaId = 'NPC' ")
    List<TicketExpedicionEntity> findAll();

    @Query(" SELECT tic " +
            " FROM TicketEntity as tic " +
            " WHERE tic.ticketPk.empresaId = 'NPC' " +
            " AND tic.fechaSalida is null " +
            " AND tic.albaranEstado is null " +
            " AND (tic.tipoOperacion = 'V' or tic.tipoOperacion = 'P') ")
    List<TicketEntity> obtenerExpedicionesEnCurso();

    @Query(" SELECT tic " +
            " FROM TicketExpedicionEntity as tic " +
            " WHERE tic.ticket.ticketPk.empresaId = 'NPC' " +
            " AND tic.ticket.ticketPk.ticId=:ticId ")
    TicketExpedicionEntity obtenerExpedicionPorTicket(@Param("ticId") String ticId);
}
