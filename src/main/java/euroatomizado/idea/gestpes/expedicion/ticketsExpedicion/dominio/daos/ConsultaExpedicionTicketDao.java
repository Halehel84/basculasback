package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.daos;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.FiltrosConsultaExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.QTicketExpedicionEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntityPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public interface ConsultaExpedicionTicketDao extends CrudRepository<TicketExpedicionEntity, TicketExpedicionEntityPk> {

    default List<TicketExpedicionEntity> consultaExpedicionTickets(FiltrosConsultaExpedicionFormDto filtros, EntityManager em) {
        QTicketExpedicionEntity ticketExpedicionEntity = QTicketExpedicionEntity.ticketExpedicionEntity;
        /*FROM*/
        JPQLQuery queryPrincipal = new JPAQuery(em);
        queryPrincipal.from(ticketExpedicionEntity);
        /*WHERE*/
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if (filtros.getInicio() != null && filtros.getFin() != null) {
            booleanBuilder.and(ticketExpedicionEntity.ticket.fecha.between(filtros.getInicio(), filtros.getFin()));
        }
        if (filtros.getCliente() != null) {
            booleanBuilder.and(ticketExpedicionEntity.ticket.cliente.clientePk.clienteId.eq(filtros.getCliente()));
        }
        if (filtros.getArticulo() != null) {
            booleanBuilder.and(ticketExpedicionEntity.ticket.articulo.articuloPk.articuloId.eq(filtros.getArticulo()));
        }
        if (filtros.getEntidad() != null) {
            booleanBuilder.and(ticketExpedicionEntity.ticket.entidad.eq(filtros.getEntidad()));
        }
        if (filtros.getPlanta() != null) {
            booleanBuilder.and(ticketExpedicionEntity.ticket.seccion.seccionId.eq(filtros.getPlanta()));
        }
        if (filtros.getOperario() != null) {
            booleanBuilder.and(ticketExpedicionEntity.perNombre.eq(filtros.getOperario()));
        }
        queryPrincipal.where(booleanBuilder);
        queryPrincipal.select(ticketExpedicionEntity);
        /*ORDENACION*/
        queryPrincipal.orderBy(ticketExpedicionEntity.ticket.ticketPk.seccionId.asc(), ticketExpedicionEntity.ticket.fecha.asc());
        return queryPrincipal.fetch();
    }
}
