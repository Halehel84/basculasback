package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.mapeadores;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorTicketExpedicion {

    @Mapping(source = "ticket.matricula", target = "matricula")
    @Mapping(source = "ticket.fechaEntrada", target = "fechaEntrada")
    @Mapping(source = "ticket.cliente.clientePk.clienteId", target = "clienteId")
    @Mapping(source = "ticket.cliente.nombre", target = "clienteDescripcion")
    @Mapping(source = "ticket.articulo.articuloPk.articuloId", target = "articuloId")
    @Mapping(source = "ticket.articulo.descripcion", target = "articuloDescripcion")
    @Mapping(source = "ticketExpedicionPk.ticketId", target = "ticketId")
    @Mapping(source = "ticketExpedicionPk.empresaId", target = "empresaId")
    @Mapping(source = "ticketExpedicionPk.seccionId", target = "seccionId")
    TicketExpedicionInfoDto toInfoDto(TicketExpedicionEntity datos);

    List<TicketExpedicionInfoDto> toInfoDto(List<TicketExpedicionEntity> datos);

    TicketExpedicionEntity toEntidad(TicketExpedicionFormDto dto);

    void actualizar(TicketExpedicionEntity datosNueva, @MappingTarget TicketExpedicionEntity datos);

}
