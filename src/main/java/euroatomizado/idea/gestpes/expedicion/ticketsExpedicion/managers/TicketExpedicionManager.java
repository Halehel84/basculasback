package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.managers;

import euroatomizado.idea.gestpes._configuracion.configs.dataSources.EmpresaService;
import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.FiltrosConsultaExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntityPk;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades.TicketExpedicionHumedadEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.managers.TicketExpedicionHumedadManager;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.entidades.TicketExpedicionSilosEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.managers.TicketExpedicionSilosManager;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.FiltrosConsultaHistoricoFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class TicketExpedicionManager implements PrimaryKeyStringManagerInterface<TicketExpedicionInfoDto, TicketExpedicionEntity, TicketExpedicionEntityPk, TicketExpedicionFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;
    @Autowired
    private EmpresaService empresaService;
    @Autowired
    private TicketExpedicionHumedadManager ticketExpedicionHumedadManager;
    @Autowired
    private TicketExpedicionSilosManager ticketExpedicionSilosManager;
    @PersistenceContext
    private EntityManager em;

    @Transactional(rollbackFor = Exception.class)
    public List<TicketInfoDto> obtenerTicketsExpedicion() {
        List<TicketEntity> lista = daoManager.getTicketExpedicionDao().obtenerExpedicionesEnCurso();
        return mapperManager.getMapeadorTicket().toInfoDto(lista);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TicketExpedicionInfoDto> obtenerExpedicionTickets(FiltrosConsultaExpedicionFormDto filtros) {
        if (filtros.getInicio() != null) {
            filtros.setInicio(filtros.getInicio().withHour(0).withMinute(0).withSecond(0).withNano(0));
        }
        if (filtros.getFin() != null) {
            filtros.setFin(filtros.getFin().withHour(23).withMinute(59).withSecond(59).withNano(0));
        }
        List<TicketExpedicionEntity> datosEntity = daoManager.getConsultaExpedicionTicketDao().consultaExpedicionTickets(filtros, em);
        List<TicketExpedicionInfoDto> datosInfo = obtenerDatosSalida(datosEntity);
        return asignarCamposRelacionadosConsulta(datosEntity, datosInfo);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public TicketExpedicionInfoDto guardar(TicketExpedicionFormDto datos) {
        TicketExpedicionEntity expedicion = mapperManager.getMapeadorTicketExpedicion().toEntidad(datos);
        expedicion.setTicketExpedicionFormDto(datos);
        try {
            TicketExpedicionEntityPk pk = obtenerPrimaryKey(expedicion.getTicketExpedicionFormDto().getTicketId());
            expedicion.setTicketExpedicionPk(pk);
            if (!existe(expedicion.getTicketExpedicionPk()))
                return obtenerDatosSalida(guardarNuevo(expedicion));
            return obtenerDatosSalida(modificar(expedicion));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Override
    public TicketExpedicionEntity guardarNuevo(TicketExpedicionEntity nuevo) {
        nuevo.setFechaRegistro(LocalDateTime.now());
        nuevo.setFechaConsulta(LocalDateTime.now());
        //TODO es el codigo de usuario que guarda la instruccion
        nuevo.setUsuarioId("31");
        daoManager.getTicketExpedicionDao().save(nuevo);
        guardarHumedades(nuevo.getTicketExpedicionFormDto().getHumedadesGuardar(), nuevo.getTicketExpedicionFormDto().getTicketId());
        return nuevo;
    }

    @Override
    public TicketExpedicionEntity modificar(TicketExpedicionEntity nuevo) {
        TicketExpedicionEntity antiguo = obtener(nuevo);
        mapperManager.getMapeadorTicketExpedicion().actualizar(nuevo, antiguo);
        nuevo.setUsuarioId("31");
        daoManager.getTicketExpedicionDao().save(nuevo);
        guardarHumedades(nuevo.getTicketExpedicionFormDto().getHumedadesGuardar(), nuevo.getTicketExpedicionFormDto().getTicketId());
        guardarSilos(nuevo.getTicketExpedicionFormDto().getSilosGuardar(), nuevo.getTicketExpedicionFormDto().getTicketId());
        return nuevo;
    }

    private void guardarHumedades(List<TicketExpedicionHumedadFormDto> humedades, String ticketId) {
        ticketExpedicionHumedadManager.guardar(humedades, ticketId);
    }

    private void guardarSilos(List<TicketExpedicionSilosFormDto> silos, String ticketId) {
        ticketExpedicionSilosManager.guardar(silos, ticketId);
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        TicketExpedicionEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getTicketExpedicionDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Override
    public TicketExpedicionInfoDto obtenerDatosSalida(TicketExpedicionEntity datos) {
        return mapperManager.getMapeadorTicketExpedicion().toInfoDto(datos);
    }

    private List<TicketExpedicionInfoDto> obtenerDatosSalida(List<TicketExpedicionEntity> datos) {
        return mapperManager.getMapeadorTicketExpedicion().toInfoDto(datos);
    }

    private List<TicketInfoDto> obtenerDatosSalidaConsulta(List<TicketEntity> datos) {
        return mapperManager.getMapeadorTicket().toInfoDto(datos);
    }

    @Override
    public boolean existe(TicketExpedicionEntityPk pk) {
        return pk != null && daoManager.getTicketExpedicionDao().exists(pk);
    }

    @Override
    public TicketExpedicionEntity obtener(TicketExpedicionEntity datos) {
        return daoManager.getTicketExpedicionDao().findOne(datos.getTicketExpedicionPk());
    }

    @Override
    public TicketExpedicionEntityPk obtenerPrimaryKey(String id) {
        TicketExpedicionEntityPk pk = new TicketExpedicionEntityPk();
        pk.setTicketId(id);
        pk.setEmpresaId(EMPRESA);
        pk.setSeccionId(empresaService.getEmpresa());
        return pk;
    }

    private List<TicketExpedicionInfoDto> asignarCamposRelacionadosConsulta(List<TicketExpedicionEntity> datosEntity, List<TicketExpedicionInfoDto> datosInfo) {
        for (int i = 0; i < datosEntity.size(); i++) {
            TicketEntity ticket = datosEntity.get(i).getTicket();
            TicketExpedicionInfoDto dato = datosInfo.get(i);

            dato.setSeccionDescripcionCorta(ticket.getSeccion().getNombreAbrev());
            dato.setEntidadDescripcion(ticket.getEntidad());
            dato.setFecha(ticket.getFecha());
            dato.setFechaEntrada(ticket.getFechaEntrada());
            dato.setFechaSalida(ticket.getFechaSalida());
            dato.setHumedades(asignarHumedades(dato));
            dato.setSilos(asignarSilos(dato));
        }

        return datosInfo;
    }

    private List<Float> asignarHumedades(TicketExpedicionInfoDto dato) {
        List<TicketExpedicionHumedadEntity> humedades = daoManager.getTicketExpedicionHumedadDao().obtenerHumedadesPorTicket(dato.getTicketId());
        List<Float> lista = new ArrayList<Float>();
        for (TicketExpedicionHumedadEntity humedad : humedades) {
            lista.add(humedad.getHumedad());
        }
        return lista;
    }

    private String asignarSilos(TicketExpedicionInfoDto dato) {
        List<TicketExpedicionSilosEntity> silos = daoManager.getTicketExpedicionSilosDao().obtenerSilosPorTicket(dato.getTicketId());
        String nsilos = "";
        for (TicketExpedicionSilosEntity silo : silos) {
            nsilos = nsilos + silo.getCodigoSilo() + " - ";
        }
        return nsilos;
    }

}
