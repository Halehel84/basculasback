package euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.FiltrosConsultaExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.managers.TicketExpedicionManager;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.FiltrosConsultaHistoricoFormDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/expedicion-tickets")
public class TicketExpedicionController {

    @Autowired
    private TicketExpedicionManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "/expediciones-actuales")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<TicketInfoDto> obtenerTicketsExpedicion() {
        return manager.obtenerTicketsExpedicion();
    }

    @ResponseStatus(OK)
    @PostMapping(value = "consulta")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<TicketExpedicionInfoDto> obtenerConsultaExpedicion(@RequestBody @Valid FiltrosConsultaExpedicionFormDto datos) {
        return manager.obtenerExpedicionTickets(datos);
    }


    @ResponseStatus(CREATED)
    @PostMapping
    public TicketExpedicionInfoDto crear(@RequestBody @Valid TicketExpedicionFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        } catch (NotFoundException e) {
            return new ResponseEntity(NOT_FOUND);
        }
    }

}