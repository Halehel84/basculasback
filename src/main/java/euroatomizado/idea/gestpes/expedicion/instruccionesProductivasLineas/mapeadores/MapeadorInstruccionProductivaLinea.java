package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.mapeadores;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.dto.InstruccionProductivaLineaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.entidades.InstruccionProductivaLineaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorInstruccionProductivaLinea {

    InstruccionProductivaLineaInfoDto toInfoDto(InstruccionProductivaLineaEntity datos);

    List<InstruccionProductivaLineaInfoDto> toInfoDto(List<InstruccionProductivaLineaEntity> datos);

    void actualizar(InstruccionProductivaLineaEntity datosNueva, @MappingTarget InstruccionProductivaLineaEntity datos);

}