package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.dto.InstruccionProductivaLineaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.managers.InstruccionProductivaLineaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/api/v1/instrucciones-productivas-lineas")
public class InstruccionProductivaLineaController {

    @Autowired
    private InstruccionProductivaLineaManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<InstruccionProductivaLineaInfoDto> obtenerInstruccionesProductivasLineas() {
        return manager.obtenerInstruccionesProductivasLineas();
    }

}
