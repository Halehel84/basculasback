package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class InstruccionProductivaLineaInfoDto {

    private Integer inslCodigo;
    private String inslEmpresa;
    private Integer artCodigo;
    private String articuloId;
    private String clienteId;
    private String inslInstruccion;

}
