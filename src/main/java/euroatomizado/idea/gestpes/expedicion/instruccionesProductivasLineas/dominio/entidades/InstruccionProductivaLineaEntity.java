package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.entidades;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivas.dominio.entidades.InstruccionProductivaEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity()
@Table(name = "instrucciones_productivas_lineas", schema = "dbo")
@Data
public class InstruccionProductivaLineaEntity {

    @Id
    @Column(name = "insl_codigo")
    private Integer inslCodigo;
    @Column(name = "insl_empresa")
    private String inslEmpresa;
    @Column(name = "art_codigo")
    private Integer artCodigo;
    @Column(name = "xarticulo_id")
    private String articuloId;
    @Column(name = "xcliente_id")
    private String clienteId;
    @Column(name = "insl_instruccion")
    private String inslInstruccion;

    @ManyToOne
    @JoinColumn(name = "ins_codigo")
    private InstruccionProductivaEntity instruccion;
}
