package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.daos;

import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.entidades.InstruccionProductivaLineaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstruccionProductivaLineaDao extends CrudRepository<InstruccionProductivaLineaEntity, Integer> {

    List<InstruccionProductivaLineaEntity> findAll();

}
