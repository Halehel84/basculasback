package euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.managers;

import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.dto.InstruccionProductivaLineaInfoDto;
import euroatomizado.idea.gestpes.expedicion.instruccionesProductivasLineas.dominio.entidades.InstruccionProductivaLineaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InstruccionProductivaLineaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;

    @Transactional(rollbackFor = Exception.class)
    public List<InstruccionProductivaLineaInfoDto> obtenerInstruccionesProductivasLineas() {
        List<InstruccionProductivaLineaEntity> lista = daoManager.getInstruccionProductivaLineaDao().findAll();
        return mapperManager.getMapeadorInstruccionProductivaLinea().toInfoDto(lista);
    }

}
