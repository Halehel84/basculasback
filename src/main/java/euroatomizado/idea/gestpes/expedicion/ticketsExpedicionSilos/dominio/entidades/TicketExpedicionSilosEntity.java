package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.entidades;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosFormDto;
import lombok.Data;

import javax.persistence.*;

@Entity()
@Table(name = "tickets_expedicion_silos", schema = "dbo")
@Data
public class TicketExpedicionSilosEntity {

    @EmbeddedId
    private TicketExpedicionSilosEntityPk ticketExpedicionSilosPk;
    @Column(name = "epl_codigo")
    private Integer codigoSilo;
    @Column(name = "epl_descripcion")
    private String descripcionSilo;

    @Transient
    private TicketExpedicionSilosFormDto ticketExpedicionSilosFormDto;

}