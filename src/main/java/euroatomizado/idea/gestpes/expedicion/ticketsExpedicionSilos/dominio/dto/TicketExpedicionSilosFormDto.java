package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class TicketExpedicionSilosFormDto {

    @NotNull
    @Size(max = 50)
    private String ticketId;
    @NotNull
    private Integer codigo;
    @NotNull
    private Integer codigoSilo;
    @NotNull
    @Size(max = 500)
    private String descripcionSilo;


}