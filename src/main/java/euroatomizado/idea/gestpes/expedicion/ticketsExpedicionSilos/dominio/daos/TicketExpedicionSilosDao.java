package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.daos;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.entidades.TicketExpedicionSilosEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.entidades.TicketExpedicionSilosEntityPk;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketExpedicionSilosDao extends CrudRepository<TicketExpedicionSilosEntity, TicketExpedicionSilosEntityPk> {

    @Query(" SELECT tic " +
            " FROM TicketExpedicionSilosEntity as tic " +
            " WHERE tic.ticketExpedicionSilosPk.ticketId = :ticket ")
    List<TicketExpedicionSilosEntity> obtenerSilosPorTicket(@Param("ticket") String ticket);

}
