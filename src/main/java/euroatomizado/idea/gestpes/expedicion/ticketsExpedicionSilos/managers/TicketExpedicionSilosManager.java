package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.managers;

import euroatomizado.idea.gestpes._configuracion.configs.dataSources.EmpresaService;
import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes._configuracion.managers.PrimaryKeyStringManagerInterface;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.entidades.TicketExpedicionHumedadEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.entidades.TicketExpedicionSilosEntity;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.entidades.TicketExpedicionSilosEntityPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class TicketExpedicionSilosManager implements PrimaryKeyStringManagerInterface<TicketExpedicionSilosInfoDto,
        TicketExpedicionSilosEntity, TicketExpedicionSilosEntityPk, TicketExpedicionSilosFormDto> {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;
    @Autowired
    private EmpresaService empresaService;


    @Transactional(rollbackFor = Exception.class)
    public List<TicketExpedicionSilosInfoDto> obtenerSilos(String ticket) {
        return obtenerDatosSalida(daoManager.getTicketExpedicionSilosDao().obtenerSilosPorTicket(ticket));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TicketExpedicionSilosInfoDto guardar(TicketExpedicionSilosFormDto datos) {
        TicketExpedicionSilosEntity silo = mapperManager.getMapeadorTicketExpedicionSilos().toEntidad(datos);
        silo.setTicketExpedicionSilosFormDto(datos);
        silo.setTicketExpedicionSilosPk(obtenerPrimaryKey(datos.getTicketId(), datos.getCodigo()));
        try {
            return obtenerDatosSalida(guardarNuevo(silo));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void guardar(List<TicketExpedicionSilosFormDto> silos, String ticketId) {
        borrarSilos(ticketId);
        Integer codigo = 1;
        for (TicketExpedicionSilosFormDto silo : silos) {
            silo.setCodigo(codigo);
            guardar(silo);
            codigo = codigo + 1;
        }
    }

    @Override
    public TicketExpedicionSilosEntity guardarNuevo(TicketExpedicionSilosEntity nuevo) {
        daoManager.getTicketExpedicionSilosDao().save(nuevo);
        return nuevo;
    }

    @Override
    public TicketExpedicionSilosEntity modificar(TicketExpedicionSilosEntity nuevo) {
        return nuevo;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        TicketExpedicionSilosEntityPk pk = obtenerPrimaryKey(id);
        if (existe(pk))
            daoManager.getTicketExpedicionSilosDao().delete(pk);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrarSilos(String ticketId) {
        List<TicketExpedicionSilosEntity> lista = daoManager.getTicketExpedicionSilosDao().obtenerSilosPorTicket(ticketId);
        for (TicketExpedicionSilosEntity silo : lista) {
            daoManager.getTicketExpedicionSilosDao().delete(silo);
        }
    }

    @Override
    public TicketExpedicionSilosInfoDto obtenerDatosSalida(TicketExpedicionSilosEntity almacen) {
        return mapperManager.getMapeadorTicketExpedicionSilos().toInfoDto(almacen);
    }

    private List<TicketExpedicionSilosInfoDto> obtenerDatosSalida(List<TicketExpedicionSilosEntity> almacenes) {
        return mapperManager.getMapeadorTicketExpedicionSilos().toInfoDto(almacenes);
    }

    @Override
    public boolean existe(TicketExpedicionSilosEntityPk pk) {
        return pk != null && daoManager.getTicketExpedicionSilosDao().exists(pk);
    }

    @Override
    public TicketExpedicionSilosEntity obtener(TicketExpedicionSilosEntity datos) {
        return daoManager.getTicketExpedicionSilosDao().findOne(datos.getTicketExpedicionSilosPk());
    }

    @Override
    public TicketExpedicionSilosEntityPk obtenerPrimaryKey(String id) {
        TicketExpedicionSilosEntityPk pk = new TicketExpedicionSilosEntityPk();
        pk.setTicketId(id);
        pk.setEmpresaId(EMPRESA);
        return pk;
    }

    public TicketExpedicionSilosEntityPk obtenerPrimaryKey(String id, Integer codigo) {
        TicketExpedicionSilosEntityPk pk = new TicketExpedicionSilosEntityPk();
        pk.setTicketId(id);
        pk.setEmpresaId(EMPRESA);
        pk.setSeccionId(empresaService.getEmpresa());
        pk.setCodigo(codigo);
        return pk;
    }

}
