package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto;

import lombok.Data;

@Data
public class TicketExpedicionSilosInfoDto {

    private String empresaId;
    private String ticketId;
    private String seccionId;
    private Integer codigo;
    private Integer codigoSilo;
    private String descripcionSilo;


}
