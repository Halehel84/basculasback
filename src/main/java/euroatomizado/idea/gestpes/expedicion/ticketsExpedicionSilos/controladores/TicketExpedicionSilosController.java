package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionHumedades.dominio.dto.TicketExpedicionHumedadInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.managers.TicketExpedicionSilosManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/tickets-expedicion-silos")
public class TicketExpedicionSilosController {

    @Autowired
    private TicketExpedicionSilosManager manager;


    @ResponseStatus(OK)
    @GetMapping(value = "obtener-silos")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<TicketExpedicionSilosInfoDto> obtenerSilos(@PathParam("ticket") String ticket) {
        return manager.obtenerSilos(ticket);
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public TicketExpedicionSilosInfoDto crear(@RequestBody @Valid TicketExpedicionSilosFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public TicketExpedicionSilosInfoDto modificar(@RequestBody @Valid TicketExpedicionSilosFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}