package euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.mapeadores;

import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.dto.TicketExpedicionSilosInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicionSilos.dominio.entidades.TicketExpedicionSilosEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorTicketExpedicionSilos {

    @Mapping(source = "ticketExpedicionSilosPk.ticketId", target = "ticketId")
    @Mapping(source = "ticketExpedicionSilosPk.empresaId", target = "empresaId")
    @Mapping(source = "ticketExpedicionSilosPk.seccionId", target = "seccionId")
    @Mapping(source = "ticketExpedicionSilosPk.codigo", target = "codigo")
    TicketExpedicionSilosInfoDto toInfoDto(TicketExpedicionSilosEntity datos);

    List<TicketExpedicionSilosInfoDto> toInfoDto(List<TicketExpedicionSilosEntity> datos);

    TicketExpedicionSilosEntity toEntidad(TicketExpedicionSilosFormDto dto);

    void actualizar(TicketExpedicionSilosEntity datosNueva, @MappingTarget TicketExpedicionSilosEntity datos);

}