package euroatomizado.idea.gestpes.sacas.managers;

import euroatomizado.idea.gestpes._configuracion.configs.dataSources.EmpresaService;
import euroatomizado.idea.gestpes._configuracion.excepciones.ErrorException;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes._configuracion.excepciones.info.InfoExceptionEnum;
import euroatomizado.idea.gestpes._configuracion.managers.DaoManager;
import euroatomizado.idea.gestpes._configuracion.managers.MapperManager;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.FiltrosConsultaExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionInfoDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.entidades.TicketExpedicionEntity;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.daos.ArticuloDao;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntity;
import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntityPk;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntityPk;
import euroatomizado.idea.gestpes.sacas.dominio.dto.FiltrosConsultaSacasFormDto;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaFormDto;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaInfoDto;
import euroatomizado.idea.gestpes.sacas.dominio.entidades.SacaEntity;
import euroatomizado.idea.gestpes.sistema.contadores.dominio.entidades.ContadorEntity;
import euroatomizado.idea.gestpes.sistema.estados.dominio.entidades.EstadoEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static euroatomizado.idea.gestpes._configuracion.constantes.EmpresaConstants.EMPRESA;

@Service
public class SacaManager {

    @Autowired
    private DaoManager daoManager;
    @Autowired
    private MapperManager mapperManager;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private EmpresaService empresaService;

    @Transactional(rollbackFor = Exception.class)
    public List<SacaInfoDto> obtenerSacas() {
        return obtenerDatosSalida(daoManager.getSacaDao().findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public List<SacaInfoDto> obtenerConsultaSacas(FiltrosConsultaSacasFormDto filtros) {
        if (filtros.getInicio() != null) {
            filtros.setInicio(filtros.getInicio().withHour(0).withMinute(0).withSecond(0).withNano(0));
        }
        if (filtros.getFin() != null) {
            filtros.setFin(filtros.getFin().withHour(23).withMinute(59).withSecond(59).withNano(0));
        }
        List<SacaEntity> datosEntity = daoManager.getConsultaSacaDao().consultaExpedicionTickets(filtros, em);
        List<SacaInfoDto> datosInfo = obtenerDatosSalida(datosEntity);
        return asignarCamposRelacionadosConsulta(datosEntity, datosInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public SacaInfoDto guardar(SacaFormDto datos) {
        SacaEntity entity = mapperManager.getMapeadorSaca().toEntidad(datos);
        entity.setSacaFormDto(datos);
        try {
            if (!existe(entity.getSacaId()))
                return obtenerDatosSalida(guardarNuevo(entity));
            return obtenerDatosSalida(modificar(entity));
        } catch (Exception e) {
            throw new ErrorException(InfoExceptionEnum.ERROR_SERVIDOR);
        }
    }

    public SacaEntity guardarNuevo(SacaEntity nuevo) {
        nuevo.setSeccionId(empresaService.getEmpresa());
        nuevo.setSacaId(obtenerIdNuevo(nuevo.getSeccionId()));
        nuevo.setEmpresa(EMPRESA);
        nuevo = asignarOtrosCampos(nuevo);
        daoManager.getSacaDao().save(nuevo);
        return nuevo;
    }

    public SacaEntity modificar(SacaEntity nuevo) {
        SacaEntity entity = obtener(nuevo);
        mapperManager.getMapeadorSaca().actualizar(nuevo, entity);
        daoManager.getSacaDao().save(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public void borrar(String id) {
        if (existe(id))
            daoManager.getSacaDao().delete(id);
        else throw new NotFoundException(InfoExceptionEnum.REGISTRO_NO_ENCONTRADO);
    }

    public SacaInfoDto obtenerDatosSalida(SacaEntity entity) {
        return mapperManager.getMapeadorSaca().toInfoDto(entity);
    }

    private List<SacaInfoDto> obtenerDatosSalida(List<SacaEntity> entidades) {
        return mapperManager.getMapeadorSaca().toInfoDto(entidades);
    }

    public boolean existe(String id) {
        return id != null && daoManager.getSacaDao().exists(id);
    }

    public SacaEntity obtener(SacaEntity datos) {
        return daoManager.getSacaDao().findOne(datos.getSacaId());
    }

    private List<SacaInfoDto> asignarCamposRelacionadosConsulta(List<SacaEntity> datosEntity, List<SacaInfoDto> datosInfo) {
        for (int i = 0; i < datosEntity.size(); i++) {

            List<TicketEntity> tickets = daoManager.getTicketDao().obtenerTicketsPorSaca(datosEntity.get(i).getSacaId());
            SacaInfoDto dato = datosInfo.get(i);
            for (int j = 0; j < tickets.size(); j++) {
                TicketEntity ticket = tickets.get(j);
                if (j == 0) datosInfo.remove(dato);
                SacaInfoDto datoNuevo = new SacaInfoDto();
                asignarDatoNuevo(dato, datoNuevo);
                datoNuevo.setFechaTicket(ticket.getFecha());
                datoNuevo.setTicketId(ticket.getTicketPk().getTicId());
                datoNuevo.setEntidad(ticket.getEntidad());
                Float cantidad = ticket.getPesoNeto().floatValue();
                datoNuevo.setCantidad(cantidad);
                Long cantidadTot = Long.valueOf(0);
                for (int k = 0; k < tickets.size(); k++) cantidadTot += tickets.get(k).getPesoNeto();
                datoNuevo.setCantidadTotal(cantidadTot.floatValue());
                datosInfo.add(datoNuevo);
            }

        }

        return datosInfo;
    }

    private void asignarDatoNuevo(SacaInfoDto dato, SacaInfoDto datoNuevo) {
        datoNuevo.setSacaId(dato.getSacaId());
        datoNuevo.setEmpresa(dato.getEmpresa());
        datoNuevo.setSeccionId(dato.getSeccionId());
        datoNuevo.setSeccionDescripcion(dato.getSeccionDescripcion());
        datoNuevo.setSeccionDescripcionCorta(dato.getSeccionDescripcionCorta());
        datoNuevo.setFecha(dato.getFecha());
        datoNuevo.setClienteId(dato.getClienteId());
        datoNuevo.setClienteDescripcion(dato.getClienteDescripcion());
        datoNuevo.setArticuloId(dato.getArticuloId());
        datoNuevo.setArticuloDescripcion(dato.getArticuloDescripcion());
        datoNuevo.setEstadoCodigo(dato.getEstadoCodigo());
        datoNuevo.setEstadoDescripcion(dato.getEstadoDescripcion());
        datoNuevo.setObservaciones(dato.getObservaciones());
        datoNuevo.setIndicadorEntradaEnvio(dato.getIndicadorEntradaEnvio());
    }

    private String obtenerIdNuevo(String seccion) {
        ContadorEntity contador = daoManager.getContadorDao().obtenerContadorSacas(seccion);
        contador.setConContador(contador.getConContador() + 1);
        daoManager.getContadorDao().save(contador);
        String id = "9" + contador.getConContador().intValue();
        return id;
    }

    private SacaEntity asignarOtrosCampos(SacaEntity saca) {
        ClienteEntityPk pkCliente = new ClienteEntityPk();
        pkCliente.setClienteId(saca.getCliente().getClientePk().getClienteId());
        pkCliente.setEmpresa("NPC");
        ClienteEntity cliente = daoManager.getClienteDao().findOne(pkCliente);
        saca.setCliente(cliente);

        ArticuloEntityPk pkArticulo= new ArticuloEntityPk();
        pkArticulo.setArticuloId(saca.getArticulo().getArticuloPk().getArticuloId());
        pkArticulo.setEmpresa("NPC");
        ArticuloEntity articulo = daoManager.getArticuloDao().findOne(pkArticulo);
        saca.setArticulo(articulo);

        EstadoEntity estado = daoManager.getEstadoDao().findOne(saca.getEstado().getEstId());
        saca.setEstado(estado);

        return saca;
    }

}

