package euroatomizado.idea.gestpes.sacas.controladores;

import euroatomizado.idea.gestpes._configuracion.constantes.ConstantesRoles;
import euroatomizado.idea.gestpes._configuracion.excepciones.NotFoundException;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.FiltrosConsultaExpedicionFormDto;
import euroatomizado.idea.gestpes.expedicion.ticketsExpedicion.dominio.dto.TicketExpedicionInfoDto;
import euroatomizado.idea.gestpes.sacas.dominio.dto.FiltrosConsultaSacasFormDto;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaFormDto;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaInfoDto;
import euroatomizado.idea.gestpes.sacas.managers.SacaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/api/v1/sacas")
public class SacaController {

    @Autowired
    private SacaManager manager;

    @ResponseStatus(OK)
    @GetMapping(value = "")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<SacaInfoDto> obtenerSacas() {
        return manager.obtenerSacas();
    }

    @ResponseStatus(OK)
    @PostMapping(value = "consulta")
    @PreAuthorize("hasAnyRole('" + ConstantesRoles.ADMINISTRADOR + "','" + ConstantesRoles.CONSULTA +"')")
    public List<SacaInfoDto> obtenerConsultaSacas(@RequestBody @Valid FiltrosConsultaSacasFormDto datos) {
        return manager.obtenerConsultaSacas(datos);
    }

    @ResponseStatus(CREATED)
    @PostMapping
    public SacaInfoDto crear(@RequestBody @Valid SacaFormDto datos) {
        return manager.guardar(datos);
    }

    @ResponseStatus(CREATED)
    @PutMapping
    public SacaInfoDto modificar(@RequestBody @Valid SacaFormDto datos) {
        return manager.guardar(datos);
    }

    @DeleteMapping(value = "{id:\\d+}")
    public ResponseEntity borrar(@PathVariable String id) {
        try {
            manager.borrar(id);
            return new ResponseEntity(OK);
        }catch (NotFoundException e){
            return new ResponseEntity(NOT_FOUND);
        }
    }

}
