package euroatomizado.idea.gestpes.sacas.dominio.daos;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import euroatomizado.idea.gestpes.sacas.dominio.dto.FiltrosConsultaSacasFormDto;
import euroatomizado.idea.gestpes.sacas.dominio.entidades.QSacaEntity;
import euroatomizado.idea.gestpes.sacas.dominio.entidades.SacaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public interface ConsultaSacaDao extends CrudRepository<SacaEntity, String> {

    default List<SacaEntity> consultaExpedicionTickets(FiltrosConsultaSacasFormDto filtros, EntityManager em) {
        QSacaEntity sacaEntity = QSacaEntity.sacaEntity;
        /*FROM*/
        JPQLQuery queryPrincipal = new JPAQuery(em);
        queryPrincipal.from(sacaEntity);
        /*WHERE*/
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if (filtros.getInicio() != null && filtros.getFin() != null) {
            booleanBuilder.and(sacaEntity.fecha.between(filtros.getInicio(), filtros.getFin()));
        }
        if (filtros.getCliente() != null) {
            booleanBuilder.and(sacaEntity.cliente.clientePk.clienteId.eq(filtros.getCliente()));
        }
        if (filtros.getArticulo() != null) {
            booleanBuilder.and(sacaEntity.articulo.articuloPk.articuloId.eq(filtros.getArticulo()));
        }
        if (filtros.getPlanta() != null) {
            booleanBuilder.and(sacaEntity.seccionId.eq(filtros.getPlanta()));
        }
        queryPrincipal.where(booleanBuilder);
        queryPrincipal.select(sacaEntity);
        /*ORDENACION*/
        queryPrincipal.orderBy(sacaEntity.seccionId.asc(), sacaEntity.fecha.asc());
        return queryPrincipal.fetch();
    }
}
