package euroatomizado.idea.gestpes.sacas.dominio.daos;

import euroatomizado.idea.gestpes.sacas.dominio.entidades.SacaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SacaDao extends CrudRepository<SacaEntity, String> {

    List<SacaEntity> findAll();

}
