package euroatomizado.idea.gestpes.sacas.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class FiltrosConsultaSacasFormDto {

    private String planta;
    private String entidad;
    private String operario;
    @NotNull
    private LocalDateTime inicio;
    @NotNull
    private LocalDateTime fin;
    private String cliente;
    private String articulo;

}
