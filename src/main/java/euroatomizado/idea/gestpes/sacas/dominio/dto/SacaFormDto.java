package euroatomizado.idea.gestpes.sacas.dominio.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class SacaFormDto {


    @Size(max = 50)
    private String sacaId;
    @Size(max = 4)
    private String empresa;
    @Size(max = 2)
    private String seccionId;
    @NotNull
    private LocalDateTime fecha;
    @NotNull
    @Size(max = 20)
    private String clienteId;
    @NotNull
    @Size(max = 25)
    private String articuloId;
    private Float cantidad;
    private Integer estadoCodigo;
    @Size(max = 500)
    private String observaciones;
    @Size(max = 50)
    private String indicadorEntradaEnvio;

}
