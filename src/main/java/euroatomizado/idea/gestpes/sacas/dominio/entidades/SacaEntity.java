package euroatomizado.idea.gestpes.sacas.dominio.entidades;

import euroatomizado.idea.gestpes.maestros.articulos.dominio.entidades.ArticuloEntity;
import euroatomizado.idea.gestpes.maestros.clientes.dominio.entidades.ClienteEntity;
import euroatomizado.idea.gestpes.maestros.secciones.dominio.entidades.SeccionEntity;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaFormDto;
import euroatomizado.idea.gestpes.sistema.estados.dominio.entidades.EstadoEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity()
@Table(name = "sacas", schema = "dbo")
@Data
public class SacaEntity {

    @Id
    @Column(name = "sac_id")
    private String sacaId;
    @Column(name = "sac_empresa")
    private String empresa;
    @Column(name = "sac_fecha")
    private LocalDateTime fecha;
    @Column(name = "sac_cantidad", columnDefinition = "real")
    private Float cantidad;
    @Column(name = "sac_observaciones")
    private String observaciones;
    @Column(name = "sac_indicador_entrada_envio")
    private String sacIndicadorEntradaEnvio;
    @Column(name = "xseccion_id")
    private String seccionId;
    @Column(name = "xcliente_id")
    private String clienteId;
    @Column(name = "xarticulo_id")
    private String articuloId;

    @OneToMany(mappedBy = "saca")
    private List<TicketEntity> tickets = new LinkedList<>();

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xcliente_id", referencedColumnName = "xcliente_id", insertable = false, updatable = false),
            @JoinColumn(name = "sac_empresa", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "xarticulo_id", referencedColumnName = "xarticulo_id", insertable = false, updatable = false),
            @JoinColumn(name = "sac_empresa", referencedColumnName = "xempresa_id", insertable = false, updatable = false)
    })
    private ArticuloEntity articulo;

    @ManyToOne
    @JoinColumn(name = "est_codigo")
    private EstadoEntity estado;

    @Transient
    private SacaFormDto sacaFormDto;

}
