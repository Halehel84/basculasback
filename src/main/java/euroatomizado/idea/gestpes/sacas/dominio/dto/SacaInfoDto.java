package euroatomizado.idea.gestpes.sacas.dominio.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SacaInfoDto {

    private String sacaId;
    private String empresa;
    private String seccionId;
    private LocalDateTime fecha;
    private String clienteId;
    private String articuloId;
    private Float cantidad;
    private Integer estadoCodigo;
    private String observaciones;
    private String indicadorEntradaEnvio;

    private String clienteDescripcion;
    private String articuloDescripcion;
    private String estadoDescripcion;
    private String seccionDescripcion;
    private String seccionDescripcionCorta;
    private String ticketId;
    private LocalDateTime fechaTicket;
    private Float cantidadTotal;
    private String entidad;

}
