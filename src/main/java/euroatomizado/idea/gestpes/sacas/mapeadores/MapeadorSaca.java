package euroatomizado.idea.gestpes.sacas.mapeadores;

import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaFormDto;
import euroatomizado.idea.gestpes.sacas.dominio.dto.SacaInfoDto;
import euroatomizado.idea.gestpes.sacas.dominio.entidades.SacaEntity;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.dto.TicketInfoDto;
import euroatomizado.idea.gestpes.ticket.tickets.dominio.entidades.TicketEntity;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MapeadorSaca {

    @AfterMapping
    default void convertirDatos(@MappingTarget SacaInfoDto resultado, SacaEntity saca) {
        if (saca.getSeccionId() != null) {
            resultado.setSeccionDescripcion(saca.getSeccionId().equals("0") ? "NPC" : "EURO");
            resultado.setSeccionDescripcionCorta(saca.getSeccionId().equals("0") ? "NPC" : "EURO");
        }
    }

    @Mapping(source = "cliente.clientePk.clienteId", target = "clienteId")
    @Mapping(source = "cliente.nombre", target = "clienteDescripcion")
    @Mapping(source = "articulo.articuloPk.articuloId", target = "articuloId")
    @Mapping(source = "articulo.descripcion", target = "articuloDescripcion")
    @Mapping(source = "estado.estId", target = "estadoCodigo")
    @Mapping(source = "estado.estDescripcion", target = "estadoDescripcion")

    SacaInfoDto toInfoDto(SacaEntity datos);

    List<SacaInfoDto> toInfoDto(List<SacaEntity> datos);

    @Mapping(source = "clienteId", target = "cliente.clientePk.clienteId")
    @Mapping(source = "articuloId", target = "articulo.articuloPk.articuloId")
    @Mapping(source = "estadoCodigo", target = "estado.estId")
    @Mapping(source = "seccionId", target = "seccion.seccionId")
    SacaEntity toEntidad(SacaFormDto dto);

    void actualizar(SacaEntity datosNueva, @MappingTarget SacaEntity datos);

}